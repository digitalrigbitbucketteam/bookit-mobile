//
//  BookViewControllerTests.swift
//  bookit-mobileTests
//
//  Created by Cristiana Yambo on 6/7/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import XCTest
@testable import bookit_mobile
class mockBookableRepository: BookableRepository {
    var completeFunc: (()->())? = nil
    init() {
        super.init(networkService: BookableNetworkService.init())
    }
    override func getCurrent(completion: @escaping ([Bookable]?, String?) -> ()) {
        completeFunc?()
    }
}

class BookViewControllerTests: XCTestCase {
    
    var viewController: BookViewController!
    var mockRepo: mockBookableRepository!
    override func setUp() {
        super.setUp()
        //create the VC and add the mock repo to it
        mockRepo = mockBookableRepository.init()
        viewController = BookViewController(bookableRepository: mockRepo, confirmController: UIViewController(), searchController: UIViewController())
    }
    
    override func tearDown() {
        
        super.tearDown()
    }
    
    /// Tests that the actionable notifications cause the loadData method to be called
    func testNotifications() {
        //list of actionable notifications
        let notifications: [Notification.Name] = [.loggedIn, .serverChanged, .bookingAdded, .locationChanged]
        
        notifications.forEach { (notification) in
            //set the expectation
            let expectation = XCTestExpectation.init()
            mockRepo.completeFunc = {
                expectation.fulfill()
            }
            //post the notification
            NotificationCenter.default.post(name: notification, object: nil)
            wait(for: [expectation], timeout: 1.0)
        }
    }
    
    /// Logout should remove all data from the rooms list
    func testLogoutNotification() {
        viewController.rooms = [[testValues.testBookable]]
        NotificationCenter.default.post(name: .loggedOut, object: nil)
        //logout removes all the rooms and it should hold zero items
        XCTAssertEqual(viewController.rooms.count, 0)
    }
    
    /// Tests to see if the semaphore doesn't block multiple calls
    func testSemaphore() {
        let expectation = XCTestExpectation.init(description: "Thread should not get blocked")
        viewController.loadData()
        viewController.loadData()
        viewController.loadData()
        expectation.fulfill()
        wait(for: [expectation], timeout: 1.0)
    }
    
}
