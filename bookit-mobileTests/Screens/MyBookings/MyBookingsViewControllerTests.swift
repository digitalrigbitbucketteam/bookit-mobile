//
//  MyBookingsViewControllerTests.swift
//  bookit-mobileTests
//
//  Created by Cristiana Yambo on 6/7/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import XCTest
@testable import bookit_mobile

class mockBookingRepository: BookingRepository {
    var completeFunc: (()->())? = nil
    init() {
        super.init(networkService: BookingNetworkService.init(), authorization: nil)
    }
    override func getAll(completion: @escaping ([Booking]?, String?) -> ()) {
        completion(nil,nil)
        completeFunc?()
    }
}
class MyBookingsViewControllerTests: XCTestCase {
    
    var viewController: MyBookingsViewController!
    var mockRepo: mockBookingRepository!
    override func setUp() {
        super.setUp()
        //setup the repo with the mock repo
        mockRepo = mockBookingRepository()
        viewController = MyBookingsViewController(bookingRepository: mockRepo)
    }
    
    override func tearDown() {
        
        super.tearDown()
    }
    
    /// Tests that the actionable notifications cause the loadData method to be called
    func testNotifications() {
        let notifications: [Notification.Name] = [.loggedIn, .serverChanged, .bookingAdded]
        
        notifications.forEach { (notification) in
            let expectation = XCTestExpectation.init()
            mockRepo.completeFunc = {
                expectation.fulfill()
            }
            NotificationCenter.default.post(name: notification, object: nil)
            wait(for: [expectation], timeout: 1.0)
        }
    }
    
    /// Logout should remove all data from the bookings list
    func testLogoutNotification() {
        viewController.bookings = [testValues.testBooking]
        NotificationCenter.default.post(name: .loggedOut, object: nil)
        //logout removes all the bookings and it should hold zero items
        XCTAssertEqual(viewController.bookings.count, 0)
    }
    
    /// Tests to see if the semaphore doesn't block multiple calls
    func testSemaphore() {
        let expectation = XCTestExpectation.init(description: "Thread should not get blocked")
        viewController.loadData()
        viewController.loadData()
        viewController.loadData()
        expectation.fulfill()
        wait(for: [expectation], timeout: 1.0)
    }
}
