//
//  SettingsGeofenceTests.swift
//  bookit-mobileTests
//
//  Created by Cristiana Yambo on 6/18/18.
//  Copyright © 2018 Buildit@Wipro Digital. All rights reserved.
//

import XCTest
@testable import bookit_mobile

class mockGeofence: Geofence {
    var enableLocationService = true
    var monitorEnabled: Bool? = nil
    
    override func locationServicesEnabled() -> Bool {
        return enableLocationService
    }
    override func monitor(_ turnOn: Bool) {
        monitorEnabled = turnOn
    }
}
fileprivate class mockDefaults: UserDefaults {
    var geofenceEnabled = true
    var setKey: String? = nil
    var setBool: Bool? = nil
    override func bool(forKey defaultName: String) -> Bool {
        return geofenceEnabled
    }
    override func set(_ value: Bool, forKey defaultName: String) {
        setKey = defaultName
        setBool = value
    }
}
class SettingsGeofenceTests: XCTestCase {

    struct geofenceStruct: SettingsGeofence {
        fileprivate var defaultsMock = mockDefaults.init()
        var geofenceMock = mockGeofence.init()
        //User Defaults
        var geofenceDefaults: UserDefaults {
            get {
                return defaultsMock
            }
        }
        //instance of the Geofence class
        var geofence: Geofence {
            get {
                return geofenceMock
            }
        }
    }
    
    var geoStruct: geofenceStruct!

    
    override func setUp() {
        super.setUp()
        geoStruct = geofenceStruct.init()

    }
    
    override func tearDown() {
        
        super.tearDown()
    }
    
    func testGeofenceEnabled() {
        geoStruct.geofenceMock.enableLocationService = false
        XCTAssertFalse(geoStruct.geofenceEnabled())
        
        geoStruct.geofenceMock.enableLocationService = true
        geoStruct.defaultsMock.geofenceEnabled = false
        XCTAssertFalse(geoStruct.geofenceEnabled())
        
        geoStruct.geofenceMock.enableLocationService = true
        geoStruct.defaultsMock.geofenceEnabled = true
        XCTAssertTrue(geoStruct.geofenceEnabled())
    }
    
    func testSetGeofenceOn() {
        geoStruct.setGeofence(enabled: true)
        if let mon = geoStruct.geofenceMock.monitorEnabled {
            XCTAssertTrue(mon)
        } else {
            XCTFail("monitorEnabled should be set")
        }
        if let setBool = geoStruct.defaultsMock.setBool {
            XCTAssertTrue(setBool)
        } else {
            XCTFail("setBool should be set")
        }
        XCTAssertEqual(geoStruct.defaultsMock.setKey, DefaultsKeys.GeofenceEnabled.rawValue)
    }
    func testSetGeofenceOff() {
        geoStruct.setGeofence(enabled: false)
        if let mon = geoStruct.geofenceMock.monitorEnabled {
            XCTAssertFalse(mon)
        } else {
            XCTFail("monitorEnabled should be set")
        }
        if let setBool = geoStruct.defaultsMock.setBool {
            XCTAssertFalse(setBool)
        } else {
            XCTFail("setBool should be set")
        }
        XCTAssertEqual(geoStruct.defaultsMock.setKey, DefaultsKeys.GeofenceEnabled.rawValue)
    }
    func testSetGeofenceFromDefaults() {
        geoStruct.geofenceMock.enableLocationService = true
        geoStruct.defaultsMock.geofenceEnabled = true
        geoStruct.setGeofenceFromDefaults()
        
        if let mon = geoStruct.geofenceMock.monitorEnabled {
            XCTAssertTrue(mon)
        } else {
            XCTFail("monitorEnabled should be set")
        }
        if let setBool = geoStruct.defaultsMock.setBool {
            XCTAssertTrue(setBool)
        } else {
            XCTFail("setBool should be set")
        }
    }
    func testSetGeofenceControls() {
        geoStruct.geofenceMock.enableLocationService = true
        geoStruct.defaultsMock.geofenceEnabled = true
        
        let geoSwitch = UISwitch()
        
        geoStruct.setGeofenceControls(control: geoSwitch)
        
        XCTAssertTrue(geoSwitch.isOn)
        
        geoStruct.geofenceMock.enableLocationService = false
        geoStruct.defaultsMock.geofenceEnabled = true
        
        geoStruct.setGeofenceControls(control: geoSwitch)
        
        XCTAssertFalse(geoSwitch.isOn)
 
        geoStruct.geofenceMock.enableLocationService = true
        geoStruct.defaultsMock.geofenceEnabled = false
        
        geoStruct.setGeofenceControls(control: geoSwitch)
        
        XCTAssertFalse(geoSwitch.isOn)
    }
    
}
