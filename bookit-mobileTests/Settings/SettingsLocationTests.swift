//
//  SettingsLocationTests.swift
//  bookit-mobileTests
//
//  Created by Cristiana Yambo on 6/4/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import XCTest
@testable import bookit_mobile


/// Mock Location Repository, contains static location data
class mockLocationRepository: LocationRepository {
    override init(networkService: LocationNetworkServiceProtocol) {
        super.init(networkService: networkService)
        //just set with testValues data
        setRepository(data: [testValues.testLocation,testValues.testLocation2,testValues.testLocation3])
    }
}

/// Mock UserDefaults class
fileprivate class mockDefaults: UserDefaults {
    var badData = false //trigger a bad data failure
    var setKey: String? = nil //key that was set in defaults
    var setData: Location? = nil //location that was set in defaults
    
    /// Overridden objectForKey
    ///
    /// - Parameter defaultName: totally ignored
    /// - Returns: returns test values or bad data
    override func object(forKey defaultName: String) -> Any? {
        if !badData {
            //if we want to return good data, send testValue data
            return try? PropertyListEncoder().encode(testValues.testLocation2)
        } else {
            //otherwise send a Date
            return try? PropertyListEncoder().encode(Date.init())
        }
    }
    
    /// Overridden setValue
    ///
    /// - Parameters:
    ///   - value: decoded as a Location and saved
    ///   - defaultName: name that was set
    override func set(_ value: Any?, forKey defaultName: String) {
        setData = try? PropertyListDecoder().decode(Location.self, from: value as! Data)
        setKey = defaultName
    }
}

class SettingsLocationTests: XCTestCase {
    
    /// Struct to test that uses the SettingsLocation extension
    struct location: SettingsLocation {
        fileprivate var defaultsMock = mockDefaults.init()
        var repoMock = mockLocationRepository.init(networkService: mockLocationNetworkService.init())
        //for defaults, send the mock
        var locationDefaults: UserDefaults {
            get {
                return defaultsMock
            }
        }
        //for the repo also send the mock
        var repo: LocationRepository {
            get {
                return repoMock
            }
        }
    }
    
    var locationStruct: location!
    
    override func setUp() {
        super.setUp()
        //make a new one every time
        locationStruct = location.init()
    }
    
    func testGetLocationFromDefaults() {
        //test with good defaults data
        XCTAssertEqual(locationStruct.getLocationFromDefaults(), testValues.testLocation2)
        
        //set too use bad data
        locationStruct.defaultsMock.badData = true
        //which should be nil
        XCTAssertNil(locationStruct.getLocationFromDefaults())
    }
    func testGetCurrentLocation() {
        //check to see if the current location matches
        XCTAssertEqual(locationStruct.getCurrentLocation(), testValues.testLocation2)
        
        //now make the defaults have bad data
        locationStruct.defaultsMock.badData = true
        //which should return the first static location
        XCTAssertEqual(locationStruct.getCurrentLocation(), testValues.testLocation)
        
        //now check when the repo is nil
        LocationRepository.repository = nil
        //which should return the AppSettings location
        XCTAssertEqual(locationStruct.getCurrentLocation(), AppSettings.location)
    }
    
    func testSetLocationControls() {
        //create a control
        let control = UISegmentedControl.init()
        //set the data
        locationStruct.setLocationControls(control: control)
        //there should be 3 segments
        XCTAssertEqual(control.numberOfSegments, 3)
        //and check the title of the second item to see if it matches the testValue
        XCTAssertEqual(control.titleForSegment(at: 1), testValues.testLocation2.name)
    }
    func testSelectLocation() {
        //the .locationChanged notification should be sent when location has changed
        let expectation = XCTestExpectation.init(description: "Notification should be sent")
        NotificationCenter.default.addObserver(forName: .locationChanged, object: nil, queue: nil) { (notification) in
            expectation.fulfill()
        }
        //select the location
        locationStruct.selectLocation(locationName: testValues.testLocation3.name)
        
        //check to see if the stored data matches the static data
        XCTAssertEqual(locationStruct.defaultsMock.setKey, DefaultsKeys.CurrentLocation.rawValue)
        XCTAssertEqual(locationStruct.defaultsMock.setData, testValues.testLocation3)
        
        wait(for: [expectation], timeout: 1.0)
    }
    func testGetAllLocations() {
        let expectation = XCTestExpectation.init(description: "All locations should run")
        locationStruct.getAllLocations { (locations) in
            expectation.fulfill()
            //make sure there are three locations
            XCTAssertEqual(locations?.count, 3)
            //and that they are correct
            XCTAssertEqual(locations?[0].name, testValues.testLocation.name)
        }
        
        wait(for: [expectation], timeout: 1.0)
    }
}
