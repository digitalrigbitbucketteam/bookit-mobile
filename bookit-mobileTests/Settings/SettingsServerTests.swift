//
//  SettingsServerTests.swift
//  bookit-mobileTests
//
//  Created by Cristiana Yambo on 5/30/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import XCTest
@testable import bookit_mobile
/// Mock UserDefaults class
fileprivate class mockDefaults: UserDefaults {
    var serverType = testValues.server.rawValue
    var currentKey: String?
    var setUrl: URL?
    var setInt: Int?
    override func integer(forKey: String) -> Int {
        currentKey = forKey
        return serverType
    }
    override func url(forKey defaultName: String) -> URL? {
        currentKey = defaultName
        return URL.init(string: testValues.customUrl)
    }
    override func set(_ url: URL?, forKey defaultName: String) {
        currentKey = defaultName
        setUrl = url
    }
    override func set(_ value: Int, forKey defaultName: String) {
        currentKey = defaultName
        setInt = value
    }
}
class SettingsServerTests: XCTestCase {
    struct server: SettingsServer {
        fileprivate var defaultsMock = mockDefaults.init()
        var serverDefaults: UserDefaults {
            get {
                return defaultsMock
            }
        }
    }
    
    var serverStruct: server!
    
    override func setUp() {
        super.setUp()
        serverStruct = server.init()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    /// Gets the server type and validates it
    func testGetSelectedServerType() {
        //see if it matches our static value
        XCTAssertEqual(serverStruct.getSelectedServerType(), testValues.server)
        
        //test to see when the serverType is bad that it sets to .Local
        serverStruct.defaultsMock.serverType = 0
        XCTAssertEqual(serverStruct.getSelectedServerType(), ServerType.Local)
        XCTAssertEqual(serverStruct.defaultsMock.currentKey, DefaultsKeys.SelectedServer.rawValue)
    }
    
    /// Tests getting the current url and to see if it is valid
    func testGetCurrentServerUrl() {
        XCTAssertEqual(serverStruct.getCurrentServerUrl(), AppSettings.serverUrls[testValues.server])

        //set to custom
        serverStruct.defaultsMock.serverType = ServerType.Custom.rawValue
        
        //check to see if it matches the customURL
        XCTAssertEqual(serverStruct.getCurrentServerUrl(), testValues.customUrl)
        //check to see if the proper key was set
        XCTAssertEqual(serverStruct.defaultsMock.currentKey, DefaultsKeys.CustomServerUrl.rawValue)
        
    }
    
    /// Test setting the URL text field when segments are changed
    func testSetServerControlsTextField() {
        var textField = UITextField.init()
        serverStruct.setServerControls(control: nil, text: textField)
        XCTAssertEqual(textField.text, serverStruct.getCurrentServerUrl())
        XCTAssertFalse(textField.isEnabled)
        
        textField = UITextField.init()
        serverStruct.defaultsMock.serverType = ServerType.Custom.rawValue
        serverStruct.setServerControls(control: nil, text: textField)
        XCTAssertEqual(textField.text, testValues.customUrl)
        XCTAssertTrue(textField.isEnabled)
    }
    
    /// Tests setting the segmented control
    func testSetServerControlsSegment() {
        let segment = UISegmentedControl.init()
        serverStruct.setServerControls(control: segment, text: nil)
        
        //get the number of segments it should have
        let numSegments = (ServerType.Custom.rawValue - ServerType.Local.rawValue) + 1
        XCTAssertEqual(segment.numberOfSegments, numSegments)
        
        //get the segment that should be selected
        let selectedSegment = serverStruct.getSelectedServerType().rawValue - ServerType.Local.rawValue
        XCTAssertEqual(segment.selectedSegmentIndex, selectedSegment)
        
        //check to see if all segments are set to the correct title
        for i in 0 ..< numSegments {
            let title = segment.titleForSegment(at: i)
            if let serverType = (ServerType.init(rawValue: (i + ServerType.Local.rawValue))) {
                XCTAssertEqual(title, String.init(describing: serverType))
            } else {
                XCTFail("Server Type should always be valid")
            }
        }
    }
    
    /// Checks to see if the proper defaults get set when changing the custom server
    func testSetCustomServer() {
        //set it to the testValue
        serverStruct.setCustomServer(url: testValues.customUrl)
        
        //check to see if proper key was set
        XCTAssertEqual(serverStruct.defaultsMock.currentKey, DefaultsKeys.CustomServerUrl.rawValue)
        
        //make sure the URL is correcr
        XCTAssertEqual(serverStruct.defaultsMock.setUrl?.absoluteString, testValues.customUrl)
    }
    
    /// Tests server selection from segmented control
    func testSelectServer() {
        //get index from our testValue
        let index = testValues.server.rawValue - ServerType.Local.rawValue
        let text = UITextField.init()
        
        //set an expectation for the .serverChanged notification
        let notificationExpectation = XCTestExpectation.init(description: ".serverChanged notification should be posted")
        
        //set observer for .serverChanged notification
        NotificationCenter.default.addObserver(forName: Notification.Name.serverChanged, object: nil, queue: nil) { (notification) in
            notificationExpectation.fulfill()
        }
        
        //select the server
        serverStruct.selectServer(index: index, text: text)
        
        //test to see if defaults were properly set
        XCTAssertEqual(serverStruct.defaultsMock.currentKey, DefaultsKeys.SelectedServer.rawValue)
        XCTAssertEqual(serverStruct.defaultsMock.setInt, testValues.server.rawValue)
        
        wait(for: [notificationExpectation], timeout: 1.0)
    }
}
