//
//  SettingsLoginTests.swift
//  bookit-mobileTests
//
//  Created by Cristiana Yambo on 5/30/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import XCTest
@testable import bookit_mobile

class SettingsLoginTests: XCTestCase {
    //struct to contain SettingsLogin protocol extension
    struct login: SettingsLogin {
    }
    var loginStruct: login!
    var authorization: mockAuthorization!
    override func setUp() {
        super.setUp()
        //init the login struct
        loginStruct = login.init()
        //set Authorization to a mock version
        authorization =  try! mockAuthorization.init(authorizationContext: mockContext.init(), authorizationView: mockAuthorizationView.init(), notificationCenter: NotificationCenter.default)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    /// Test loginOrOut when the user is logged in
    func testLogout() {
        let logoutExpectation = XCTestExpectation.init()
        //set the mock to be logged in
        authorization.loggedIn = true
        do {
            //test loginOrOut, and logout block should only be called with a nil token
            try loginStruct.logInOrOut(authorization: authorization, loginBlock: { (token) in
                XCTFail("Login Block should not be called")
            }) { (token) in
                logoutExpectation.fulfill()
                XCTAssertNil(token)
            }
        } catch {
            XCTFail("Error should not be thrown")
        }

        wait(for: [logoutExpectation], timeout: 1.0)
    }
    
    /// Tests loginOrOut when user is logged in and an error is thrown
    func testLogoutFail() {
        let logoutErrorExpectation = XCTestExpectation.init()
        //set the mock to be logged in and to trigger an error
        authorization.loggedIn = true
        authorization.logoutError = true
        do {
            //test the logInOrOut, and an error should be thrown
            try loginStruct.logInOrOut(authorization: authorization, loginBlock: { (token) in
                XCTFail("Login Block should not be called")
            }) { (token) in
                XCTFail("Logout block should not be called")
            }
        } catch {
            logoutErrorExpectation.fulfill()
        }
        wait(for: [logoutErrorExpectation], timeout: 1.0)
    }
    
    /// Test loginOrOut when user is not logged in
    func testLogin() {
        let loginExpectation = XCTestExpectation.init()
        //set the user to be logged out
        authorization.loggedIn = false
        do {
            //see if the login block is called whe loginOrOut is run
            try loginStruct.logInOrOut(authorization: authorization, loginBlock: { (token) in
                //test the token to be not nil, and to contain our mock value
                XCTAssertNotNil(token)
                XCTAssertEqual(token!.getTokenString(), "Bearer \(testValues.testUser.id)")
                loginExpectation.fulfill()
            }, logoutBlock: { (token) in
                XCTFail("Logout block should not be called")
            })
        } catch {
            XCTFail("Errors should not be thrown")
        }
        wait(for: [loginExpectation], timeout: 1.0)
    }
    
    /// Tests loginOrOut when user is not logged in and it fails
    func testLoginFail() {
        let loginErrorExpectation = XCTestExpectation.init()
        //set the user to be logged out and an error to be thrown
        authorization.loggedIn = false
        authorization.loginError = true
        do {
            try loginStruct.logInOrOut(authorization: authorization, loginBlock: { (token) in
                XCTFail("Login block should not be called")
            }, logoutBlock: { (token) in
                XCTFail("Logout block should not be called")
            })
        } catch {
            //make sure an error is thrown
            loginErrorExpectation.fulfill()
        }
        wait(for: [loginErrorExpectation], timeout: 1.0)
    }
    
    /// Test setting the button title when the user is logged in or out
    func testSetLoginButtonTitle() {
        var button = UIButton.init()
        loginStruct.setLoginButtonTitle(authorization: nil, button: button)
        XCTAssertEqual(button.titleLabel?.text, "Login")
        
        button = UIButton.init()
        mockAuthorization.authorizationToken = AuthorizationToken.init(tokenResult: mockResult.init())
        loginStruct.setLoginButtonTitle(authorization: authorization, button: button)

        XCTAssertEqual(button.titleLabel?.text, "Logout \(testValues.testUser.name)")
        
    }
}
