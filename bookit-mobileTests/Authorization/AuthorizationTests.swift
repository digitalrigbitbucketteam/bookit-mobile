//
//  AuthorizationTests.swift
//  bookit-mobileTests
//
//  Created by Cristiana Yambo on 5/21/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import XCTest
import MSAL
@testable import bookit_mobile

class AuthorizationTests: XCTestCase {
    var context: mockContext!
    var authorization: Authorization!
    var viewSingleton: mockViewSingleton!
    var authorizationView: mockAuthorizationView!
    var notificationCenter = NotificationCenter.default
    override func setUp() {
        super.setUp()
        
        //setup the mock application context
        let appContext = mockApplicationContext.init()
        
        //logout user
        try? appContext.remove(appContext.users().first)
        
        //initialize the mock context
        context = mockContext.init(applicationContext: appContext)
        
        //init the view singletons
        viewSingleton = mockViewSingleton.init()
        
        //setup the auth view
        authorizationView = mockAuthorizationView.init()
        
        
        
        //setup the authorization class to be tested with mock objects
        authorization = Authorization.init(authorizationContext: context, authorizationView: authorizationView, notificationCenter: notificationCenter)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testLoginPass() {
        context.fail = false
        let expectation = XCTestExpectation.init(description: "Login closure should run")
        let notificationExpectation = XCTestExpectation.init(description: ".loggedIn notification should be posted")
        NotificationCenter.default.addObserver(forName: Notification.Name.loggedIn, object: nil, queue: nil) { (notification) in
            notificationExpectation.fulfill()
        }
        do {
            try authorization.acquireUser(completion: { (token) in
                expectation.fulfill()
                XCTAssertTrue(self.authorization.isLoggedIn())
                if let _ = token?.tokenResult as? mockResult {
                    //token contains our mock result, so it passes
                } else {
                    XCTFail("Login token should be valid")
                }
            })
        } catch {
            XCTFail("Errors should not be thrown")
        }

        wait(for: [expectation,notificationExpectation], timeout: 1.0)
    }
    
    func testLoginFail() {
        context.fail = true
        let expectation = XCTestExpectation.init(description: "Login closure should run")
        do {
            try authorization.acquireUser(completion: { (token) in
                expectation.fulfill()
                XCTAssertFalse(self.authorization.isLoggedIn())
                XCTAssertNil(token, "Login should fail")
            })
        } catch {
            XCTFail("Errors should not be thrown")
        }

        wait(for: [expectation], timeout: 1.0)
    }
    func testLoginInteraction() {
        context.fail = false
        context.empty = true
        context.useSuperLoadUser = true
        let expectation = XCTestExpectation.init(description: "Interaction Login block should run")
        mockAuthorizationView.loginBlock = {
            expectation.fulfill()
        }
        do {
            try authorization.acquireUser(completion: { (token) in
                XCTFail("Login should not complete")
            })
        } catch {
            XCTFail("Errors should not be thrown")
        }

        wait(for: [expectation], timeout: 1.0)
    }
    
    
    func testLogout() {
        Authorization.authorizationToken = AuthorizationToken.init(tokenResult: mockResult.init())
        let expectation = XCTestExpectation.init(description: "Logout should be called")
        let notificationExpectation = XCTestExpectation.init(description: ".loggedOut notification should be posted")
        context.logoutBlock = {
            expectation.fulfill()
        }
        NotificationCenter.default.addObserver(forName: Notification.Name.loggedOut, object: nil, queue: nil) { (notification) in
            notificationExpectation.fulfill()
        }
        do {
           try authorization.logout()
        } catch {
            XCTFail("Errors should not be thrown")
        }
        XCTAssertFalse(authorization.isLoggedIn())
        XCTAssertNil(authorization.getToken(), "Token should be nil")
        wait(for: [expectation,notificationExpectation], timeout: 1.0)
    }
    func testGetTokenString() {
        let token = AuthorizationToken.init(tokenResult: mockResult.init())
        guard let tokenString = token.getTokenString() else {
            XCTFail("Token string should be available")
            return
        }
        XCTAssertEqual(tokenString, "Bearer \(testValues.testUser.id)")
    }
    func testCancelLogin() {
        let expectation = XCTestExpectation.init(description: "Login cancel notification should be sent")
        NotificationCenter.default.addObserver(forName: Notification.Name.loginCancelled, object: nil, queue: nil) { (notification) in
            expectation.fulfill()
        }
        authorization.loginCancelled()
        wait(for: [expectation], timeout: 1.0)
    }
}
