//
//  TestConstants.swift
//  bookit-mobileTests
//
//  Created by Cristiana Yambo on 5/30/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import XCTest
@testable import bookit_mobile

/// Struct to contain contstants
struct testValues {
//    static let idToken = "TOKEN"
//    static let username = "USER"
    static let customUrl = "https://test.com"
    static let server = ServerType.Stage
    static let intErrorCode = 99999
    static let errorMessageString = "ERROR MESSAGE STRING"
    static let testLocation = Location(id: "b1177996-75e2-41da-a3e9-fcdd75d1ab31", name: "LOC NAME", timeZone: "TZ", timezoneDisplayName: "LOC TIMEZONE DISPLAY")
    static let testLocation2 = Location(id: "43ec3f7d-348d-427f-8c13-102ca0362a62", name: "LOC NAME2", timeZone: "TZ2", timezoneDisplayName: "LOC TIMEZONE DISPLAY2")
    static let testLocation3 = Location(id: "439c3fe8-124f-4a44-8f97-662a5d8334d3", name: "LOC NAME3", timeZone: "TZ3", timezoneDisplayName: "LOC TIMEZONE DISPLAY3")
    
    static let testBookableDisposition = BookableDisposition.init(closed: true, reason: "BOOKABLEREASON")
    static let testBookable = Bookable.init(id: "BID", name: "BNAME", location: testValues.testLocation, bookings: nil, disposition: testValues.testBookableDisposition)
    static let testUser = User.init(id: "UID", externalId: "EXTERNID", name: "User Name")
    static let testUser2 = User.init(id: "UID2", externalId: "EXTERNID2", name: "User Name2")
    static let testBooking = Booking.init(id: "BOOKINGID", subject: "BOOKINGSUBJ", start: Date.init(), startTimezoneAbbreviation: "STZ", end: Date.init(), endTimezoneAbbreviation: "ETZ", bookable: Bookable.init(id: "BOOKID", name: "BOOKANAME", location: testValues.testLocation, bookings: nil, disposition: testValues.testBookableDisposition), user: testValues.testUser)
    static let testBooking2 = Booking.init(id: "BOOKINGID2", subject: "BOOKINGSUBJ2", start: Date.init(), startTimezoneAbbreviation: "STZ2", end: Date.init(), endTimezoneAbbreviation: "ETZ", bookable: Bookable.init(id: "BOOKID", name: "BOOKANAME", location: testValues.testLocation2, bookings: nil, disposition: testValues.testBookableDisposition), user: testValues.testUser2)
    static let testBooking3 = Booking.init(id: "BOOKINGID3", subject: "BOOKINGSUBJ3", start: Date.init(), startTimezoneAbbreviation: "STZ3", end: Date.init(), endTimezoneAbbreviation: "ETZ", bookable: Bookable.init(id: "BOOKID2", name: "BOOKANAME2", location: testValues.testLocation3, bookings: nil, disposition: testValues.testBookableDisposition), user: testValues.testUser)
}
