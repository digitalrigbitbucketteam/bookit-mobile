//
//  TestMocks.swift
//  bookit-mobileTests
//
//  Created by Cristiana Yambo on 5/30/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import XCTest
import MSAL
@testable import bookit_mobile


/// Mock authorizationView - used to check to see if showLoginScreen was called
class mockAuthorizationView: AuthorizationView {
    
    //block called when showLoginScreen is run
    static var loginBlock: (() -> Void)?
    
    /// new showLoginScreen that just calls the loginBlock
    ///
    /// - Parameters:
    ///   - loginMessagingDelegate: ignored
    ///   - displayBlock: ignored
    override func showLoginScreen(loginMessagingDelegate: LoginMessagingAction, displayBlock: () -> Void) {
        if let block = mockAuthorizationView.loginBlock {
            block()
        }
    }
}

/// Mock view view singleton, allows for changing of values that are returned
class mockViewSingleton: viewSingletons {
    //backing variables that can be changed to get appropriate responses
    var vc: UIViewController? = UIApplication.shared.keyWindow?.rootViewController
    var kw: UIWindow? = UIApplication.shared.keyWindow
    
    /// Returns key window backing variable
    var keyWindow: UIWindow? {
        get {
            return kw
        }
    }
    
    /// Returns root view controller backing variable
    var rootViewController: UIViewController? {
        get {
            return vc
        }
        set {
            vc = newValue
        }
    }
}

/// Mock result
class mockResult: MSALResult {
    //internal mockUser, used to return our test
    //constant username
    class mockUser: MSALUser {
        override var name: String { get {
            return testValues.testUser.name
            }}
        override var uid: String { get {
            return testValues.testUser.externalId
        }}
    }
    /// Returns our token test value
    override var idToken: String {
        return testValues.testUser.id
    }
    
    /// Returns a mock user
    override var user : mockUser {
        get {
            return mockUser.init()
        }
    }
}

/// Mock Application Context
class mockApplicationContext: MSALPublicClientApplication {
}

/// Mock AuthorizationContext
class mockContext: AuthorizationContext {
    //set if acquire token should fail
    var fail = false
    //set to true to return empty on isEmpty
    var empty = false
    //set to try to use the super.loadUser instead of ours
    var useSuperLoadUser = false
    //block to run when logout happens
    var logoutBlock : (() -> Void)?
    
    var silentAcquire = false
    /// Loads a mock user or fails depending on options
    ///
    /// - Parameters:
    ///   - completionBlock: called when completed
    ///   - interactionRequiredBlock: called when interaction is needed
    /// - Throws: error when load fails
    override func loadUser(completionBlock: @escaping (AuthorizationToken?, Bool, AuthorizationError?) -> Void, interactionRequiredBlock: @escaping () -> Void) throws {
        //if useSuperLoadUser is true, then run the
        //loadUser on the superclass
        if useSuperLoadUser {
            try? super.loadUser(completionBlock: completionBlock, interactionRequiredBlock: interactionRequiredBlock)
        //otherwise mock the process
        } else {
            //if we do not want to fail create a mock token
            //and send it to the user
            if !fail {
                let token = AuthorizationToken.init(tokenResult: mockResult.init())
                completionBlock(token, silentAcquire, nil)
            //otherwise fail and send an error
            } else {
                let error = AuthorizationError.init(kind: .acquireTokenFailed)
                completionBlock(nil,silentAcquire ,error)
            }
        }
        
    }
    
    /// Mock acquireToken
    ///
    /// - Parameter completionBlock: called when complete
    override func acquireToken(completionBlock: @escaping (AuthorizationToken?, Bool, AuthorizationError?) -> Void) {
        //right now fail or success does the same thing
        if !fail {
            completionBlock(nil, silentAcquire, nil)
        } else {
            completionBlock(nil, silentAcquire, nil)
        }
    }
    
    /// Mocks out the isEmpty method based on settings
    ///
    /// - Returns: result based on settings
    /// - Throws: nothing
    override func isEmpty() throws -> Bool {
        if empty {
            return true
        } else {
            return false
        }
    }
    
    /// overrides logout, will run custom block
    ///
    /// - Throws: nothing
    override func logout() throws {
        if let block = logoutBlock {
            block()
        }
    }
}

/// Mock Authorization class
class mockAuthorization: Authorization {
    //settings to control application flow
    var loggedIn = false //set based on login status
    var logoutError = false //set to trigger logout error
    var loginError = false //set to trigger login erro
    
    
    /// Mocks logout method based on settings
    ///
    /// - Throws: error when logoutError is true
    override func logout() throws {
        if logoutError {
            throw NSError.init()
        }
        return
    }
    
    /// Mocks isLoggedIn
    ///
    /// - Returns: login status based on settings
    override func isLoggedIn() -> Bool {
        return loggedIn
    }
    
    /// Mocks the login process
    ///
    /// - Parameter completion: completion bkock
    /// - Throws: error when loginErro is true
    override func acquireUser(completion: @escaping (AuthorizationToken?) -> ()) throws {
        if loginError {
            throw AuthorizationError.init(kind: .loginFailed)
        }
        else {
            try super.acquireUser(completion: completion)
        }
    }
}


class mockLocationNetworkService: LocationNetworkServiceProtocol {
    var fail: Bool = false
    func fetch(completion: @escaping ([Location]?, LocationNetworkError?) -> ()) {
        if fail {
            let err = LocationNetworkError.init(code: testValues.intErrorCode, message: testValues.errorMessageString)
            completion(nil, err)
            return
        } else {
            completion([testValues.testLocation, testValues.testLocation2, testValues.testLocation3],nil)
            return
        }
    }
}
