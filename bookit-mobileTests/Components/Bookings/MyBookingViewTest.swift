//
//  MyBookingViewTest.swift
//  bookit-mobileTests
//
//  Created by Gregory Soloshchenko on 5/10/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import XCTest
@testable import bookit_mobile

class MyBookingViewTest: XCTestCase {
    var view: MyBookingView?
    var model: Booking?
    
    let user = User(id: "1", externalId: "1", name: "Name")
    
    let bookable = Bookable(
        id: "1",
        name: "name",
        location: Location(id: "1", name: "NYC", timeZone: "EDT", timezoneDisplayName: "Eastern"),
        bookings: [],
        disposition: BookableDisposition(closed: false, reason: "")
    )
    
    override func setUp() {
        super.setUp()
        
        view = MyBookingView(frame: .zero)
    }
    
    override func tearDown() {
        view = nil
        model = nil
        
        super.tearDown()
    }
    
    func testSetupGeneral() {
        model = Booking(
            id: "1",
            subject: "subject",
            start: Formatter.bookit.date(from: "2018-08-30T12:30")!,
            startTimezoneAbbreviation: "edt",
            end: Formatter.bookit.date(from: "2018-08-30T13:30")!,
            endTimezoneAbbreviation: "edt",
            bookable: bookable,
            user: user
        )
        
        view!.setup(for: model!)
        
        XCTAssertEqual(view!.nameLabel.text, model!.subject, "Name label should equal Model's name")
        XCTAssertEqual(view!.roomNameLabel.text, "\(model!.bookable.name.capitalizingFirstLetter())", "Room label should equal Model's  room name")
        XCTAssertEqual(view!.capacityLabel.text, "Holds \(model!.bookable.capacity) people", "Name label should equal Model's capacity")
        XCTAssertEqual(view!.timeLabel.text, "8/30/18: 12:30 PM - 1:30 PM", "Time label should equal Model's time")
    }

    func testBranding(){
        XCTAssertEqual(view!.nameLabel.textColor, ThemeService.primaryTextColor, "Name label should have branded text color")
        XCTAssertEqual(view!.timeLabel.textColor, ThemeService.primaryTextColor, "Capacity label should have branded text color")
        XCTAssertEqual(view!.roomNameLabel.textColor, ThemeService.primaryTextColor, "Room label should have branded text color")
        XCTAssertEqual(view!.capacityLabel.textColor, ThemeService.secondaryTextColor, "Capacity label should have branded text color")

        XCTAssertEqual(view!.nameLabel.font.pointSize, ThemeService.font.subtitle.size, "Name label should have branded font size")
        XCTAssertEqual(view!.timeLabel.font.pointSize, ThemeService.font.subtitle.size, "Time label should have branded font size")
        XCTAssertEqual(view!.roomNameLabel.font.pointSize, ThemeService.font.subtitle.size, "Room label should have branded font size")
        XCTAssertEqual(view!.capacityLabel.font.pointSize, ThemeService.font.subheading.size, "Capacity label should have branded font size")
    }
}
