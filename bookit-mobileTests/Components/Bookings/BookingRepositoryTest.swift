//
//  BookingRepositoryTest.swift
//  bookit-mobileTests
//
//  Created by Gregory Soloshchenko on 5/21/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import XCTest
@testable import bookit_mobile

fileprivate class MockBookingNetworkService: BookingNetworkServiceProtocol{
    var pushHasBeenCalled = false
    enum fetchDataTypes {
        case twoOwned
        case oneOwned
        case noneOwned
    }
    var fetchError = false
    var fetchData = false
    var fetchDataType = fetchDataTypes.twoOwned
    
    let bookingNetworkError = BookingNetworkError.init(code: testValues.intErrorCode, message: testValues.errorMessageString)
    func fetch(path: String, queryItems: [URLQueryItem]?, completion: @escaping ([Booking]?, BookingNetworkError?) -> ()) {
        if fetchError {
            completion(nil, bookingNetworkError)
            return
        }
        var data : [Booking]? = nil
        
        if fetchData {
            switch fetchDataType {
            case .twoOwned:
                data = [testValues.testBooking,testValues.testBooking2,testValues.testBooking3]
            case .oneOwned:
                data = [testValues.testBooking,testValues.testBooking2,testValues.testBooking2]
            case .noneOwned:
                data = [testValues.testBooking2,testValues.testBooking2,testValues.testBooking2]
            }
            
        }
        
        completion(data,nil)
    }
    
    func push(data: Booking, completion: @escaping ([Booking]?, BookingNetworkError?) -> ()) {
        pushHasBeenCalled = true
        completion(nil, nil)
    }
}

class BookingRepositoryTest: XCTestCase {
    var repository: BookingRepository?
    
    fileprivate var networkService: MockBookingNetworkService?
    let context = try? mockContext.init()
    var auth: mockAuthorization!
    
    override func setUp() {
        super.setUp()
        networkService = MockBookingNetworkService()
        
        networkService?.pushHasBeenCalled = false
        auth = mockAuthorization.init(authorizationContext: context!, authorizationView: mockAuthorizationView.init(), notificationCenter: NotificationCenter.default)

        repository = BookingRepository(networkService: networkService!, authorization: auth)
    }
    
    override func tearDown() {

        super.tearDown()
    }
    
    func testSave(){
        guard let networkService = networkService else {
            fatalError("Network Service must not be nil")
        }
        
        var saveCompletionCalled = false
        
        let dispositionMock = BookableDisposition(closed: false, reason: "")
        
        let bookableMock = Bookable(
            id: "",
            name: "",
            location: Location(id: "", name: "", timeZone: "", timezoneDisplayName: ""),
            bookings: nil,
            disposition: dispositionMock
        )
        
        let bookingMock = Booking(
            id: nil,
            subject: nil,
            start: Date(),
            startTimezoneAbbreviation: "",
            end: Date(),
            endTimezoneAbbreviation: "",
            bookable: bookableMock,
            user: nil
        )
        
        XCTAssertFalse(networkService.pushHasBeenCalled, "Checkup value must be false beforehand")
        
        repository?.save(booking: bookingMock, completion: { _,_ in saveCompletionCalled = true })
        
        XCTAssertTrue(networkService.pushHasBeenCalled, "Should call fetch of Network service")
        XCTAssertTrue(saveCompletionCalled, "Should call completion when done")
    }
    /// Tests the repo when there is a networking error, and makes sure the error contains the same message
    func testGetAllFail() {
        let expectation = XCTestExpectation.init(description: "Get All Should Fail")
        networkService?.fetchError = true
        repository?.getAll(completion: { (booking, error) in
            if error == self.networkService?.bookingNetworkError.message {
                expectation.fulfill()
            }
        })
        wait(for: [expectation], timeout: 1.0)
    }
    
    /// Tests the repo when the data is nil
    func testGetAllNilData() {
        let expectation = XCTestExpectation.init(description: "Data should be nil")
        networkService?.fetchData = false
        repository?.getAll(completion: { (booking, error) in
            if error == BookingRepositoryErrorKinds.nilBooking.rawValue {
                expectation.fulfill()
            }
        })
        wait(for: [expectation], timeout: 1.0)
    }

    func testGetAll() {
        let expectation = XCTestExpectation.init()

        networkService?.fetchData = true
        networkService?.fetchDataType = .oneOwned
        
        repository?.getAll(completion: { (booking, error) in
            XCTAssertEqual(3, booking?.count, "There should be 3 bookings")
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: 1.0)
    }
    
    
    /// Tests the repo when there is a networking error, and makes sure the error contains the same message
    func testGetMyFail() {
        let expectation = XCTestExpectation.init(description: "GetMy Should Fail")
        networkService?.fetchError = true
        repository?.getMyBookings(completion: { (booking, error) in
            if error == self.networkService?.bookingNetworkError.message {
                expectation.fulfill()
            }
        })
        wait(for: [expectation], timeout: 1.0)
    }
    
    /// Tests the repo when the data is nil
    func testGetMyNilData() {
        let expectation = XCTestExpectation.init(description: "Data should be nil")
        networkService?.fetchData = false
        repository?.getMyBookings(completion: { (booking, error) in
            if error == BookingRepositoryErrorKinds.nilBooking.rawValue {
                expectation.fulfill()
            }
        })
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testGetMyNilAuth() {
        let expectation = XCTestExpectation.init(description: "GetMy should fail")
        
        repository = BookingRepository(networkService: networkService!, authorization: nil)
        
        networkService?.fetchData = true
        repository?.getMyBookings(completion: { (booking, error) in
            if error == BookingRepositoryErrorKinds.noAuthorization.rawValue {
                expectation.fulfill()
            }
        })
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testGetMyAuthFailed() {
        let expectation = XCTestExpectation.init(description: "GetMy should fail")
        networkService?.fetchData = true
        auth.loginError = true
        repository?.getMyBookings(completion: { (booking, error) in
            if error == BookingRepositoryErrorKinds.acquireUserFailed.rawValue {
                expectation.fulfill()
            }
        })
        wait(for: [expectation], timeout: 1.0)
    }
    
    /// Calls getAll on the repository and runs assertions on the data
    ///
    /// - Parameters:
    ///   - number: number of items to check for
    ///   - expectation: expectation to fulfil
    ///   - file: file to show assert errors on proper file
    ///   - line: line to show assert errors on proper line
    private func repoGetMy(number: Int, expectation: XCTestExpectation, file: StaticString = #file, line: UInt = #line) {
        
        
        context?.useSuperLoadUser = false
        context?.fail = false
        context?.empty = false
        
        repository?.getMyBookings(completion: { (booking, error) in
            if number == 0 {
                XCTAssertEqual(error, BookingRepositoryErrorKinds.noBookings.rawValue, file: file, line: line)
            } else {
                XCTAssertNil(error, file: file, line: line)
                XCTAssertEqual(number, booking?.count, "There should be \(number) bookings", file: file, line: line)
            }

            expectation.fulfill()
        })
    }
    
    /// Tests the repo when there should be no filtered bookings
    func testGetMy0() {
        let expectation = XCTestExpectation.init(description: "Get all should call completion")

        networkService?.fetchData = true
        networkService?.fetchDataType = .noneOwned
        repoGetMy(number: 0, expectation: expectation)
        wait(for: [expectation], timeout: 1.0)
    }
    
    /// Tests the repo when there should be one filtered booking
    func testGetMy1() {
        let expectation = XCTestExpectation.init(description: "Get all should call completion")

        networkService?.fetchData = true
        networkService?.fetchDataType = .oneOwned
        repoGetMy(number: 1, expectation: expectation)
        wait(for: [expectation], timeout: 1.0)
    }
    
    /// Tests the repo when there should be two filtered bookings
    func testGetMy2() {
        let expectation = XCTestExpectation.init(description: "Get all should call completion")

        networkService?.fetchData = true
        networkService?.fetchDataType = .twoOwned
        repoGetMy(number: 2, expectation: expectation)
        wait(for: [expectation], timeout: 1.0)
    }


    
}
