//
//  PickerTest.swift
//  bookit-mobileTests
//
//  Created by Gregory Soloshchenko on 5/31/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import XCTest
@testable import bookit_mobile

class PickerTest: XCTestCase {
    var picker: Picker?
    let values = [[
        PickerValue(name: "name0-1", value: "value0-1"),
        PickerValue(name: "name0-2", value: "value0-2"),
        PickerValue(name: "name0-3", value: "value0-3")
    ],[
        PickerValue(name: "name1-1", value: "value1-1"),
        PickerValue(name: "name1-2", value: "value1-2"),
        PickerValue(name: "name1-3", value: "value1-3")
    ]]
    
    let selectedIndexes = [
        PickerIndex(component: 0, row: 1),
        PickerIndex(component: 1, row: 2)
    ]
    
    override func setUp() {
        super.setUp()
        picker = Picker(frame: .zero)
    }
    
    override func tearDown() {
        picker = nil
        
        super.tearDown()
    }
    
    func testSetup() {
        guard let picker = picker else {
            fatalError("Where is Picker?")
        }
        
        picker.setup(values: values, selectedIndexes: selectedIndexes)
        
        XCTAssert(picker.values.count == values.count, "should set values on setup")
        XCTAssert(picker.values[0].count == values[0].count, "should set values on setup")
        XCTAssertEqual(picker.values[0][0].name, values[0][0].name, "should set values on setup")
        XCTAssertEqual(picker.values[1][2].name, values[1][2].name, "should set values on setup")
        
        XCTAssertEqual(picker.selectedIndexes.count, selectedIndexes.count, "should set selected values on setup")
        XCTAssertEqual(picker.selectedIndexes[0].component, selectedIndexes[0].component, "should set selected values on setup")
        XCTAssertEqual(picker.selectedIndexes[0].row, selectedIndexes[0].row, "should set selected values on setup")
        XCTAssertEqual(picker.selectedIndexes[1].component, selectedIndexes[1].component, "should set selected values on setup")
        XCTAssertEqual(picker.selectedIndexes[1].row, selectedIndexes[1].row, "should set selected values on setup")
    }
    
    func testUpdateTextFiledText() {
        guard let picker = picker else {
            fatalError("Where is Picker?")
        }
        
        picker.text = ""
        picker.updateTextField = true
        picker.setup(values: values, selectedIndexes: selectedIndexes)
        
        XCTAssertEqual(picker.text, "name0-2name1-3", "should set textfiled on setup (if relevant property is set to true)")
        
        picker.text = ""
        picker.updateTextField = false
        picker.setup(values: values, selectedIndexes: selectedIndexes)
        
        XCTAssertEqual(picker.text, "", "should NOT set textfiled on setup (if relevant property is set to false)")
        
        picker.text = ""
        picker.updateTextField = true
        picker.separator = "%%%"
        picker.setup(values: values, selectedIndexes: selectedIndexes)
        
        XCTAssertEqual(picker.text, "name0-2%%%name1-3", "should add separator in textfieled text")
    }
    
    func testDone() {
        guard let picker = picker else {
            fatalError("Where is Picker?")
        }
        
        picker.setup(values: values, selectedIndexes: selectedIndexes)
        
        var callbackHasBeenCalled = false
        picker.doneCallback = { _ in
            callbackHasBeenCalled = true
        }
        
        picker.done(sender: UIButton())
        XCTAssertTrue(callbackHasBeenCalled, "should call 'done' callback (if was specified) when user touches Done")
        
        callbackHasBeenCalled = false
        picker.doneCallback = nil
        picker.done(sender: UIButton())
        XCTAssertFalse(callbackHasBeenCalled, "should NOT call 'done' callback (if was NOT specified) when user touches Done")
        
        picker.text = ""
        picker.updateTextField = true
        picker.done(sender: UIButton())
        XCTAssertEqual(picker.text, "name0-2name1-3", "should update textfiled when user touches Done (if relevant property is set to true)")
        
        
        picker.text = ""
        picker.updateTextField = false
        picker.done(sender: UIButton())
        XCTAssertEqual(picker.text, "", "should NOT update textfiled when user touches Done (if relevant property is set to false)")
        
        
        picker.pickerView.selectRow(2, inComponent: 0, animated: false)
        picker.pickerView.selectRow(1, inComponent: 1, animated: false)
        picker.done(sender: UIButton())
        
        XCTAssertEqual(picker.selectedIndexes.count, 2, "should update selected property by pickerView values when user touches Done")
        XCTAssertEqual(picker.selectedIndexes[0].component, 0, "should update selected property by pickerView values when user touches Done")
        XCTAssertEqual(picker.selectedIndexes[0].row, 2, "should update selected property by pickerView values when user touches Done")
        XCTAssertEqual(picker.selectedIndexes[1].component, 1, "should update selected property by pickerView values when user touches Done")
        XCTAssertEqual(picker.selectedIndexes[1].row, 1, "should update selected property by pickerView values when user touches Done")
    }
    
    func testCancel() {
        guard let picker = picker else {
            fatalError("Where is Picker?")
        }
        
        picker.setup(values: values, selectedIndexes: selectedIndexes)
        
        var callbackHasBeenCalled = false
        picker.cancelCallback = {
            callbackHasBeenCalled = true
        }
        
        picker.cancel(sender: UIButton())
        XCTAssertTrue(callbackHasBeenCalled, "should call 'Cancel' callback (if was specified) when user touches Cancel")
        
        callbackHasBeenCalled = false
        picker.cancelCallback = nil
        picker.cancel(sender: UIButton())
        XCTAssertFalse(callbackHasBeenCalled, "should NOT call 'Cancel' callback (if was NOT specified) when user touches Cancel")
        
        
        picker.pickerView.selectRow(2, inComponent: 0, animated: false)
        picker.pickerView.selectRow(1, inComponent: 1, animated: false)
        picker.cancel(sender: UIButton())
        
        XCTAssertEqual(picker.pickerView.selectedRow(inComponent: 0), 1, "should revert pickerView values to selected properties when user touches Cancel")
        
        XCTAssertEqual(picker.pickerView.selectedRow(inComponent: 1), 2, "should revert pickerView values to selected properties when user touches Cancel")
    }

    func testGetValues(){
        guard let picker = picker else {
            fatalError("Where is Picker?")
        }
        
        picker.setup(values: values, selectedIndexes: selectedIndexes)
        
        let result: [String] = picker.getValues()
        XCTAssertEqual(result.joined(separator: "::"), "value0-2::value1-3", "should return array of values")
        
        // update values
        picker.pickerView.selectRow(2, inComponent: 0, animated: false)
        picker.pickerView.selectRow(1, inComponent: 1, animated: false)
        picker.done(sender: UIButton())
        
        
        let result2: [String] = picker.getValues()
        XCTAssertEqual(result2.joined(separator: "::"), "value0-3::value1-2", "should return array of values")
    }
}
