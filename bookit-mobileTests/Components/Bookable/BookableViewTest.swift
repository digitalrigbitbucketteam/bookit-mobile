//
//  BookableViewTest.swift
//  bookit-mobileTests
//
//  Created by Gregory Soloshchenko on 5/10/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import XCTest
@testable import bookit_mobile

class BookableViewTest: XCTestCase {
    var view: BookableView?
    var location: Location = Location(id: "1", name: "NYC", timeZone: "EDT", timezoneDisplayName: "Eastern")
    var disposition: BookableDisposition?
    var model: Bookable?
    
    override func setUp() {
        super.setUp()
        
        view = BookableView(frame: .zero)
    }
    
    override func tearDown() {
        view = nil
        model = nil
        disposition = nil
        
        super.tearDown()
    }
    
    func testSetupGeneral() {
        disposition = BookableDisposition(closed: false, reason: "")
        model = Bookable(id: "1", name: "name", location: location, bookings: [], disposition: disposition!)
        
        view!.setup(for: model!)
        
        XCTAssertEqual(view!.name.text, model!.name.capitalizingFirstLetter(), "Name label should equal Model's name")
        XCTAssertEqual(view?.capacity.text, "Holds \(model!.capacity) people", "Name label should equal Model's capacity")
        
    }
    
    func testSetupWhenDispositionIsClosed() {
        disposition = BookableDisposition(closed: true, reason: "some reason")
        model = Bookable(id: "1", name: "name", location: location, bookings: [], disposition: disposition!)
        
        view!.setup(for: model!)

        XCTAssertEqual(view!.occupancy.text, disposition?.reason, "Occupancy sholuld equal disposition's reason if bookbale is closed")
    }
    
    func testSetupWhenDispositionIsOpen() {
        disposition = BookableDisposition(closed: false, reason: "")
        model = Bookable(id: "1", name: "name", location: location, bookings: [], disposition: disposition!)
        
        view!.setup(for: model!)
        
        XCTAssertEqual(view!.occupancy.text, "", "Occupancy sholuld equal hardcoded 'Free until 4PM' if bookable is open")
    }
    
    func testBranding(){
        XCTAssertEqual(view!.name.textColor, ThemeService.primaryTextColor, "Name label should have branded text color")
        XCTAssertEqual(view!.capacity.textColor, ThemeService.primaryTextColor, "Capacity label should have branded text color")
        XCTAssertEqual(view!.occupancy.textColor, ThemeService.primaryTextColor, "Occupancy label should have branded text color")
        
        XCTAssertEqual(view!.name.font.pointSize, ThemeService.font.title.size, "Name label should have branded font size")
        XCTAssertEqual(view!.capacity.font.pointSize, ThemeService.font.subtitle.size, "Capacity label should have branded font size")
        XCTAssertEqual(view!.occupancy.font.pointSize, ThemeService.font.subtitle.size, "Occupancy label should have branded font size")
    }
}
