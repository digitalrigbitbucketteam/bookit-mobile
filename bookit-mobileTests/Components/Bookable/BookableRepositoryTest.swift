//
//  BookableRepositoryTest.swift
//  bookit-mobileTests
//
//  Created by Gregory Soloshchenko on 5/21/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import XCTest
@testable import bookit_mobile


/// Mock for BookableNetworkService
fileprivate class MockBookableNetworkService: BookableNetworkServiceProtocol{
    var fetchHasBeenCalled = false
    var pushHasBeenCalled = false
    func fetch(path: String, queryItems: [URLQueryItem]?, completion: @escaping ([Bookable]?, BookableNetworkError?) -> ()) {
        completion(nil, nil)
        fetchHasBeenCalled = true
    }
    
    func push(data: [Bookable], completion: @escaping ([Bookable]?, BookableNetworkError?) -> ()) {
        completion(nil, nil)
        pushHasBeenCalled = true
    }
}

class BookableRepositoryTest: XCTestCase {
    
    /// Ref to tested instance
    var repository: BookableRepository?
    
    /// Mock for network service
    fileprivate var networkService: MockBookableNetworkService?
    
    /// Determines whether getCurrent completion callback was called or not
    var getCurrentCompletionCalled = false
    
    /// Determines whether search by time completion callback was called or not
    var searchByStartAndEndTimeCompletionCalled = false
    
    override func setUp() {
        super.setUp()
        
        searchByStartAndEndTimeCompletionCalled = false
        networkService = MockBookableNetworkService()
        repository = BookableRepository(networkService: networkService!)
    }
    
    override func tearDown() {
        repository = nil
        networkService = nil
        
        super.tearDown()
    }
    
    func testGetCurrentCallsNetworkMethodFetch() {
        guard let networkService = networkService else {
            fatalError("Network Service must not be nil")
        }
        
        getCurrentCompletionCalled = false
        
        XCTAssertFalse(networkService.fetchHasBeenCalled, "Checkup value must be false beforehand")
        
        repository?.getCurrent(completion: { _,_ in self.getCurrentCompletionCalled = true })
        
        XCTAssertTrue(networkService.fetchHasBeenCalled, "Should call fetch of Network service")
        XCTAssertTrue(self.getCurrentCompletionCalled, "Should call completion when done")
    }
    
    func testSearchByStartAndEndTime() {
        guard let networkService = networkService else {
            fatalError("Network Service must not be nil")
        }
        
        searchByStartAndEndTimeCompletionCalled = false
        XCTAssertFalse(networkService.fetchHasBeenCalled, "Checkup value must be false beforehand")
        
        repository?.search(start: Date(), end: Date(), completion: { _,_ in self.searchByStartAndEndTimeCompletionCalled = true })
        
        XCTAssertTrue(networkService.fetchHasBeenCalled, "Should call fetch of Network service")
        XCTAssertTrue(self.searchByStartAndEndTimeCompletionCalled, "Should call completion when done")
    }
}
