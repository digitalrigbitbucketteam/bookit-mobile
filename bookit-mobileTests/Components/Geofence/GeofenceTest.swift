//
//  GeofenceTest.swift
//  bookit-mobileTests
//
//  Created by Cristiana Yambo on 6/18/18.
//  Copyright © 2018 Buildit@Wipro Digital. All rights reserved.
//

import XCTest
import CoreLocation
@testable import bookit_mobile


/// Mock Location Manager class
class mockLocationManager: CLLocationManager {
    //Bools to determine if one of the location manager methods was called
    var requestedInUse: Bool? = nil
    var startedUpdate: Bool? = nil
    var stoppedUpdate: Bool? = nil
    
    /// overridden method to set the bool to true so that tests can determine if it was called
    override func requestWhenInUseAuthorization() {
        requestedInUse = true
    }
    
    /// overridden method to set the bool to true so that tests can determine if it was called
    override func startUpdatingLocation() {
        startedUpdate = true
    }
    
    /// overridden method to set the bool to true so that tests can determine if it was called
    override func stopUpdatingLocation() {
        stoppedUpdate = true
    }
}


/// A subclass of Geofence to override certain methods for testing
class GeofenceTester: Geofence {
    //data for tests
    let locationManagerMock = mockLocationManager()
    let allLocations = [testValues.testLocation,testValues.testLocation2,testValues.testLocation3]
    var useSuperLocationSelectLocation = true //turn on to use the superclass locationSelect
    var lastSetLocationName: String? //if not using the superclass then this will have the last set location name
    var locationSelectExpectation: XCTestExpectation? //an expectation to fulfil when location select happens
    
    //overriden var to return the test mock
    override var locManager: CLLocationManager {
        get {
            return locationManagerMock
        }
    }

    
    /// Overridden getAllLocations so that a custom set of data can be used instead of API data
    ///
    /// - Parameter completion: <#completion description#>
    override func locationsGetAllLocations(_ completion: (([Location]?) -> ())?) {
        if let c = completion {
            let data = allLocations
            c(data)
        }
    }
    
    /// Overridden selectLocation to save the data from location selection
    ///
    /// - Parameter locationName: name of the location to switch to
    override func locationSelectLocation(locationName: String) {
        //if this is set, then call the superclass
        if (useSuperLocationSelectLocation) {
            super.locationSelectLocation(locationName: locationName)
        } else {
            //otherwise store the name
            lastSetLocationName = locationName
            //and try to fulfil the expectation
            if let expectation = locationSelectExpectation {
                expectation.fulfill()
            }
        }
    }
}

class GeofenceTest: XCTestCase {
    var tester: GeofenceTester!
    override func setUp() {
        super.setUp()
        //create a new tester each time
        tester = GeofenceTester.init()
    }

    
    /// Tests the initializtion process
    func testInit() {
        //make sure the distance filter is set
        XCTAssertEqual(tester.locationManagerMock.distanceFilter, AppSettings.distanceFilter)
        //and that the locationManagerDelegate was set
        XCTAssertEqual(tester.locationManagerMock.delegate as? GeofenceTester , tester)
    }
    
    /// Test turning on the location monitor
    func testMonitorOn() {
        tester.monitor(true) //turn on the monitor
        //and check to see if the proper CLLocationManager methods were called
        XCTAssertTrue(tester.locationManagerMock.requestedInUse!)
        XCTAssertTrue(tester.locationManagerMock.startedUpdate!)
        //and see if stop was not called
        XCTAssertNil(tester.locationManagerMock.stoppedUpdate)
    }
    
    /// Test stopping the monitor
    func testMonitorOff() {
        tester.monitor(false) //turn off the monitor
        //and make suer the start process wasn't run
        XCTAssertNil(tester.locationManagerMock.requestedInUse)
        XCTAssertNil(tester.locationManagerMock.startedUpdate)
        //and that that stop was called
        XCTAssertTrue(tester.locationManagerMock.stoppedUpdate!)
    }
    
    /// Testing the CLLocationManager update delegate
    func testDidUpdateDelegate() {
        tester.useSuperLocationSelectLocation = false
        //get the current location
        var currentLoc = tester.getCurrentLocation()
        //set the expectation that the location change is not triggered
        var expectation = XCTestExpectation.init(description: "Location change should not be triggered")
        //set the expectation to NOT be called, if it is run, then the test fails
        expectation.isInverted = true
        tester.locationSelectExpectation = expectation
        
        //run the delegate method
        tester.locationManager(tester.locationManagerMock, didUpdateLocations: [CLLocation.init(latitude: 0, longitude: 0)])
        wait(for: [expectation], timeout: 1.0)

        //get the current location
        currentLoc = tester.getCurrentLocation()
        var nextLoc: Location?
        //and go through all the test locations to find a different one
        for (_,loc) in tester.allLocations.enumerated() {
            //the first different one is found
            if loc != currentLoc {
                //so set and break
                nextLoc = loc
                break
            }
        }
        //make sure the location was found
        if let n = nextLoc {
            //set the expectation to change
            expectation = self.expectation(description: "Location should change")
            tester.locationSelectExpectation = expectation
            //call the delegate method
            tester.locationManager(tester.locationManagerMock, didUpdateLocations: [n.getCoordinates()!])
           
            //wait for the expectations to be run
            waitForExpectations(timeout: 1.0) { (error) in
                //and make sure the location was changed to the proper location
                XCTAssertEqual(n.name, self.tester.lastSetLocationName, "Location should have changed")
            }
            
        } else {
            //fail if a different location was not found
            XCTFail("A different location should have been found")
        }
        
        
    }
}
