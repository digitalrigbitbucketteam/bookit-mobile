//
//  LocationRepositoryTest.swift
//  bookit-mobileTests
//
//  Created by Cristiana Yambo on 6/4/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import XCTest
@testable import bookit_mobile

class LocationRepositoryTest: XCTestCase {
    var service : mockLocationNetworkService!
    var repo : LocationRepository!
    override func setUp() {
        super.setUp()
        service = mockLocationNetworkService.init()
        repo = LocationRepository.init(networkService: service)
        LocationRepository.repository = nil
        LocationRepository.keyedRepository = nil
    }
    
    override func tearDown() {

        super.tearDown()
    }
    func testGetAll() {
        let expectation = XCTestExpectation.init(description: "Completion Should Run")
        service.fail = false
        repo.getAll { (locations, err) in
            if let l = locations {
                expectation.fulfill()
                XCTAssertEqual(l.count, 3)
                XCTAssertEqual(l[0], testValues.testLocation)
                XCTAssertEqual(LocationRepository.repository![0], testValues.testLocation)
                XCTAssertEqual(LocationRepository.keyedRepository![testValues.testLocation.name], testValues.testLocation)
            }
        }
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testGetAllFail() {
        let expectation = XCTestExpectation.init(description: "Completion Should Run")
        service.fail = true
        repo.getAll { (locations, error) in
            if let error = error {
                expectation.fulfill()
                XCTAssertNil(locations)
                XCTAssertEqual(error, testValues.errorMessageString)
            }

        }
        wait(for: [expectation], timeout: 1.0)
    }
    func testCache() {
        let expectation = XCTestExpectation.init(description: "Completion Should Run")
        XCTAssertNil(LocationRepository.getCachedLocations())
        service.fail = false
        repo.getAll { (location, err) in
            expectation.fulfill()
            XCTAssertNotNil(LocationRepository.getCachedLocations())
            XCTAssertEqual(LocationRepository.getCachedLocations(), location)
        }
        wait(for: [expectation], timeout: 1.0)
    }
    func testCachedGet() {
        let expectation = XCTestExpectation.init(description: "Completion Should Run")
        LocationRepository.repository = [testValues.testLocation,testValues.testLocation]
        repo.getAll { (location, err) in
            XCTAssertEqual(location?.count, 2)
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 1.0)
    }
    func testGetLocationForName() {
        XCTAssertNil(repo.getLocationForName(name: testValues.testLocation.name))
        let expectation = XCTestExpectation.init(description: "Completion Should Run")
        XCTAssertNil(LocationRepository.getCachedLocations())
        service.fail = false
        repo.getAll { (location, err) in
            expectation.fulfill()
            XCTAssertEqual(self.repo.getLocationForName(name: testValues.testLocation.name), testValues.testLocation)
        }
        wait(for: [expectation], timeout: 1.0)
    }
}
