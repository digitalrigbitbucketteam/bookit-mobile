//
//  UINavigationBarTest.swift
//  bookit-mobileTests
//
//  Created by Gregory Soloshchenko on 5/10/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import XCTest
@testable import bookit_mobile

class UINavigationBarTest: XCTestCase {
    var view: UINavigationBar?
    
    override func setUp() {
        super.setUp()
        view = UINavigationBar(frame: .zero)
    }
    
    override func tearDown() {
        view = nil
        
        super.tearDown()
    }
    
    func testBranding() {
        view?.brandit()
        
        XCTAssertEqual(view!.tintColor, ThemeService.primaryBgColor, "Branded NavController tint color should be as it specified in ThemeService")
        XCTAssertEqual(view!.barTintColor, ThemeService.primaryBrandColor, "Branded NavController bar tint color should be as it specified in ThemeService")
        XCTAssertEqual(view!.titleTextAttributes![NSAttributedStringKey.foregroundColor] as! UIColor, ThemeService.primaryBgColor, "Branded NavController title color should be as it specified in ThemeService")
    }
}
