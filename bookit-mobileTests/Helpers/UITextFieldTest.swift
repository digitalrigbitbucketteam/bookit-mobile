//
//  UITextFieldTest.swift
//  bookit-mobileTests
//
//  Created by Gregory Soloshchenko on 5/10/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import XCTest
@testable import bookit_mobile

class UITextFieldTest: XCTestCase {
    var view: UITextField?
    
    override func setUp() {
        super.setUp()
        
        view = UITextField(frame: .zero)
    }
    
    override func tearDown() {
        view = nil
        
        super.tearDown()
    }
    
    func testBrandAsTitle() {
        view?.brandAsTitle()
        
        XCTAssertEqual(view!.textColor, ThemeService.primaryTextColor, "Title label text should be as it specified in ThemeService")
        XCTAssertEqual(view!.font?.pointSize, ThemeService.font.title.size, "Title label font size should be as it specified in ThemeService")
    }
    
    func testBrandAsSubTitle() {
        view?.brandAsSubtitle()
        
        XCTAssertEqual(view!.textColor, ThemeService.primaryTextColor, "Subtitle label text should be as it specified in ThemeService")
        XCTAssertEqual(view!.font?.pointSize, ThemeService.font.subtitle.size, "Subtitle label font size should be as it specified in ThemeService")
    }
    
    func testBrandAsHeading() {
        view?.brandAsHeading()
        
        XCTAssertEqual(view!.textColor, ThemeService.secondaryTextColor, "Heading label text should be as it specified in ThemeService")
        XCTAssertEqual(view!.font?.pointSize, ThemeService.font.heading.size, "Heading label font size should be as it specified in ThemeService")
    }
    
    func testBrandAsSubheading() {
        view?.brandAsSubheading()
        
        XCTAssertEqual(view!.textColor, ThemeService.secondaryTextColor, "Subheading label text should be as it specified in ThemeService")
        XCTAssertEqual(view!.font?.pointSize, ThemeService.font.subheading.size, "Subheading label font size should be as it specified in ThemeService")
    }
    
}
