//
//  UIButtonTest.swift
//  bookit-mobileTests
//
//  Created by Gregory Soloshchenko on 5/10/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import XCTest
@testable import bookit_mobile

class UIButtonTest: XCTestCase {
    var view: UIButton?
    override func setUp() {
        super.setUp()
        view = UIButton(frame: .zero)
    }
    
    override func tearDown() {
        view = nil
        super.tearDown()
    }
    
    func testPrimary() {
        view?.brandAsPrimary()
        
        XCTAssertEqual(view!.titleColor(for: .normal), ThemeService.primaryButtonTextColor, "Primary button title color should be as it specified in ThemeService")
        XCTAssertEqual(view!.backgroundColor, ThemeService.primaryButtonBgColor, "Primary button background color should be as it specified in ThemeService")
    }
    
}
