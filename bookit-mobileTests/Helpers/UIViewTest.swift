//
//  UIViewTest.swift
//  bookit-mobileTests
//
//  Created by Gregory Soloshchenko on 5/15/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import XCTest
@testable import bookit_mobile

class UIViewTest: XCTestCase {
    var superview: UIView?
    var view: UIView?
    
    override func setUp() {
        super.setUp()
        superview = UIView()
        view = UIView()
    }
    
    override func tearDown() {
        superview = nil
        view = nil
        super.tearDown()
    }
    
    func testAddingSubviewWithTopLeftRightBottomConstraints() {
        guard let view = view else {
            fatalError("View is nil")
        }
        
        guard let superview = superview else {
            fatalError("Superview is nil")
        }
        
        let topAnchorEtalon: CGFloat = 101
        let leftAnchorEtalon: CGFloat = 102
        let rightAnchorEtalon: CGFloat = 103
        let bottomAnchorEtalon: CGFloat = 104
        
        superview.addSubviewWithConstraints(
            view: view,
            top: topAnchorEtalon,
            left: leftAnchorEtalon,
            bottom: bottomAnchorEtalon,
            right: rightAnchorEtalon
        )
        
        var topAnchor: CGFloat = 0
        var leftAnchor: CGFloat = 0
        var rightAnchor: CGFloat = 0
        var bottomAnchor: CGFloat = 0
        for constraint in superview.constraints {
            if constraint.identifier == "topAnchorToSubview" {
                topAnchor = constraint.constant
            } else if(constraint.identifier == "leftAnchorToSubview" ) {
                leftAnchor = constraint.constant
            } else if(constraint.identifier == "bottomAnchorToSubview" ) {
                bottomAnchor = constraint.constant
            } else if(constraint.identifier == "rightAnchorToSubview" ) {
                rightAnchor = constraint.constant
            }
        }
        
        XCTAssertEqual(topAnchor, topAnchorEtalon, "Top anchor constant should be as specified")
        XCTAssertEqual(leftAnchor, leftAnchorEtalon, "Left anchor constant should be as specified")
        XCTAssertEqual(rightAnchor, rightAnchorEtalon, "Right anchor constant should be as specified")
        XCTAssertEqual(bottomAnchor, bottomAnchorEtalon, "Bottom anchor constant should be as specified")
    }
    
    func testAddingSubviewWithWidthHeightConstraints() {
        guard let view = view else {
            fatalError("View is nil")
        }
        
        guard let superview = superview else {
            fatalError("Superview is nil")
        }
        
        let widthAnchorEtalon: CGFloat = 101
        let heightAnchorEtalon: CGFloat = 102
        
        superview.addSubviewWithConstraints(
            view: view,
            width: widthAnchorEtalon,
            height: heightAnchorEtalon
        )
        
        var widthAnchor: CGFloat = 0
        var heightAnchor: CGFloat = 0
        for constraint in superview.constraints {
            if constraint.identifier == "widthAnchorToSubview" {
                widthAnchor = constraint.constant
            } else if(constraint.identifier == "heightAnchorToSubview" ) {
                heightAnchor = constraint.constant
            }
        }
        
        XCTAssertEqual(widthAnchor, widthAnchorEtalon, "Width anchor constant should be as specified")
        XCTAssertEqual(heightAnchor, heightAnchorEtalon, "Height anchor constant should be as specified")
    }
    
    func testAddingSubviewWithCenterXAndCenterYConstraints() {
        guard let view = view else {
            fatalError("View is nil")
        }
        
        guard let superview = superview else {
            fatalError("Superview is nil")
        }
        
        let centerXAnchorEtalon: CGFloat = 101
        let centerYAnchorEtalon: CGFloat = 102
        
        superview.addSubviewWithConstraints(
            view: view,
            centerX: centerXAnchorEtalon,
            centerY: centerYAnchorEtalon
        )
        
        var centerXAnchor: CGFloat = 0
        var centerYAnchor: CGFloat = 0
        for constraint in superview.constraints {
            if constraint.identifier == "centerXAnchorToSubview" {
                centerXAnchor = constraint.constant
            } else if(constraint.identifier == "centerYAnchorToSubview" ) {
                centerYAnchor = constraint.constant
            }
        }
        
        XCTAssertEqual(centerXAnchor, centerXAnchorEtalon, "Center X anchor constant should be as specified")
        XCTAssertEqual(centerYAnchor, centerYAnchorEtalon, "Center Y anchor constant should be as specified")
    }
}
