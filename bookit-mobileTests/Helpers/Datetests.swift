//
//  Datetests.swift
//  bookit-mobileTests
//
//  Created by Tony Rizzo on 6/7/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import XCTest
@testable import bookit_mobile

class Datetests: XCTestCase {
    
    let year: Int = 2017
    let month: Int = 06
    let day: Int = 15
    let hour: Int = 13
    
    var oClock: Date {
        return DateComponents(calendar: Calendar(identifier: .gregorian), timeZone: TimeZone(abbreviation: "EST"), era: nil, year: year, month: month, day: day, hour: hour, minute: 0, second: nil, nanosecond: nil, weekday: nil, weekdayOrdinal: nil, quarter: nil, weekOfMonth: nil, weekOfYear: nil, yearForWeekOfYear: nil).date!
    }
    
    var on15Past: Date {
        return  DateComponents(calendar: Calendar(identifier: .gregorian), timeZone: TimeZone(abbreviation: "EST"), era: nil, year: year, month: month, day: day, hour: hour, minute: 15, second: nil, nanosecond: nil, weekday: nil, weekdayOrdinal: nil, quarter: nil, weekOfMonth: nil, weekOfYear: nil, yearForWeekOfYear: nil).date!
    }
    
    var on30Past: Date {
        return DateComponents(calendar: Calendar(identifier: .gregorian), timeZone: TimeZone(abbreviation: "EST"), era: nil, year: year, month: month, day: day, hour: hour, minute: 30, second: nil, nanosecond: nil, weekday: nil, weekdayOrdinal: nil, quarter: nil, weekOfMonth: nil, weekOfYear: nil, yearForWeekOfYear: nil).date!
    }
    
    var on45Past: Date {
        return DateComponents(calendar: Calendar(identifier: .gregorian), timeZone: TimeZone(abbreviation: "EST"), era: nil, year: year, month: month, day: day, hour: hour, minute: 45, second: nil, nanosecond: nil, weekday: nil, weekdayOrdinal: nil, quarter: nil, weekOfMonth: nil, weekOfYear: nil, yearForWeekOfYear: nil).date!
    }
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    

    func testRoundUpToFifteenMinutes() {

        XCTAssertNotEqual(oClock, on15Past)
        XCTAssertEqual(oClock.roundUpToFifteenMinutes(), on15Past)
        XCTAssertNotEqual(oClock.addingTimeInterval(-1), on15Past)
        XCTAssertEqual(oClock.addingTimeInterval(15*60-1).roundUpToFifteenMinutes(), on15Past)
        XCTAssertNotEqual(oClock.addingTimeInterval(15*60+1).roundUpToFifteenMinutes(), on15Past)
        
        XCTAssertNotEqual(on15Past, on30Past)
        XCTAssertEqual(on15Past.roundUpToFifteenMinutes(), on30Past)
        XCTAssertNotEqual(on15Past.addingTimeInterval(-1), on30Past)
        XCTAssertEqual(on15Past.addingTimeInterval(15*60-1).roundUpToFifteenMinutes(), on30Past)
        XCTAssertNotEqual(on15Past.addingTimeInterval(15*60+1).roundUpToFifteenMinutes(), on30Past)

        XCTAssertNotEqual(on30Past, on45Past)
        XCTAssertEqual(on30Past.roundUpToFifteenMinutes(), on45Past)
        XCTAssertNotEqual(on30Past.addingTimeInterval(-1), on45Past)
        XCTAssertEqual(on30Past.addingTimeInterval(15*60-1).roundUpToFifteenMinutes(), on45Past)
        XCTAssertNotEqual(on30Past.addingTimeInterval(15*60+1).roundUpToFifteenMinutes(), on45Past)

        XCTAssertNotEqual(on45Past, oClock.addingTimeInterval(60*60))
        XCTAssertEqual(on45Past.roundUpToFifteenMinutes(), oClock.addingTimeInterval(60*60))
        XCTAssertNotEqual(on45Past.addingTimeInterval(-1), oClock.addingTimeInterval(60*60))
        XCTAssertEqual(on45Past.addingTimeInterval(15*60-1).roundUpToFifteenMinutes(), oClock.addingTimeInterval(60*60))
        XCTAssertNotEqual(on45Past.addingTimeInterval(15*60+1).roundUpToFifteenMinutes(), oClock.addingTimeInterval(60*60))

    }
    
    func testGetDurationFrom() {
        
        XCTAssertEqual(oClock.getDurationFrom(on45Past), "45 minutes")
    }
    
    func testGetLocalizedDateTime() {
        let (dateString, timeString) = on45Past.getLocalizedDateTime()
        
        XCTAssertEqual(dateString, "Jun 15, 2017")
        XCTAssertEqual(timeString, "1:45 PM")
    }
    
    func testGetLocalizedDateTimeRelative(){
        let (dateString, timeString) = on45Past.getLocalizedDateTimeRelative()

        XCTAssertEqual(dateString, "6/15/17")
        XCTAssertEqual(timeString, "1:45 PM")
    }
}
