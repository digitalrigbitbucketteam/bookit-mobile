# Bookit mobile

## Architectural notes
### Folder structure
- Components: reusbale components that should be ready to be reused in this app or even in other apps (if they have no bussines logic within). Each component may vary but usually it has model and view (xib and/or uiview)
- Screens: Thin as possible controllers to represent particular screen. Should avoid containing bussiness logic
- Services: Theming, Logging etc. Helps determine app-wide logic
- Helpers. Reusable extentions and additions

### No-storyboard policy. Even though storyboard is awesome prototyping and visualization tool it should be avoided in case of: 
- Mid-large project 
- Team of devs contains more that one person
Use *.nib instead and keep them as small as possible
