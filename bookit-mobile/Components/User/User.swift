//
//  User.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 5/8/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import Foundation


/// User data model
struct User: Codable {
    let id: String
    let externalId: String
    let name: String
}
