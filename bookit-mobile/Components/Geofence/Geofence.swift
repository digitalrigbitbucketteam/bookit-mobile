//
//  Geofence.swift
//  bookit-mobile
//
//  Created by Cristiana Yambo on 6/15/18.
//  Copyright © 2018 Buildit@Wipro Digital. All rights reserved.
//

import CoreLocation

//Singleton class to monitor location
class Geofence: NSObject, SettingsLocation, CLLocationManagerDelegate {
    let _locationManager = {
        DispatchQueue.mainSyncSafe {
           CLLocationManager()
        }
    }()
    
    var locManager: CLLocationManager {
        get {
            return _locationManager
        }
    }

    
    //Is a singleton because it needs to be long lived without being attached to something
    //and since a user can only be in one place at a time, there can only be one
    static let instance = Geofence() //singleton
    
    override init() {
        super.init()
        //CLLocationManger settings
        locManager.distanceFilter = AppSettings.distanceFilter //distance that user needs to move to trigger a location update
        locManager.delegate = self

    }

    /// Enables or disables location monitoring
    ///
    /// - Parameter turnOn: on or off
    func monitor(_ turnOn: Bool) {
        
        if turnOn {
            //to turn on, request Auth first
            locManager.requestWhenInUseAuthorization()
            //if it is authorized
            if locationServicesEnabled() {
                //turn on the updates
                locManager.startUpdatingLocation()
            }
        } else {
            //to turn off, simply stop updating
           locManager.stopUpdatingLocation()
        }

    }
    
    /// Location service status - on or off
    ///
    /// - Returns: location service status
    func locationServicesEnabled() -> Bool {
        return CLLocationManager.locationServicesEnabled()
    }
    
    /// Calls the SettingsLocation getAllLocations
    ///
    /// - Parameter completion: completion with data
    func locationsGetAllLocations(_ completion: (([Location]?) -> ())?) {
        getAllLocations(completion)
    }
    
    /// Calls SettingsLocation selectLocation
    ///
    /// - Parameter locationName: name of the location to select
    func locationSelectLocation(locationName: String) {
        selectLocation(locationName: locationName)
    }
    /// Location Manager delegate - didUpdateLocation
    ///
    /// - Parameters:
    ///   - manager: location manager
    ///   - locations: location list
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //get the user's current location
        guard let currentLocation = locations.first else {
            return
        }

        //now get all the stored locations to compare
        locationsGetAllLocations { (locations) in
            guard let locations = locations else {
                return
            }
            //go through the locations
            for (_,location) in locations.enumerated() {
                if let coords = location.getCoordinates() {
                    //get the distance from the user to the stored location
                    let distance = currentLocation.distance(from: coords)

                    //if the user is within the radius
                    if distance < AppSettings.geoFenceRadius * 1000 {
                        //select the location
                        self.locationSelectLocation(locationName: location.name)
                        break
                    }
                }
            }
        }
    }
    
    /// Location manager failure delegate
    ///
    /// - Parameters:
    ///   - manager: location manager
    ///   - error: error
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        //TODO: failure _should_ do something
        print(error.localizedDescription)
    }
    
    /// Location manager authorization delegate
    ///
    /// - Parameters:
    ///   - manager: location manager
    ///   - status: status
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        //check to see if user is authorized
        if status == .authorizedWhenInUse {
            //location is authorized
            //TODO: do we need to do something here ?
        }
    }
}
