//
//  Test.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 5/2/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit


/// Represents animated success view: circle and checkmark
class SuccessAnimation: UIView {
    
    /// Color of the stroke of checkmark
    var checkmarkStrokeColor = UIColor.white.cgColor
    
    /// Storke width of the checkmark
    var checkmarkStrokeWidth: CGFloat = 3
    
    /// Color of the circle
    var circleFillColor = UIColor.black.cgColor
    
    /// Animation delegate
    var delegate: CAAnimationDelegate?

    /// Circle shape layer
    let circle = CAShapeLayer()
    
    /// Checkmark shape layer
    let checkmark = CAShapeLayer()
    
    /// Basic init
    ///
    /// - Parameters:
    ///   - frame: frame that determies sizes of the view
    ///   - lineColor: Checkmark line color
    ///   - lineWidth: Checkmark line width
    ///   - circleColor: Circle color
    ///   - delegate: Who will react on animation?
    init(frame: CGRect?, lineColor: CGColor?, lineWidth: CGFloat?, circleColor: CGColor?, delegate: CAAnimationDelegate?){
        if(frame != nil){
            super.init(frame: frame!)
        } else {
            super.init(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        }
        
        if(lineColor != nil) {
            self.checkmarkStrokeColor = lineColor!
        }
        
        if(lineWidth != nil) {
            self.checkmarkStrokeWidth = lineWidth!
        }
        
        if(circleColor != nil) {
            self.circleFillColor = circleColor!
        }
        
        if(delegate != nil) {
            self.delegate = delegate!
        }

        commonInit()
    }
    
    /// Init from code
    ///
    /// - Parameter frame: frame that determies sizes of the view
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()
    }
    
    /// Init from nib
    ///
    /// - Parameter aDecoder: actual decoder
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    /// Actual preparation of this entity
    func commonInit(){
        prepareCircle()
        prepareCheckmark()
    }
    
    /// Prepare circle with color and path and add it to the view
    func prepareCircle(){
        circle.fillColor = circleFillColor
        circle.path = UIBezierPath(ovalIn: CGRect(x: frame.width / 2, y: frame.height / 2, width: 0.0, height: 0.0)).cgPath

        layer.addSublayer(circle)
    }
    
    /// Prepare checkmark with color and width and add it to the view
    func prepareCheckmark(){
        checkmark.fillColor = UIColor.clear.cgColor
        checkmark.strokeColor = checkmarkStrokeColor
        checkmark.lineWidth = checkmarkStrokeWidth
        
        layer.addSublayer(checkmark)
    }
    
    /// Creates checkmark's path
    ///
    /// - Parameters:
    ///   - start: Start pont
    ///   - first: First keypoint
    ///   - second: Second key point
    /// - Returns: created path
    func createPath(start: CGPoint, first: CGPoint, second: CGPoint) -> UIBezierPath {
        let path = UIBezierPath()
        path.move(to: start)
        path.addLine(to: first)
        path.addLine(to: second)
        
        return path
    }
    
    /// Start the animation
    func start(){
        let startPoint = CGPoint(x: frame.width * 0.8, y: frame.height * 0.35)
        let endLine1 = CGPoint(x: frame.width * 0.45 , y: frame.height * 0.75)
        let endLine2 = CGPoint(x: frame.width * 0.3 , y: frame.height * 0.55)
        
        checkmark.path = createPath(start: startPoint, first: startPoint, second: startPoint).cgPath
        
        let firstLinePath = createPath(start: startPoint, first: endLine1, second: endLine1)
        let secondLinePath = createPath(start: startPoint, first: endLine1, second: endLine2)

        // prepare circle scale animation
        let scale = CABasicAnimation(keyPath: "path")
        scale.fromValue = circle.path
        scale.duration = 0.2
        scale.toValue = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: frame.width, height: frame.height)).cgPath
        
        // prepare circle fading animation
        let fade = CABasicAnimation(keyPath: "opacity")
        fade.duration = 0.2
        fade.fromValue = 0
        fade.toValue = 1
    
        // prepare circle total animation
        let circleAnimation = CAAnimationGroup()
        circleAnimation.duration = fade.duration
        circleAnimation.repeatCount = 1
        circleAnimation.animations = [scale, fade]
        circleAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        circleAnimation.fillMode = kCAFillModeForwards
        circleAnimation.isRemovedOnCompletion = false
        
        circle.add(circleAnimation, forKey: nil)

        // prepare drawing first line animation
        let line1 = CABasicAnimation(keyPath: "path")
        line1.duration = 0.2
        line1.fromValue = checkmark.path!
        line1.toValue = firstLinePath.cgPath
        line1.beginTime = circleAnimation.duration + 0.2
        line1.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        
        // prepare drawing second line animation
        let line2 = CABasicAnimation(keyPath: "path")
        line2.duration = 0.2
        line2.fromValue = firstLinePath.cgPath
        line2.toValue = secondLinePath.cgPath
        line2.beginTime = line1.beginTime + line1.duration
        line2.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        
        // prepare total checkmark animation
        let checkmarkAnimation = CAAnimationGroup()
        checkmarkAnimation.duration = line2.beginTime + line2.duration
        checkmarkAnimation.repeatCount = 1
        checkmarkAnimation.animations = [line1, line2]
        checkmarkAnimation.fillMode = kCAFillModeForwards
        checkmarkAnimation.isRemovedOnCompletion = false
        
        if(delegate != nil) {
            checkmarkAnimation.delegate = delegate
        }
        
        checkmark.add(checkmarkAnimation, forKey: nil)
    }
}
