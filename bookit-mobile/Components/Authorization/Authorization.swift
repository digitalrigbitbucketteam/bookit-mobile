//
//  Authorization.swift
//  bookit-mobile
//
//  Created by Cristiana Yambo on 5/10/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit


/// Authorization Notifications
extension Notification.Name {
    static let loggedIn = Notification.Name("BookitAuthorizationLogin")
    static let loggedOut = Notification.Name("BookitAuthorizationLogout")
    static let loginCancelled = Notification.Name("BookitAuthorizationLoginCancelled")
}

/// Authorization class, handles calling MS Services to get authorization credentials
class Authorization: LoginMessagingAction {

    /// Completion handler from getToken
    static var completionHandler : ((AuthorizationToken?) -> ())?
    
    /// Stored AuthorizationToken
    static var authorizationToken : AuthorizationToken?
    
    /// Authorization Context
    let authorizationContext : AuthorizationContext
    
    /// Holds the loginViewController (which displays a login button and some messaging)
    var loginViewController : LoginViewController?
    
    /// Holds the loginMessagingViewController which displays messaging on top of the MS login process to hide their controls
    var loginMessagingViewController : LoginMessagingViewController?
    
    let authorizationView: AuthorizationView
    
    let notificationCenter: NotificationCenter
    /// Initializes Authorization
    ///
    /// - Throws: AuthorizationError
    init() throws {
        //load the Authorization Context
        do {
           try authorizationContext = AuthorizationContext.init()
        }
        authorizationView = AuthorizationView.init()
        notificationCenter = NotificationCenter.default
    }
    
    /// Initialization with injection
    ///
    /// - Parameters:
    ///   - authorizationContext: Authorization context
    ///   - authorizationView: authorization view
    init(authorizationContext: AuthorizationContext, authorizationView: AuthorizationView, notificationCenter: NotificationCenter) {
        self.authorizationContext = authorizationContext
        self.authorizationView = authorizationView
        self.notificationCenter = notificationCenter
    }
    
    /// Gets the AuthorizationToken from MS Services
    ///
    /// - Parameter completion: completion callback, contains token
    /// - Throws: throws simple AuthorizationError
    func acquireUser(completion: @escaping (AuthorizationToken?) -> ()) throws  {
        //save the handler for later
        // TODO: This probably should not be a static variable. Does this value really need to be shared amongs all objects of this class.
        Authorization.completionHandler = completion
        //load Authorization
        try authorizationContext.loadUser(completionBlock: checkToken, interactionRequiredBlock: {
            self.showLoginScreen()
        })
    }
    
    /// Displays the login screen
    func showLoginScreen() {
        authorizationView.showLoginScreen(loginMessagingDelegate: self, displayBlock: acquireToken)
    }
    
    /// Acquires Token from MS
    func acquireToken() {
        authorizationContext.acquireToken(completionBlock: checkToken)
    }
    
    /// Checks the validity of the token, runs the completion callback and posts a .loggedIn notification
    ///
    /// - Parameters:
    ///   - result: AuthorizationToken
    ///   - silent: Bool to indicate it is a silent login
    ///   - error: Any error with login will be posted here
    private func checkToken(result: AuthorizationToken?, silent: Bool, error: Error?) {
        
        authorizationView.removeLoginViews()
        
        //set token to nil before checking anything to prevent false logins
        Authorization.authorizationToken = nil
        
        //make sure the saved handler is good
        guard let handler = Authorization.completionHandler else { return }
        
        //check for any error
        if let error = error {
            debugPrint(error.localizedDescription)
            
            //if so, call the handler with a nil token
            handler(nil)
        } else {
            if let result = result {
                //set the token
                Authorization.authorizationToken = result
                handler(result)
                //post a login notification only if it wasn't a silent login
                if !silent {
                    notificationCenter.post(name: .loggedIn, object: nil)
                }
                
            } else {
                handler(nil)
            }
        }
    }
    
    /// Checks to see if the user is logged in
    ///
    /// - Returns: logged in status
    func isLoggedIn() -> Bool {
        if let _ = Authorization.authorizationToken {
            return true
        }
        return false
    }
    
    /// Gets the current AuthorizationToken
    ///
    /// - Returns: AuthorizationToken
    func getToken() -> AuthorizationToken? {
        return Authorization.authorizationToken
    }
    
    /// Logs out the user and posts a .loggedOut notification when done
    ///
    /// - Throws: AuthorizationError
    func logout() throws {
        Authorization.authorizationToken = nil
        try authorizationContext.logout()
        //send a logout notification
        notificationCenter.post(name: .loggedOut, object: nil)
    }
    
    /// LoginMessagingActionDelegate - Called when login was cancelled
    func loginCancelled() {
        authorizationContext.cancelLogin()
        notificationCenter.post(name: .loginCancelled, object: nil)
    }
}
