//
//  AuthorizationContext.swift
//  bookit-mobile
//
//  Created by Cristiana Yambo on 5/18/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import Foundation
import MSAL
/// Contains the MSAL token

struct AuthorizationToken {
    let tokenResult : MSALResult

    /// Gets the token string for use in API calls
    ///
    /// - Returns: token string
    func getTokenString() -> String? {
        if let tokenString = tokenResult.idToken {
            return "Bearer \(tokenString)"
        }
        return nil
    }
    
    /// Gets the username from token
    ///
    /// - Returns: username
    func getUserName() -> String? {
        return tokenResult.user.name
    }
    
    /// Gets the user's unique ID
    ///
    /// - Returns: unique ID string
    func getUserId() -> String? {
        return tokenResult.user.uid
    }
}

/// class to abstract any MSAL functionality
class AuthorizationContext {
    
    /// MS application context
    private var applicationContext : MSALPublicClientApplication
    
    /// completion handler for loadUser
    private static var completionHandler : ((AuthorizationToken?, Bool, AuthorizationError?) -> ())?
    
    //var to indicate the token was a silent acquire
    private static var tokenAcquiredSilently = false
    /// Initializes the struct
    ///
    /// - Throws: AuthorizationError
    init() throws {
        //load the MS library
        do {
            try applicationContext = MSALPublicClientApplication.init(clientId: AppSettings.loginClientId, authority: AppSettings.loginUrl)
        } catch {
            throw AuthorizationError.init(kind: .loadContextFailed)
        }
    }
    
    /// Init with applicationContext
    ///
    /// - Parameter context: MSALPublicClientApplication
    init(applicationContext context: MSALPublicClientApplication) {
        applicationContext = context
    }
    
    /// Loads user
    ///
    /// - Parameters:
    ///   - completionBlock: called when user is loaded
    ///   - interactionRequiredBlock: called when 'interaction' is required - i.e. need to show the login window
    /// - Throws: AuthorizationError
    func loadUser(completionBlock: @escaping (AuthorizationToken?,Bool,AuthorizationError?) -> Void, interactionRequiredBlock: @escaping () -> Void) throws {
        AuthorizationContext.completionHandler = completionBlock
        do {
            //Check to see if there is a stored user
            if try isEmpty() {
                //if not, load up the login view
                throw getInteractionError()
                //otherwise acquire the token silently
            } else {
                try acquireTokenSilently()
            }
            
        } catch let error as NSError {
            //check to see if we need to load up the view
            if error.code == MSALErrorCode.interactionRequired.rawValue {
                interactionRequiredBlock()
            } else {
                throw AuthorizationError.init(kind: .acquireTokenFailed)
            }
        } catch {
            //Throw an error because token cannot be acquired
            throw AuthorizationError.init(kind: .acquireTokenFailed)
        }
    }
    
    /// Gets the authorization token
    ///
    /// - Parameter completionBlock: called when token retrieval is complete
    func acquireToken(completionBlock: @escaping (AuthorizationToken?,Bool,AuthorizationError?) -> Void) {
        AuthorizationContext.tokenAcquiredSilently = false
        //save the completion block
        AuthorizationContext.completionHandler = completionBlock
        //call the MS service
        applicationContext.acquireToken(forScopes: AppSettings.loginScopes, completionBlock: tokenCompletionBlock)
    }
    
    /// Acquires the user token in the backgound
    private func acquireTokenSilently() throws {
        AuthorizationContext.tokenAcquiredSilently = true
        //check to see if there is a stored user
        if let user = try getUser() {
            applicationContext.acquireTokenSilent(forScopes: AppSettings.loginScopes, user: user , completionBlock: tokenCompletionBlock)
        } else {
            //if there is no user, or it failed send interactionRequired error
            throw getInteractionError()
        }
        
    }
    
    /// Called from MS services when token acquire ends
    ///
    /// - Parameters:
    ///   - result: MSALResult from service
    ///   - error: any error from service
    private func tokenCompletionBlock(result :MSALResult?, error: Error?) {
        
        /// check to see if there is a stored handler
        guard let handler = AuthorizationContext.completionHandler else {
            return
        }

        /// check for an error
        if let error = error {
            debugPrint(error.localizedDescription)
            
            /// then send an AuthorizationError to the handler
            let authError = AuthorizationError.init(kind: .externalError)
            handler(nil,AuthorizationContext.tokenAcquiredSilently,authError)
        } else {
            /// Check for a valid result
            if let result = result {
                let token = AuthorizationToken.init(tokenResult: result)
                handler(token, AuthorizationContext.tokenAcquiredSilently, nil)
            } else {
                let authError = AuthorizationError.init(kind: .resultFailed)
                handler(nil, AuthorizationContext.tokenAcquiredSilently, authError)
            }
        }
    }
    
    /// Gets the first user from context
    ///
    /// - Returns: MSALUser
    /// - Throws: MSAL error
    func getUser() throws -> MSALUser? {
        return try applicationContext.users().first
    }
    
    /// Checks to see if the user cache is empty
    ///
    /// - Returns: isEmpty
    /// - Throws: MSAL error
    func isEmpty() throws -> Bool {
         return try applicationContext.users().isEmpty
    }
    
    /// Logs the user out
    ///
    /// - Throws: AuthorizationError
    func logout() throws {
        do {
            // Removes all tokens from the cache for the current user
            try applicationContext.remove(applicationContext.users().first)
        } catch let error {
            debugPrint(error.localizedDescription)
            throw AuthorizationError.init(kind: .logoutFailed)
        }
    }
    
    /// Returns the specific error for interactionRequired used in MSAL
    ///
    /// - Returns: interaction required error
    private func getInteractionError() -> NSError {
       return NSError.init(domain: "MSALErrorDomain", code: MSALErrorCode.interactionRequired.rawValue, userInfo: nil)
    }
    
    /// Logs the user out
    func cancelLogin() {
        MSALPublicClientApplication.cancelCurrentWebAuthSession()
    }
    
}
