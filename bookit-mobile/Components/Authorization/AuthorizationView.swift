//
//  AuthorizationViewModel.swift
//  bookit-mobile
//
//  Created by Cristiana Yambo on 5/22/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit

class AuthorizationView {
    /// Holds the loginViewController (which displays a login button and some messaging)
    var loginViewController : LoginViewController?
    
    /// The view singletons access
    var vs: viewSingletons = viewSingleton.init()
    
    /// Holds the loginMessagingViewController which displays messaging on top of the MS login process to hide their controls
    var loginMessagingViewController : LoginMessagingViewController?
    
    /// Shows the overlay messaging
    ///
    /// - Parameters:
    ///   - loginMessagingDelegate: login messaging delegate - called when cancelled
    ///   - displayBlock: block that is called after messaging is loaded
    func displayOverlayMessaging(loginMessagingDelegate: LoginMessagingAction, displayBlock: () -> Void) {
        let loginMessaging = LoginMessagingViewController.init()
        loginMessaging.actionDelegate = loginMessagingDelegate
        loginMessagingViewController = loginMessaging
        if let w = vs.keyWindow {
            w.addSubview(loginMessaging.view)
            w.bringSubview(toFront: loginMessaging.view)
            displayBlock()
        } else {
            debugPrint("No Key Window")
        }
    }
    
    /// Displays the login screen
    ///
    /// - Parameters:
    ///   - loginMessagingDelegate: loginMessaging delegate
    ///   - displayBlock: what gets run after messaging is shown
    func showLoginScreen(loginMessagingDelegate: LoginMessagingAction, displayBlock: () -> Void)  {
        
        //check to see if there is a root view controller, if so, then just display the ms login
        if let _ = vs.rootViewController {
            displayOverlayMessaging(loginMessagingDelegate: loginMessagingDelegate, displayBlock: displayBlock)
        } else {
            //if there isn't one, then we just opened the app and display the login screen
            let lvc = LoginViewController()
            loginViewController = lvc
            vs.rootViewController = UINavigationController(rootViewController:  lvc)
        }
    }

    
    /// Removes any of the login views from the screen
    func removeLoginViews() {
        
        /// remove the login view
        if let loginViewController = loginViewController {
            loginViewController.dismiss(animated: true) {
                self.loginViewController = nil
            }
        }
        
        /// remove the messaging view
        if let loginMessagingViewController = loginMessagingViewController {
            loginMessagingViewController.view.removeFromSuperview()
            self.loginMessagingViewController = nil
        }
    
    }
}
