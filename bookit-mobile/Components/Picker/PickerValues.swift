//
//  PickerValues.swift
//  bookit-mobile
//
//  Created by Cristiana Yambo on 6/8/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import Foundation

struct PickerValues {
    /// Opitions for start time
    static let timeValues = [
        [
            PickerValue(name: "8", value: "8"),
            PickerValue(name: "9", value: "9"),
            PickerValue(name: "10", value: "10"),
            PickerValue(name: "11", value: "11"),
            PickerValue(name: "12", value: "12"),
            PickerValue(name: "13", value: "13"),
            PickerValue(name: "14", value: "14"),
            PickerValue(name: "15", value: "15"),
            PickerValue(name: "16", value: "16"),
            PickerValue(name: "17", value: "17"),
            PickerValue(name: "18", value: "18"),
            ],
        [
            PickerValue(name: ":", value: nil)
        ],
        [
            PickerValue(name: "00", value: "00"),
//            PickerValue(name: "15", value: "15"),
            PickerValue(name: "30", value: "30"),
//            PickerValue(name: "45", value: "45"),
            ]
    ]
    
    /// Options for duration
    static let durationValues = [[
        PickerValue(name: "30 min", value: 30.0),
        PickerValue(name: "1 hour", value: 60.0),
        PickerValue(name: "1 hour 30 min", value: 90.0),
        PickerValue(name: "2 hours", value: 120.0),
        PickerValue(name: "2 hours 30 min", value: 150.0),
        PickerValue(name: "3 hours", value: 180.0),
        PickerValue(name: "3 hours 30 min", value: 210.0),
        PickerValue(name: "4 hours", value: 240.0),
        PickerValue(name: "4 hours 30 min", value: 270.0),
        PickerValue(name: "5 hours", value: 300.0),
        PickerValue(name: "5 hours 30 min", value: 330.0),
        PickerValue(name: "6 hours", value: 360.0),
        PickerValue(name: "6 hours 30 min", value: 390.0),
        PickerValue(name: "7 hours", value: 420.0),
        PickerValue(name: "7 hours 30 min", value: 450.0),
        PickerValue(name: "All day", value: 480.0)
        ]]
    
    
    /// Options for date
    static let dateValues = [Picker.prepareValuesForNDays(n: 5)]
    
    /// Options for capacity
    static let capacityValues = [[
        PickerValue(name: "1", value: 1),
        PickerValue(name: "2", value: 2),
        PickerValue(name: "3", value: 3),
        PickerValue(name: "4", value: 4),
        PickerValue(name: "5", value: 5),
        PickerValue(name: "6", value: 6),
        PickerValue(name: "7", value: 7),
        PickerValue(name: "8", value: 8),
        PickerValue(name: "9", value: 9),
        PickerValue(name: "10", value: 10),
        PickerValue(name: "11", value: 11),
        PickerValue(name: "12", value: 12),
        PickerValue(name: "13", value: 13),
        ]]
    
    /// Selected event date
    static let selectedDate = [
        PickerIndex(component: 0, row: 0)
    ]
    
    /// Selected event start time
    static let selectedTime = [
        PickerIndex(component: 0, row: 3),
        PickerIndex(component: 2, row: 0)
    ]
    
    /// Selected event duration
    static let selectedDuration = [PickerIndex(component: 0, row: 1)]
    
    /// Selected room capacity for the event
    static let selectedCapacity = [PickerIndex(component: 0, row: 4)]
    
    /// Width setting for Time picker
    static let widthForTimePicker = [
        PickerComponentWidth(component: 0, width: 0.2),
        PickerComponentWidth(component: 1, width: 0.1),
        PickerComponentWidth(component: 2, width: 0.2)
    ]

}
