//
//  Picker.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 4/30/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit

/// Represents selected value of the Picker
struct PickerIndex {
    let component: Int
    let row: Int
}

/// Represents value structure of Picker
struct PickerValue {
    let name: String
    let value: Any?
}

/// Represents Picker's component width
struct PickerComponentWidth {
    let component: Int
    let width: CGFloat
}

/// Custom picker implemetnation
/// Combines UITextField, UIPickerView and UIToolbar
/// Supports callbacks when user finished making a choice or denied it
class Picker: UITextField, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    /// Values of the picker
    var values: [[PickerValue]] = []
    
    /// Current selected index
    var selectedIndexes = [PickerIndex]()
    
    /// Width of each row
    var componentWidths: [PickerComponentWidth]?
    
    /// Called when user presses "Done" in toolbar
    var doneCallback: ((_ selectedIndexes: [PickerIndex]) -> ())?
    
    /// Called when user presses "Cancel"
    var cancelCallback: (() -> ())?
    
    /// When textfield is updated with new data separator is added to resulted string between row values
    var separator = ""
    
    /// Determines if textfield should been updated after user presess "Done"
    var updateTextField = true
    
    /// Ref to picker view
    let pickerView = UIPickerView(frame: .zero)
    
    /// Setting up the Picker
    ///
    /// - Parameters:
    ///   - values: Array of values for each row
    ///   - selected: Determines what value is selected now
    ///   - widths: Array of ros widths
    ///   - done: Done callback
    ///   - cancel: Cancel callback
    func setup(values: [[PickerValue]], selectedIndexes: [PickerIndex]) {
        // Set data
        self.values = values
        self.selectedIndexes = selectedIndexes
        
        //setting the UITextFieldDelegate to self
        self.delegate = self
        
        // Prepare UIPickerView
        self.pickerView.dataSource = self
        self.pickerView.delegate = self
        self.inputView = pickerView
        
        // Select by default
        self.setSelected()
        self.updateTextFiledText()
        
        // Prepare ToolBar
        self.prepareToolBar()
    }

    /// Setup toolbar and it's buttons
    func prepareToolBar(){
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(done(sender:)))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancel(sender:)))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        self.inputAccessoryView = toolBar
    }
    
    /// Sets number of components
    ///
    /// - Parameter pickerView: Picker View
    /// - Returns: Number of components
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return values.count
    }
    
    /// Sets number of rows in each component
    ///
    /// - Parameters:
    ///   - pickerView: Picker view
    ///   - component: Number of component
    /// - Returns: Number of rows in particular component
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return values[component].count
    }
    
    /// Sets a string value (visible title) for each element of picker
    ///
    /// - Parameters:
    ///   - pickerView: Picker view
    ///   - row: Row number
    ///   - component: Component number
    /// - Returns: Title for this row in component
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if !values.indices.contains(component) {
            return "??"
        }
        
        if !values[component].indices.contains(row) {
            return "??"
        }
        
        return values[component][row].name
    }

    /// Sets height
    ///
    /// - Parameters:
    ///   - pickerView: Picker view
    ///   - component: Component number
    /// - Returns: Height for this component
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50
    }
    
    /// Sets width for each row in component
    ///
    /// - Parameters:
    ///   - pickerView: Picker view
    ///   - component: Component number
    /// - Returns: Width for this row in the component
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        guard let width = componentWidths else {
            return frame.width / CGFloat(values.count)
        }
        
        if let i = width.index(where: { $0.component == component }) {
            return frame.width * width[i].width
        }
        
        return frame.width
    }
    
    /// Update pickerView with selected info
    func setSelected(){
        for index in selectedIndexes {
            pickerView.selectRow(index.row, inComponent: index.component, animated: false)
        }
    }
    
    /// Update selected
    func updateSelected(){
        var selected = [PickerIndex]()
        for (component, _) in values.enumerated() {
            let row = pickerView.selectedRow(inComponent: component)
            selected.append(PickerIndex(component: component, row: row))
        }
        
        selectedIndexes = selected
    }
    
    /// Update textfield if necessary
    func updateTextFiledText(){
        if(!updateTextField){
            return
        }
        
        var selectedForInput = [String]()
        for (component, value) in values.enumerated() {
            let row = pickerView.selectedRow(inComponent: component)
            selectedForInput.append(value[row].name)
        }
        
        self.text = selectedForInput.joined(separator: self.separator)
    }
    
    
    /// Return array string values
    ///
    /// - Returns: Array of values
    func getValues<T>() -> [T] {
        var values = [T]()
        for index in selectedIndexes {
            guard let value = self.values[index.component][index.row].value as? T else {
                continue
            }
            
            values.append(value)
        }
        
        return values
    }
    
    /// When user presses "Done"
    ///
    /// - Parameter sender: Done button
    @objc func done(sender: UIButton) {
        self.resignFirstResponder()
        
        updateSelected()
        updateTextFiledText()
        
        // If there is a callback: call it back!
        if let doneCallback = self.doneCallback {
            doneCallback(selectedIndexes)
        }
    }
    
    /// When user presses "Cancel"
    ///
    /// - Parameter sender: Cancel button
    @objc func cancel(sender: UIButton) {
        self.resignFirstResponder()
        
        setSelected() // revert pickerView values to stored ones
        
        if let cancelCallback = self.cancelCallback {
            cancelCallback()
        }
    }
    
    /// UITextFieldDelegate is always set to return false to disable any keyboard input
    ///
    /// - Parameters:
    ///   - textField: default
    ///   - range: default
    ///   - string: default
    /// - Returns: always returns false
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
    
}

extension Picker {
    static func prepareValuesForNDays(starting date: Date = Date(), n: Int) -> [PickerValue]{
        var result = [PickerValue]()
        var last: Date?
        var index: Int = 0
        let calendar = NSCalendar.current
        while index < n {
            var current = date
            if last != nil {
                current = last!.getStartOfNextDay()
            }
            
            last = current
            
            if calendar.isDateInWeekend(current) {
                continue
            }
            
            if calendar.isDateInToday(current) {
                result.append(PickerValue(name: "Today", value: Formatter.pickerValue.string(from: current)))
            } else if(calendar.isDateInTomorrow(current)) {
                result.append(PickerValue(name: "Tomorrow", value: Formatter.pickerValue.string(from: current)))
            } else {
                result.append(PickerValue(name: Formatter.pickerName.string(from: current), value: Formatter.pickerValue.string(from: current)))
            }
            
            index += 1
        }
        
        return result
    }
    
    /// Selects the passed in time in the Picker
    ///
    /// - Parameter date: date to select
    /// - Returns: picker index array to use to select value in picker
    static func getSelectedIndexForDate(date: Date) -> [PickerIndex] {

        //get the day start hour
        guard let dayStartHour = Int((PickerValues.timeValues[0][0].value as? String) ?? "0") else {
            return [PickerIndex]()
        }
        //get the day end hour
        guard let dayEndHour = Int(PickerValues.timeValues[0].last?.value as? String ?? "0") else {
            return [PickerIndex]()
        }
        
        //gets the inteval by selecting the second item in the timeValues
        guard let minuteInterval = Int((PickerValues.timeValues[2][1].value as? String) ?? "0") else {
            return [PickerIndex]()
        }
        
        //get the date hours
        let hours = Calendar.current.component(.hour, from: date)
        
        //check to see if the hours are out of range, if so, return an empty index
        if hours < dayStartHour || hours > dayEndHour {
            return [PickerIndex]()
        }
        let minutes = Calendar.current.component(.minute, from: date)
        

        //get the indicies
        let hourIndex = hours - dayStartHour
        let minuteIndex = Int(minutes / minuteInterval)
        
        return [PickerIndex.init(component: 0, row: hourIndex),PickerIndex.init(component: 2, row: minuteIndex)]
    }
}

extension Formatter {
    static let pickerValue: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }()
    
    static let pickerName: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EE MMMM d"
        return formatter
    }()
}
