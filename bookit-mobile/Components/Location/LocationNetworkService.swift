//
//  LocationNetworkService.swift
//  bookit-mobile
//
//  Created by Cristiana Yambo on 6/4/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import Foundation

/// This service is responsible for handling Network APi calls
class LocationNetworkService: LocationNetworkServiceProtocol, SettingsServer {
    
    private var backendNetwork: NetworkAccessProtocol
    
    init(backendNetwork: NetworkAccessProtocol? = nil) {
        self.backendNetwork = backendNetwork ?? BackendNetworkAccess()
    }
    
    func fetch(completion: @escaping ([Location]?, LocationNetworkError?) -> ()) {
        
        // get the data
        guard let urlString = getCurrentServerUrl()
            , var urlComponents = URLComponents(string: urlString) else {
            completion(nil,LocationNetworkError(code: nil, message: "Could not create the url via string"))
            return
        }
        
        urlComponents.path = "/v1/location"
        
        guard let url = urlComponents.url else {
            completion(nil, LocationNetworkError(code: nil, message: "Could not create the url via the components"))
            return
        }
            
        backendNetwork.fetch(from: url) { (data, error) in
            if let error = error {
                completion(nil, LocationNetworkError(code: nil, message: error.localizedDescription))
                return
            }
            
            guard let data = data else {
                completion(nil, LocationNetworkError(code: nil, message: "No data was returned viaz the network request"))
                return
            }
            
            guard let result = try? JSONDecoder().decode([Location].self, from: data) else {
                let error = LocationNetworkError(code: nil, message: "Cannot parse")
                
                let parsedData = try? JSONSerialization.jsonObject(with: data, options: [])
                if parsedData != nil {
                    debugPrint("Parsed data: ", parsedData!)
                } else {
                    debugPrint("Parsed data is nil")
                }
                
                completion(nil, error)
                return
            }
            completion(result, nil)
        }
    }
}
