//
//  Location.swift
//  bookit-mobile
//
//  Created by Cristiana Yambo on 6/4/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import Foundation
import CoreLocation


/// Represents an office location
struct Location: Codable, Equatable {
    let id: String
    let name: String
    let timeZone: String
    let timezoneDisplayName: String
    
    
    /// Gets the CLLocation of a Location
    ///
    /// - Returns: CLLocation or nil
    func getCoordinates() -> CLLocation? {
        return AppSettings.coordinates[id]
    }
}
