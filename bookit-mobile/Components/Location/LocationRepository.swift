//
//  LocationRepository.swift
//  bookit-mobile
//
//  Created by Cristiana Yambo on 6/4/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import Foundation

/// Retrieval and caching of Locations
class LocationRepository {
    
    /// Service resposible for networking
    let networkService: LocationNetworkServiceProtocol
    
    static var keyedRepository: [String : Location]? = nil
    static var repository: [Location]? = nil
    
    /// Init
    ///
    /// - Parameter networkService: Network service
    init(networkService: LocationNetworkServiceProtocol) {
        self.networkService = networkService
    }
    
    static func getCachedLocations() -> [Location]? {
        //TODO: use real caching, along with ttls etc
        return LocationRepository.repository
    }
    
    /// Load all locations
    ///
    /// - Parameter completion: Completion handler
    func getAll(_ completion: @escaping ([Location]?, String?) -> ()){
        if let repository = LocationRepository.repository {
            completion(repository, nil)
            return
        }
        self.networkService.fetch { (data, error) in
            if let error = error {
                completion(nil, error.message)
                return
            }
            
            guard let data = data else {
                completion(nil, "Locations are nil")
                return
            }
            self.setRepository(data: data)
            completion(data, nil)
        }
    }
    
    /// Sets the repository data
    ///
    /// - Parameter data: Location's array to set
    internal func setRepository(data: [Location]) {
        //set the repo
        LocationRepository.repository = data
        //and the keyed repo
        LocationRepository.keyedRepository = [String: Location]()
        data.forEach({ (l) in
            LocationRepository.keyedRepository?[l.name] = l
        })
    }
    
    /// Gets the location from the repo
    ///
    /// - Parameter name: name of the location to return
    /// - Returns: matched location
    func getLocationForName(name: String) -> Location? {
        return LocationRepository.keyedRepository?[name]
    }
}
