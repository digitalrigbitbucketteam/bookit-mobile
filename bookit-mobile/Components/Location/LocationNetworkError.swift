//
//  LocationNetworkError.swift
//  bookit-mobile
//
//  Created by Tony Rizzo on 6/8/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import Foundation

/// Location Network Error
struct LocationNetworkError : NetworkError, Error {
    var code: Int?
    var message: String?
}
