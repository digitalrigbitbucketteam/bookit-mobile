//
//  LocationNetworkServiceProtocol.swift
//  bookit-mobile
//
//  Created by Tony Rizzo on 6/8/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import Foundation

/// Blueprint for entities that implement Bookable API network call
protocol LocationNetworkServiceProtocol {
    /// Function that implements request to remote API in order to grab data
    ///
    /// - Parameters:
    ///   - path: Relative path to call
    ///   - queryItems: Items of query string
    ///   - completion: Completion handler
    func fetch(completion: @escaping ([Location]?, LocationNetworkError?) -> ())
    
}
