//
//  MyBooking.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 5/3/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit


/// Booking Model View
class MyBookingView: UIView {
    
    /// Main view
    @IBOutlet var contentView: UIView!
    
    /// Booking timeline
    @IBOutlet weak var timeLabel: UILabel!
    
    /// Name of the booking (aka event name)
    @IBOutlet weak var nameLabel: UILabel!
    
    /// Name of the bookabale
    @IBOutlet weak var roomNameLabel: UILabel!
    
    /// Capacity of the bookable
    @IBOutlet weak var capacityLabel: UILabel!
    
    /// Name of nib file associated with this view
    private let nibName = "MyBookingView"
    
    /// Init from code
    ///
    /// - Parameter frame: frame that determies sizes of the view
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    /// Init from nib
    ///
    /// - Parameter aDecoder: actual decoder
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    /// Actual preparation of this entity
    func commonInit(){
        Bundle.main.loadNibNamed(nibName, owner: self, options: nil)
        
        // Handy way of adding view with contstraints pined to superview
        addSubviewWithConstraints(
            view: contentView,
            top: 0,
            left: 0,
            bottom: 0,
            right: 0
        )
        
        brandit()
    }
    
    /// Setup view with data.
    /// Pay attention that this method might be called several times during lifetime of the app
    ///
    /// - Parameter booking: Data model
    func setup(for booking: Booking){

        let (sd,st) = booking.start.getLocalizedDateTimeRelative()
        let (_,et) = booking.end.getLocalizedDateTimeRelative()

        timeLabel.text = "\(sd): \(st) - \(et)"
        nameLabel.text = booking.subject
        roomNameLabel.text = "\(booking.bookable.name.capitalizingFirstLetter())"
        capacityLabel.text = "Holds \(booking.bookable.capacity) people"
    }
    
    /// Format date from string for proper timezone
    ///
    /// - Parameters:
    ///   - str: DateTime string
    ///   - timeZoneAbbr: Time Zone abbreviation
    /// - Returns: Date object formatted from string
    func prepareDate(str: String, timeZoneAbbr: String) -> Date? {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone(abbreviation: timeZoneAbbr)
        formatter.dateFormat = "yyyy/MM/dd'T'HH:mm"
        
        return formatter.date(from: str)
    }
    
    /// Apply all branding styles to this view
    func brandit(){
        timeLabel.brandAsSubtitle()
        nameLabel.brandAsSubtitle()
        roomNameLabel.brandAsSubtitle()
        capacityLabel.brandAsSubheading()
    }
}
