//
//  BookingRepository.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 5/8/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import Foundation

/// Repository is responsible for delivering Booking data from/to different external APIs (Network, Storage etc)
/// For example, if there is no in ternet connection we might choose to load user's bookings from local storage and update the storage when connection esteblishes
class BookingRepository {
    /// Service resposible for networking
    let networkService: BookingNetworkServiceProtocol
    
    let authorization: Authorization?
    /// Init
    ///
    /// - Parameter networkService: Network service
    init(networkService: BookingNetworkServiceProtocol, authorization: Authorization?) {
        self.networkService = networkService
        self.authorization = authorization
    }
    
    /// Load all bookings
    ///
    /// - Parameter completion: Completion handler
    func getAll(completion: @escaping ([Booking]?, String?) -> ()){
        self.networkService.fetch(path: "\(AppSettings.apiV)booking", queryItems: nil) { (data, error) in
            if let error = error {
                completion(nil, error.message)
                return
            }
            
            guard let data = data else {
                completion(nil, BookingRepositoryErrorKinds.nilBooking.rawValue)
                return
            }

            completion(data, nil)
        }
    }
    
    /// Retrieves only my bookings
    ///
    /// - Parameters:
    ///   - authorization: Authorization object to get auth token
    ///   - completion: completeion handler with Booking array and error
    func getMyBookings(completion: @escaping ([Booking]?, String?) -> ()){
        getAll { (data, error) in
            if let error = error {
                completion(nil, error)
                return
            }
            guard let data = data else {
                completion(nil, BookingRepositoryErrorKinds.nilBooking.rawValue)
                return
            }
            guard let authorization = self.authorization else {
                completion(nil, BookingRepositoryErrorKinds.noAuthorization.rawValue)
                return
            }
            do {
                try authorization.acquireUser(completion: { (token) in
                    guard let uid = token?.getUserId() else {
                        completion(nil, BookingRepositoryErrorKinds.nilUser.rawValue)
                        return
                    }
                    
                    //filters the data and removes any bookings that are not the current user
                    let filteredData = data.compactMap({ (booking) -> Booking? in
                        //if the user does not match, return nil (which compactMap will remove)
                        guard booking.user?.externalId == uid else {
                            return nil
                        }
                        return booking
                    })
                    let msg = filteredData.count == 0 ? BookingRepositoryErrorKinds.noBookings.rawValue : nil
                    completion(filteredData, msg)
                })
            } catch  {
                completion(nil, BookingRepositoryErrorKinds.acquireUserFailed.rawValue)
            }


        }
    }
    
    /// Save data in some persistent storage: remote server or core data etc
    ///
    /// - Parameters:
    ///   - booking: Data to be stored
    ///   - completion: Completion handler
    func save(booking: Booking, completion: @escaping ([Booking]?, String?) -> ()){
        self.networkService.push(data: booking) { (bookings, error) in
            if let error = error {
                completion(nil, error.message)
                return
            }
            
            guard let bookings = bookings else {
                completion(nil, "Bookings are nil")
                return
            }
            
            completion(bookings, nil)
        }
    }
}
