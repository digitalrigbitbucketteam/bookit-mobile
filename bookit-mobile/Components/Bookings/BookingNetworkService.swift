//
//  BookingNetworkService.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 5/8/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import Foundation

/// This service is responsible for handling Network APi calls
class BookingNetworkService: BookingNetworkServiceProtocol, SettingsServer {

    static var authorization = try? Authorization.init()
    
    private var backendNetwork: NetworkAccessProtocol
    
    init(backendNetwork: NetworkAccessProtocol? = nil) {
        self.backendNetwork = backendNetwork ?? BackendNetworkAccess()
    }
    
    /// Function that implements request to remote API in order to grab data
    ///
    /// - Parameters:
    ///   - path: Relative path to call
    ///   - queryItems: Items of query string
    ///   - completion: Completion handler
    func fetch(path: String, queryItems: [URLQueryItem]?, completion: @escaping ([Booking]?, BookingNetworkError?) -> ()) {
        
        //TODO: create proper error when url fails
        guard let urlString = getCurrentServerUrl()
            , var urlComponents = URLComponents(string: urlString) else {
            completion(nil,nil)
            return
        }
        
        urlComponents.path = path
        if(queryItems != nil){
            urlComponents.queryItems = queryItems
        }
        
        guard let url = urlComponents.url else {
            completion(nil, BookingNetworkError(code: nil, message: "URL could not be created from components  \(urlComponents)"))
            return
        }
        
        backendNetwork.fetch(from: url) { (data, error) in
            
            if let error = error {
                completion(nil, BookingNetworkError(code: nil, message: "\(error)"))
                return
            }
            
            guard let data = data else {
                completion(nil, BookingNetworkError(code: nil, message: "No Data was returned"))
                return
            }
            
            let decoder = JSONDecoder()
            //configure it to decode dates into Date
            decoder.dateDecodingStrategy = .formatted(DateFormatter.bookit)
            guard let result = try? decoder.decode([Booking].self, from: data) else {
                let error = BookingNetworkError(code: nil, message: "Cannot parse")
                debugPrint("URL: ", url)
                debugPrint("Data: ", data)
                
                let parsedData = try? JSONSerialization.jsonObject(with: data, options: [])
                
                if parsedData != nil {
                    debugPrint("Parsed data: ", parsedData!)
                } else {
                    debugPrint("Parsed data is nil")
                }
                
                completion(nil, error)
                return
            }
            
            completion(result, nil)
        }
    }
    
    /// Function that implements request to remote API in order to push data
    ///
    /// - Parameters:
    ///   - data: Data to be pushed
    ///   - completion: Completion handler
    func push(data: Booking, completion: @escaping ([Booking]?, BookingNetworkError?) -> ()) {
        // MARK: 1. First, prepare URLSession, URLComponents and URLRequest
        let configuration = URLSessionConfiguration.ephemeral
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: .main)
        
        //TODO: create proper error when url fails
        guard let url = getCurrentServerUrl() else {
            completion(nil,nil)
            return
        }
        guard var urlComponents = URLComponents.init(string: url) else {
            completion(nil,nil)
            return
        }
        urlComponents.path = "\(AppSettings.apiV)booking"
        do {
            //first make sure we have a valid auth token
            try BookableNetworkService.authorization?.acquireUser { (token) in
                guard
                    let tokenString = token?.getTokenString(),
                    let url = urlComponents.url else {
                    completion(nil,nil)
                    return
                }
                //create the request
                var request = URLRequest(url: url)
                //set to POST
                request.httpMethod = "POST"
                //add auth token
                request.addValue(tokenString, forHTTPHeaderField: "Authorization")
                //set content-type to JSON
                request.addValue("application/json", forHTTPHeaderField: "content-type")
                //Quick dictionary for JSON post data
                let postData : [String: Any] = ["end": Formatter.bookit.string(from: data.end),
                                "start": Formatter.bookit.string(from: data.start),
                                "locationId": data.bookable.location.id,
                                "bookableId": data.bookable.id,
                                "subject": data.subject ?? ""]
                //set the body to the json data
                request.httpBody = try? JSONSerialization.data(withJSONObject: postData)
                
                //create the request
                session.dataTask(with: request) { (data, response, err) in
                    //if there is an error from the URLRequest, fail out
                    if err != nil {
                        let error = BookingNetworkError(code: nil, message: err.debugDescription)
                        completion(nil, error)
                        return
                    }
                    //check for a response, if there isn't one, fail out
                    guard let response = response else {
                        completion(nil, BookingNetworkError.init(code: nil, message: "Bad Response"))
                        return
                    }
                    //get the network messages, either from URLResponse, or data received
                    let (code, message) = response.getNetworkMessages(data: data)
                    
                    //if the HTTP code is >= 400, there was a service error
                    if (code ?? 0) >= 400 {
                        //so send the error across
                        let error = BookingNetworkError(code: code, message: message ?? "")
                        completion(nil, error)
                        return
                    }
                    
                    //make sure data is valid
                    guard let data = data else {
                        let error = BookingNetworkError(code: nil, message: "Data is empty")
                        completion(nil, error)
                        return
                    }
                    
                    // MARK: 3. Finally decode the response and map it with Model
                    let decoder = JSONDecoder()
                    //configure it to decode dates into Date
                    decoder.dateDecodingStrategy = .formatted(DateFormatter.bookit)
                    guard let result = try? decoder.decode(Booking.self, from: data) else {
                        let error = BookingNetworkError(code: nil, message: "Cannot parse")
                        debugPrint("URL: ", url)
                        debugPrint("Status Code: ", code ?? 0)
                        debugPrint("Response Data: ", message ?? "")
                        debugPrint("Data: ", data)
                        
                        let parsedData = try? JSONSerialization.jsonObject(with: data, options: [])
                        
                        if parsedData != nil {
                            debugPrint("Parsed data: ", parsedData!)
                        } else {
                            debugPrint("Parsed data is nil")
                        }
                        
                        completion(nil, error)
                        return
                    }
                    
                    completion([result], nil)
                    
                    }.resume()
            }
        } catch let error {
            var description = "Token Acquire Failed"
            if let error = error as? AuthorizationError {
                description = error.localizedDescription
            }
            //TODO: do something here with the error
            debugPrint(description)
        }
    }

}
