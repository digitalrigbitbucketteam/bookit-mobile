//
//  BookingNetworkServiceProtocol.swift
//  bookit-mobile
//
//  Created by Tony Rizzo on 6/18/18.
//  Copyright © 2018 Buildit@Wipro Digital. All rights reserved.
//

import Foundation

/// Blueprint for entities that imlement Booking API network call
protocol BookingNetworkServiceProtocol {
    
    /// Function that implements request to remote API in order to grab data
    ///
    /// - Parameters:
    ///   - path: Relative path to call
    ///   - queryItems: Items of query string
    ///   - completion: Completion handler
    func fetch(path: String, queryItems: [URLQueryItem]?, completion: @escaping ([Booking]?, BookingNetworkError?) -> ())
    
    
    /// Function that implements request to remote API in order to push data
    ///
    /// - Parameters:
    ///   - data: Data to be pushed
    ///   - completion: Completion handler
    func push(data: Booking, completion: @escaping ([Booking]?,  BookingNetworkError?) -> ())
}
