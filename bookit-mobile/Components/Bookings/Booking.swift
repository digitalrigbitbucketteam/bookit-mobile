//
//  Booking.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 5/3/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import Foundation


/// Booking data model
struct Booking: Codable {
    let id: String?
    var subject: String?
    var start: Date
    let startTimezoneAbbreviation: String
    var end: Date
    let endTimezoneAbbreviation: String
    let bookable: Bookable
    let user: User?
}

/// In order to properly manage Date parsing we have to add manual decoder
extension Booking {
    init (from decoder: Decoder) {
        guard let values = try? decoder.container(keyedBy: CodingKeys.self) else {
            fatalError("Cannot decode Booking values")
        }
        
        guard let id = try? values.decode(String.self, forKey: .id) else {
            fatalError("Cannot decode Booking id")
        }
        
        guard let subject = try? values.decode(String.self, forKey: .subject) else {
            fatalError("Cannot decode Booking subject")
        }
        
        guard let startTimezoneAbbreviation = try? values.decode(String.self, forKey: .startTimezoneAbbreviation) else {
            fatalError("Cannot decode Booking startTimezoneAbbreviation")
        }
        
        guard let endTimezoneAbbreviation = try? values.decode(String.self, forKey: .endTimezoneAbbreviation) else {
            fatalError("Cannot decode Booking endTimezoneAbbreviation")
        }
        
        guard let bookable = try? values.decode(Bookable.self, forKey: .bookable) else {
            fatalError("Cannot decode Booking bookable")
        }
        
        guard let user = try? values.decode(User.self, forKey: .user) else {
            fatalError("Cannot decode Booking user")
        }
        
        guard
            let startString = try? values.decode(String.self, forKey: .start),
            let start = Formatter.bookit.date(from: startString) else {
                fatalError("Cannot decode Booking start")
        }
        
        guard
            let endString = try? values.decode(String.self, forKey: .end),
            let end = Formatter.bookit.date(from: endString) else {
                fatalError("Cannot decode Booking end")
        }
        
        self.id = id
        self.subject = subject
        self.startTimezoneAbbreviation = startTimezoneAbbreviation
        self.endTimezoneAbbreviation = endTimezoneAbbreviation
        self.bookable = bookable
        self.user = user
        self.start = start
        self.end = end
    }
}
