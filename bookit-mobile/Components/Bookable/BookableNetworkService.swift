//
//  BookableNetworkService.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 5/7/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import Foundation

/// This service is responsible for handling Network APi calls
class BookableNetworkService: BookableNetworkServiceProtocol, SettingsServer {
    
    /// Provide the connectivity to the remote data store
    private var backendNetwork: NetworkAccessProtocol
    
    static var authorization = try? Authorization.init()

    /// Initializer
    ///
    /// - Parameter backendNetwork: Optional backend network provider. Use nil for default value
    init(backendNetwork: NetworkAccessProtocol? = nil) {
        self.backendNetwork = backendNetwork ?? BackendNetworkAccess()
    }
    
    /// Function that implements request to remote API in order to grab data
    ///
    /// - Parameters:
    ///   - path: Relative path to call
    ///   - queryItems: Items of query string
    ///   - completion: Completion handler
    func fetch(path: String, queryItems: [URLQueryItem]?, completion: @escaping ([Bookable]?, BookableNetworkError?) -> ()) {
        
        guard let urlString = getCurrentServerUrl()
            , var urlComponents = URLComponents.init(string: urlString) else {
            completion(nil,nil)
            return
        }

        urlComponents.path = path
        if(queryItems != nil){
            urlComponents.queryItems = queryItems
        }
        
        guard let url = urlComponents.url else {
            completion(nil, nil)
            return
        }
        
        backendNetwork.fetch(from: url) { (data, error) in
            if let error = error {
                completion(nil, BookableNetworkError(code: nil, message: "\(error)"))
                return
            }
            
            guard let data = data else {
                let error = BookableNetworkError(code: nil, message: "Data is empty")
                completion(nil, error)
                return
            }
            
            guard let result = try? JSONDecoder().decode([Bookable].self, from: data) else {
                let error = BookableNetworkError(code: nil, message: "Cannot parse")
                debugPrint("URL: ", url)
                debugPrint("Data: ", data)
                
                let parsedData = try? JSONSerialization.jsonObject(with: data, options: [])
                if parsedData != nil {
                    debugPrint("Parsed data: ", parsedData!)
                } else {
                    debugPrint("Parsed data is nil")
                }
                
                completion(nil, error)
                return
            }
            
            completion(result, nil)
        }
    }
    
    /// Function that implements request to remote API in order to push data
    ///
    /// - Parameters:
    ///   - data: Data to be pushed
    ///   - completion: Completion handler
    func push(data: [Bookable], completion: @escaping ([Bookable]?, BookableNetworkError?) -> ()) {
        fatalError("Not implemented")
    }
}
