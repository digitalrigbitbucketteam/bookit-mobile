//
//  RoomRepository.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 5/7/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//
import Foundation

/// Repository is responsible for delivering Bookable data from/to different external APIs (Network, Storage etc)
/// For example, in order to make a reservation in offline mode BookableRepository might save data in local storage and sync it with Server when connection is established
class BookableRepository: SettingsLocation {
    /// Service resposible for networking
    let networkService: BookableNetworkServiceProtocol
    
    
    /// API resource ID
    private let url = "bookable"
    
    /// Init
    ///
    /// - Parameter networkService: Network service
    init(networkService: BookableNetworkServiceProtocol) {
        self.networkService = networkService
    }
    
    /// Get all booking this available currently
    ///
    /// - Parameter completion: Completion handler
    func getCurrent(completion: @escaping ([Bookable]?, String?) -> ()){
        let start = Date()
        let queryItems = [
            URLQueryItem(name: "start", value: Formatter.bookit.string(from: start)),
            URLQueryItem(name: "end", value: Formatter.bookit.string(from: start.getStartOfNextDay())), // API requires start of the next day as end point
            URLQueryItem(name: "expand", value: "bookings")
        ]
        
        networkService.fetch(path: "\(AppSettings.apiV)location/\(getCurrentLocation().id)/\(url)", queryItems: queryItems) { (data, err) in
            if let err = err {
                completion(nil, err.message)
                return
            }
            
            guard let data = data else {
                completion(nil, "Data is nil")
                return
            }

            completion(self.prepareDisposition(bookables: data, from: start, to: start.addingTimeInterval(60 * 60)), nil)
        }
    }
    
    /// Search for bookable in date range
    ///
    /// - Parameters:
    ///   - start: Start time
    ///   - end: End time
    ///   - completion: Callback
    func search(start: Date, end: Date, completion: @escaping ([Bookable]?, String?) -> ()) {
        let queryItems = [
            URLQueryItem(name: "start", value: Formatter.bookit.string(from: start)),
            URLQueryItem(name: "end", value: Formatter.bookit.string(from: start.getStartOfNextDay())), // API requires start of the next day as end point
            URLQueryItem(name: "expand", value: "bookings")
        ]
        
        networkService.fetch(path: "\(AppSettings.apiV)location/\(getCurrentLocation().id)/\(url)", queryItems: queryItems) { (data, err) in
            if let err = err {
                completion(nil, err.message)
                return
            }
            
            guard let data = data else {
                completion(nil, "Data is nil")
                return
            }
            
            completion(self.prepareDisposition(bookables: data, from: start, to: end), nil)
        }
    }
    
    /// Update disposition in order to check if any bookings in this bookables are intersect with desired timeframe
    ///
    /// - Parameters:
    ///   - data: Bookables
    ///   - start: Start point of the timeframe
    ///   - end: End point of the timeframe
    /// - Returns: Same bookables but with updated dispositions
    func prepareDisposition(bookables data: [Bookable], from start: Date, to end: Date) -> [Bookable]{
        var bookables = [Bookable]()
        
        let searchStartTimestamp = start.timeIntervalSince1970
        let searchEndTimestamp = end.timeIntervalSince1970
        for item in data {
            var bookable = item
            
            // If bookable already closed there is nothing we should do
            if bookable.disposition.closed {
                bookables.append(bookable)
                continue
            }
            
            // If there is no booking at all for this bookable: allow booking for all day
            guard let bookings = bookable.bookings, !bookings.isEmpty else {
                bookable.disposition = BookableDisposition(closed: false, reason: "Free all day".localize())
                bookables.append(bookable)
                continue
            }
            
            // find if there is any FUTURE bookings
            // because some booking may already past and we don't care about them anymore
            let futureBookings = bookings.filter { (boolking) -> Bool in
                boolking.end.timeIntervalSince1970 > start.timeIntervalSince1970
            }
            
            if futureBookings.isEmpty {
                bookable.disposition = BookableDisposition(closed: false, reason: "Free all day".localize())
                bookables.append(bookable)
                continue
            }
            
            // Find all bookings that intersect with our timeframe and mark bookable if any
            for booking in futureBookings {
                let bookingStartTimestamp = booking.start.timeIntervalSince1970
                let bookingEndTimestamp = booking.end.timeIntervalSince1970
                
                if searchStartTimestamp <= bookingStartTimestamp && bookingStartTimestamp < searchEndTimestamp ||
                    searchStartTimestamp < bookingEndTimestamp && bookingEndTimestamp <= searchEndTimestamp {
                    let (_, st) = booking.end.getLocalizedDateTime()
                    bookable.disposition = BookableDisposition(closed: true, reason: "Booked 'til \(st)")
                } else {
                    let (_, st) = booking.start.getLocalizedDateTime()
                    bookable.disposition = BookableDisposition(closed: false, reason: "Free 'til \(st)")
                }
            }
            
            bookables.append(bookable)
        }
        
        return bookables
    }
}

