//
//  BookableNetworkError.swift
//  bookit-mobile
//
//  Created by Tony Rizzo on 6/18/18.
//  Copyright © 2018 Buildit@Wipro Digital. All rights reserved.
//

import Foundation

/// Blueprint for API Network error
struct BookableNetworkError: NetworkError, Error {
    var code: Int?
    var message: String?
}
