//
//  Bookable.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 4/27/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//
import Foundation


// Possible amnity of particular room
enum BookableAmenity {
    case whiteboard
    case television
    case video
    case speakers
    case printer
}

// Determines if a room is basically ready to be booked
struct BookableDisposition: Codable {
    let closed: Bool // If true room cannot be booked
    let reason: String // Already booked for this time, closed permanently etc
}

// Bookable aka Room
struct Bookable: Codable {
    let id: String
    let name: String
    let location: Location
    let bookings: [Booking]?
    var disposition: BookableDisposition
//    let amenities: [BookableAmenity] = []
    let capacity: Int = 0
}


