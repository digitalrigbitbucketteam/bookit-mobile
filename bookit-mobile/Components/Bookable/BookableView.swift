//
//  RoomView.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 4/27/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit

/// Represents visually a particular room
class BookableView: UIView {
    
    /// Main view
    @IBOutlet var contentView: UIView!
    
    /// Room name
    @IBOutlet weak var name: UILabel!
    
    /// Room capacity
    @IBOutlet weak var capacity: UILabel!
    
    /// Room availability
    @IBOutlet weak var occupancy: UILabel!
    
    /// Name of nib file associated with this view
    private let nibName = "BookableView"
    
    /// Init from code
    ///
    /// - Parameter frame: frame that determies sizes of the view
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    /// Init from nib
    ///
    /// - Parameter aDecoder: actual decoder
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    /// Actual preparation of this entity
    func commonInit(){
        Bundle.main.loadNibNamed(nibName, owner: self, options: nil)
        
        // Handy way of adding view with contstraints pined to superview
        addSubviewWithConstraints(
            view: contentView,
            top: 0,
            left: 0,
            bottom: 0,
            right: 0
        )
        
        brandit()
    }
    
    /// Setup view with data.
    /// Pay attention that this method might be called several times during lifetime of the app
    ///
    /// - Parameter room: Data model
    func setup(for room: Bookable){
        name.text = room.name.capitalizingFirstLetter()
        capacity.text = "Holds \(room.capacity) people"
        occupancy.text = room.disposition.reason
    }
    
    /// Apply all branding styles to this view
    func brandit(){
        name.brandAsTitle()
        capacity.brandAsSubtitle()
        occupancy.brandAsSubtitle()
    }
}
