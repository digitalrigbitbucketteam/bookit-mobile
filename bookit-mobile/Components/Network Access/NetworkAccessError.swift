//
//  NetworkAccessError.swift
//  bookit-mobile
//
//  Created by Tony Rizzo on 6/15/18.
//  Copyright © 2018 Buildit@Wipro Digital. All rights reserved.
//

import Foundation


enum NetworkAccessError: Error {
    case passThrough(Error)
    case noHTTPResponse
    case httpResponseCode(Int)
    case noData
}
