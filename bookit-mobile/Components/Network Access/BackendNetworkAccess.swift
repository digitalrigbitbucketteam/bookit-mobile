//
//  BackendNetworkAccess.swift
//  bookit-mobile
//
//  Created by Tony Rizzo on 6/15/18.
//  Copyright © 2018 Buildit@Wipro Digital. All rights reserved.
//

import Foundation

/// Provides the connectivity to BuildIt's backend systems that require MSAL access
class BackendNetworkAccess: NetworkAccessProtocol {
    
    func fetch(from url: URL, completion: @escaping (Data?, Error?) -> ()) {

        // TODO: This needs to move to a background thread but this is too tightly coupled because one of the View Controllers is initiated on a background thread
        let session = URLSession(configuration: .ephemeral)
//        let session = URLSession(configuration: .ephemeral, delegate: nil, delegateQueue: .main)
        
        do {
            
            let authorization = try Authorization.init()
            try authorization.acquireUser { (token) in
                
                guard let tokenString = token?.getTokenString() else {
                    completion(nil,nil)
                    return
                }

                var request = URLRequest(url: url)
                request.httpMethod = "GET"
                request.addValue(tokenString, forHTTPHeaderField: "Authorization")
                
                session.dataTask(with: request) { (data, response, err) in
                    if let error = err {
                        completion(nil, error)
                        return
                    }
                    
                    guard let httpResponse = response as? HTTPURLResponse else {
                        completion(nil, NetworkAccessError.noHTTPResponse)
                        return
                    }
                    
                    if httpResponse.statusCode >= 400 {
                        completion(nil, NetworkAccessError.httpResponseCode(httpResponse.statusCode))
                        return
                    }
                    
                    guard let data = data else {
                        completion(nil, NetworkAccessError.noData)
                        return
                    }
                    
                    completion(data, nil)
                    }.resume()
            }
        }  catch let error {
            completion(nil, error)
        }
        
    }
}
