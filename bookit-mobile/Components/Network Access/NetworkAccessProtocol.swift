//
//  NetworkAccessProtocol.swift
//  bookit-mobile
//
//  Created by Tony Rizzo on 6/15/18.
//  Copyright © 2018 Buildit@Wipro Digital. All rights reserved.
//

import Foundation

/// Network Access Protocol
protocol NetworkAccessProtocol {
    
    func fetch(from url: URL, completion: @escaping (Data?, Error?)->())
}
