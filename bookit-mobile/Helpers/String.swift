//
//  String.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 4/30/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import Foundation

extension String {
    
    /// Returns copy of current string with capital first letter
    ///
    /// - Returns: Updated string
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    
    /// Capitalized first letter of current string
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    
    /// Boilerplate for localization of this string
    ///
    /// - Returns: localized string for current locale
    func localize() -> String {
        return self
    }
}
