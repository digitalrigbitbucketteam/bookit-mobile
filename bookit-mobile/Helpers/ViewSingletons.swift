//
//  ViewSingletons.swift
//  bookit-mobile
//
//  Created by Cristiana Yambo on 5/22/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit
protocol viewSingletons {
    var rootViewController: UIViewController? { get set }
    var keyWindow: UIWindow? { get }
}
extension viewSingletons {
    var rootViewController: UIViewController? {
        get {
            return UIApplication.shared.keyWindow?.rootViewController
        }
        set {
            UIApplication.shared.keyWindow?.rootViewController = newValue
        }
    }
    var keyWindow: UIWindow? {
        get {
            return UIApplication.shared.keyWindow
        }
    }
}
struct viewSingleton: viewSingletons {
}
