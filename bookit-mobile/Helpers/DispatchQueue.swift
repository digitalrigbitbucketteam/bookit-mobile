//
//  DispatchQueue.swift
//  bookit-mobile
//
//  Created by Cristiana Yambo on 6/27/18.
//  Copyright © 2018 Buildit@Wipro Digital. All rights reserved.
//

import Foundation

extension DispatchQueue {
    
    /// Safe main thread sync, only calls dispatchQueue if not on main thread
    ///
    /// - Parameter work: closure to act on
    class func mainSyncSafe(execute work: () -> Void) {
        if Thread.isMainThread {
            work()
        } else {
            DispatchQueue.main.sync(execute: work)
        }
    }
    
    class func mainSyncSafe<T>(execute work: () throws -> T) rethrows -> T {
        if Thread.isMainThread {
            return try work()
        } else {
            return try DispatchQueue.main.sync(execute: work)
        }
    }
}
