//
//  UITextField.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 5/1/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit

extension UITextField {
    
    /// Brand this label as Title Label
    func brandAsTitle(){
        textColor = ThemeService.primaryTextColor
        font = ThemeService.brandFont(with: ThemeService.font.title)
    }
    
    /// Brand this label as Sub Title Label
    func brandAsSubtitle(){
        textColor = ThemeService.primaryTextColor
        font = ThemeService.brandFont(with: ThemeService.font.subtitle)
    }
    
    /// Brand this label as Heading Label
    func brandAsHeading(){
        textColor = ThemeService.secondaryTextColor
        font = ThemeService.brandFont(with: ThemeService.font.heading)
    }
    
    /// Brand this label as Sub Heading Label
    func brandAsSubheading(){
        textColor = ThemeService.secondaryTextColor
        font = ThemeService.brandFont(with: ThemeService.font.subheading)
    }
}
