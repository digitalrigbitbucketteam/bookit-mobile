//
//  UIView.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 4/30/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit

// Determines what side of view should contain border
enum UIViewBorderPosition {
    case top
    case bottom
    case left
    case right
}

extension UIView {
    
    /// Adds provided subview and pins constraints to it's superview
    ///
    /// - Parameters:
    ///   - view: UIView to be added as subview to current UIView
    ///   - top: Constant of Top constraint
    ///   - left: Constant of Left constraint
    ///   - bottom: Constant of Bottom constraint
    ///   - right: Constant of Right constraint
    ///   - width: Constant of Width constraint
    ///   - height: Constant of Height constraint
    func addSubviewWithConstraints(view: UIView, top: CGFloat?, left: CGFloat?, bottom: CGFloat?, right: CGFloat?, width: CGFloat?, height: CGFloat?) {
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.pinConstraintsToSubview(
            top: top,
            left: left,
            bottom: bottom,
            right: right,
            width: width,
            height: height,
            centerX: nil,
            centerY: nil
        )
    }
    
    
    /// Adds provided subview and pins constraints to it's superview
    ///
    /// - Parameters:
    ///   - view: UIView to be added as subview to current UIView
    ///   - top: Constant of Top constraint
    ///   - left: Constant of Left constraint
    ///   - bottom: Constant of Bottom constraint
    ///   - right: Constant of Right constraint
    func addSubviewWithConstraints(view: UIView, top: CGFloat?, left: CGFloat?, bottom: CGFloat?, right: CGFloat?){
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.pinConstraintsToSubview(
            top: top,
            left: left,
            bottom: bottom,
            right: right,
            width: nil,
            height: nil,
            centerX: nil,
            centerY: nil
        )
    }
    
    /// Adds provided subview and pins constraints to it's superview
    ///
    /// - Parameters:
    ///   - view: UIView to be added as subview to current UIView
    ///   - centerX: Constant of Center X constraint
    ///   - centerY: Constant of Center Y constraint
    func addSubviewWithConstraints(view: UIView, centerX: CGFloat?, centerY: CGFloat?){
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.pinConstraintsToSubview(
            top: nil,
            left: nil,
            bottom: nil,
            right: nil,
            width: nil,
            height: nil,
            centerX: centerX,
            centerY: centerY
        )
    }
    
    /// Adds provided subview and pins constraints to it's superview
    ///
    /// - Parameters:
    ///   - view: UIView to be added as subview to current UIView
    ///   - width: Constant of Width constraint
    ///   - height: Constant of Height constraint
    func addSubviewWithConstraints(view: UIView, width: CGFloat?, height: CGFloat?){
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.pinConstraintsToSubview(
            top: nil,
            left: nil,
            bottom: nil,
            right: nil,
            width: width,
            height: height,
            centerX: nil,
            centerY: nil
        )
    }
    
    /// Adds provided subview and pins constraints to it's superview
    ///
    /// - Parameters:
    ///   - view: UIView to be added as subview to current UIView
    ///   - width: Constant of Width constraint
    ///   - height: Constant of Height constraint
    ///   - centerX: Constant of Center X constraint
    ///   - centerY: Constant of Center Y constraint
    func addSubviewWithConstraints(view: UIView, width: CGFloat?, height: CGFloat?, centerX: CGFloat?, centerY: CGFloat?){
        addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        view.pinConstraintsToSubview(
            top: nil,
            left: nil,
            bottom: nil,
            right: nil,
            width: width,
            height: height,
            centerX: centerX,
            centerY: centerY
        )
    }
    
    
    /// Adds constraints between UIView and it's superview
    ///
    /// - Parameters:
    ///   - top: Constant of Top constraint
    ///   - left: Constant of Left constraint
    ///   - bottom: Constant of Bottom constraint
    ///   - right: Constant of Right constraint
    func pinConstraintsToSubview(top: CGFloat?, left: CGFloat?, bottom: CGFloat?, right: CGFloat?){
        pinConstraintsToSubview(
            top: top,
            left: left,
            bottom: bottom,
            right: right,
            width: nil,
            height: nil,
            centerX: nil,
            centerY: nil
        )
    }
    
    /// Adds constraints between UIView and it's superview
    ///
    /// - Parameters:
    ///   - width: Constant of Width constraint
    ///   - height: Constant of Height constraint
    func pinConstraintsToSubview(width: CGFloat?, height: CGFloat?){
        pinConstraintsToSubview(
            top: nil,
            left: nil,
            bottom: nil,
            right: nil,
            width: width,
            height: height,
            centerX: nil,
            centerY: nil
        )
    }
    
    /// Adds constraints between UIView and it's superview
    ///
    /// - Parameters:
    ///   - width: Constant of Width constraint
    ///   - height: Constant of Height constraint
    ///   - centerX: Constant of Center X constraint
    ///   - centerY: Constant of Center Y constraint
    func pinConstraintsToSubview(width: CGFloat?, height: CGFloat?, centerX: CGFloat?, centerY: CGFloat?){
        pinConstraintsToSubview(
            top: nil,
            left: nil,
            bottom: nil,
            right: nil,
            width: width,
            height: height,
            centerX: centerX,
            centerY: centerY
        )
    }
    
    /// Adds constraints between UIView and it's superview
    ///
    /// - Parameters:
    ///   - top: Constant of Top constraint
    ///   - left: Constant of Left constraint
    ///   - bottom: Constant of Bottom constraint
    ///   - right: Constant of Right constraint
    ///   - width: Constant of Width constraint
    ///   - height: Constant of Height constraint
    ///   - centerX: Constant of Center X constraint
    ///   - centerY: Constant of Center Y constraint
    func pinConstraintsToSubview(top: CGFloat?, left: CGFloat?, bottom: CGFloat?, right: CGFloat?, width: CGFloat?, height: CGFloat?, centerX: CGFloat?, centerY: CGFloat?){
        guard let parent = superview else {
            fatalError("Before pinning you have to add view to the subview")
        }
        
        if(top != nil) {
            let constraint = topAnchor.constraint(equalTo: parent.topAnchor, constant: top!)
            constraint.isActive = true
            constraint.identifier = "topAnchorToSubview"
        }
        
        if(left != nil) {
            let constraint = leftAnchor.constraint(equalTo: parent.leftAnchor, constant: left!)
            constraint.isActive = true
            constraint.identifier = "leftAnchorToSubview"
        }
        
        if(bottom != nil) {
            let constraint = bottomAnchor.constraint(equalTo: parent.bottomAnchor, constant: bottom!)
            constraint.isActive = true
            constraint.identifier = "bottomAnchorToSubview"
        }
        
        if(right != nil) {
            let constraint = rightAnchor.constraint(equalTo: parent.rightAnchor, constant: right!)
            constraint.isActive = true
            constraint.identifier = "rightAnchorToSubview"
        }
        
        if(width != nil) {
            let constraint = widthAnchor.constraint(equalTo: parent.widthAnchor, constant: width!)
            constraint.isActive = true
            constraint.identifier = "widthAnchorToSubview"
        }
        
        if(height != nil) {
            let constraint = heightAnchor.constraint(equalTo: parent.heightAnchor, constant: height!)
            constraint.isActive = true
            constraint.identifier = "heightAnchorToSubview"
        }
        
        if(centerX != nil) {
            let constraint = centerXAnchor.constraint(equalTo: parent.centerXAnchor, constant: centerX!)
            constraint.isActive = true
            constraint.identifier = "centerXAnchorToSubview"
        }
        
        if(centerY != nil) {
            let constraint = centerYAnchor.constraint(equalTo: parent.centerYAnchor, constant: centerY!)
            constraint.isActive = true
            constraint.identifier = "centerYAnchorToSubview"
        }
    }
    
    
    /// Add border to some side of UIView
    ///
    /// - Parameters:
    ///   - position: Position of border: left, top etc
    ///   - width: Width of the border
    ///   - color: Color of the border
    func addBorder(position: UIViewBorderPosition, width: CGFloat, color: UIColor){
        let border = CALayer()
        
        border.borderColor = color.cgColor
        border.borderWidth = width
        
        switch position {
        case .top:
            border.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: width)
        case .left:
            border.frame = CGRect(x: 0, y: 0, width: width, height: frame.size.height)
        case .right:
            border.frame = CGRect(x: frame.size.width - width, y: 0, width: frame.size.width, height: frame.size.height)
        case .bottom:
            border.frame = CGRect(x: 0, y: frame.size.height - width, width: frame.size.width, height: width)
        }
        
        layer.addSublayer(border)
        layer.masksToBounds = true
    }
}
