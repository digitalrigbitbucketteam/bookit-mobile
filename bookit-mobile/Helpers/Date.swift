//
//  Time.swift
//  bookit-mobile
//
//  Created by Cristiana Yambo on 5/31/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import Foundation

extension Date {
    
    /// Rounds up to the next HH:00, HH:15, HH:30 or HH:45
    func roundUpToFifteenMinutes() -> Date {
        let seconds = TimeInterval(Int(timeIntervalSinceReferenceDate / (900)) * (900)) + 900
        return Date.init(timeIntervalSinceReferenceDate: seconds)
    }
    
    /// Rounds up to the next HH:00 or HH:30
    func roundUpToThirtyMinutes() -> Date {
        let seconds = TimeInterval(Int(timeIntervalSinceReferenceDate / (1800)) * (1800)) + 1800
        return Date.init(timeIntervalSinceReferenceDate: seconds)
    }
    
    /// Time from the date to current date in terms of hours and minutes
    func getDurationFrom(_ from: Date) -> String {
        let interval = from.timeIntervalSinceReferenceDate - timeIntervalSinceReferenceDate
        return (Formatter.duration.string(from: interval) ?? "0").replacingOccurrences(of: ",", with: "")
    }
    
    /// Gets the localized date in a tuple of strings representing the date and time
    func getLocalizedDateTime() -> (String,String) {
        let date = DateFormatter.localizedString(from: self, dateStyle: .medium, timeStyle: .none)
        let time = DateFormatter.localizedString(from: self, dateStyle: .none, timeStyle: .short)
        return (date,time)
    }
    
    func getLocalizedDateTimeRelative() -> (String,String) {
        let sd = Formatter.relative.string(from: self)
        let st = DateFormatter.localizedString(from: self, dateStyle: .none, timeStyle: .short)
        return (sd,st)
    }
    
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
    
    var endOfDay: Date? {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)
    }
    
    func getStartOfNextDay() -> Date {
        return self.startOfDay.addingTimeInterval(60 * 60 * 24)
    }
    
    func getIntervalsThrough(end: Date) -> Int {
        //gets the inteval by selecting the second item in the timeValues
        guard let minuteInterval = Int((PickerValues.timeValues[2][1].value as? String) ?? "0") else {
            return 0
        }
        let seconds = Int(abs(end.timeIntervalSinceReferenceDate - timeIntervalSinceReferenceDate))
        return Int(seconds / (minuteInterval * 60))
    }
    
    func getDaysThrough(end: Date) -> Int {
        let start = startOfDay
        let end = end.startOfDay
        let seconds = abs(end.timeIntervalSinceReferenceDate - start.timeIntervalSinceReferenceDate)
        return Int(seconds / 86400)
    }
}

extension Formatter {
    static let iso8601: ISO8601DateFormatter = {
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withInternetDateTime]
        return formatter
    }()
    static let relative: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.doesRelativeDateFormatting = true
        return formatter
    }()
    static let bookit: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm"
        return formatter
    }()
    static let duration: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        formatter.allowedUnits = [ .minute, .hour ]
        return formatter
    }()
    static let timeOnly: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter
    }()
}
