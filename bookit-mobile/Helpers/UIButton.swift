//
//  UIButton.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 5/1/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit

extension UIButton {
    
    /// Apply branding styles
    func brandAsPrimary(){
        setTitleColor(ThemeService.primaryButtonTextColor, for: .normal)
        backgroundColor = ThemeService.primaryButtonBgColor
    }

}
