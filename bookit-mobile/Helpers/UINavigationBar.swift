//
//  UINavigationBar.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 5/1/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit
extension UINavigationBar {
    
    /// Adds branding styles to this navigation bar
    func brandit(){
        tintColor = ThemeService.primaryBgColor
        barTintColor = ThemeService.primaryBrandColor
        
        let textAttributes = [NSAttributedStringKey.foregroundColor: ThemeService.primaryBgColor]
        titleTextAttributes = textAttributes
    }
}
