//
//  UrlResponse.swift
//  bookit-mobile
//
//  Created by Cristiana Yambo on 6/1/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import Foundation

extension URLResponse {
    func getNetworkMessages(data: Data?) -> (Int?,String?) {
        let decoder = JSONDecoder()
        if let data = data {
            if let result = try? decoder.decode(NetworkErrorResponse.self, from: data) {
                return (result.status, result.message)
            }
        }
        if let response =  self as? HTTPURLResponse {
            return (response.statusCode, response.description)
        }
        return (nil,nil)
    }
}
