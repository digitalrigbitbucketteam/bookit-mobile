//
//  NotificationNames.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 6/4/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import Foundation
extension Notification.Name {
    static let searchStartDateChanged = Notification.Name(rawValue: "searchStartDateChanged")
    static let searchEndDateChanged = Notification.Name(rawValue: "searchEndDateChanged")
    static let searchDurationChanged = Notification.Name(rawValue: "searchDurationChanged")
    static let searchCapacityChanged = Notification.Name(rawValue: "searchCapacityChanged")
}
