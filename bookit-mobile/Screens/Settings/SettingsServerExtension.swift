//
//  SettingsServerExtension.swift
//  bookit-mobile
//
//  Created by Cristiana Yambo on 5/29/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit

/// Enum contaning all the server types
///
/// - Local: local server
/// - Dev: dev server
/// - Stage: staging server
/// - Prod: production server
/// - Custom: custom server
enum ServerType: Int {
    case Local = 200
    case Dev
    case Stage
    case Prod
    case Custom
}

// MARK: - Notifcation sent when server has changed
extension Notification.Name {
    static let serverChanged = Notification.Name("BookitServerChanged")
}
/// Protocol Extension setting and getting the current server
protocol SettingsServer {
    var serverDefaults: UserDefaults { get }
    func setServerControls(control: UISegmentedControl?, text: UITextField?)
    func getCurrentServerUrl() -> String?
    func selectServer(index: Int, text: UITextField?)
    func setCustomServer(url: String)
}

// MARK: - Protocol Extension for server
extension SettingsServer {
    
    /// Local UserDefaults
    var serverDefaults: UserDefaults {
        get {
            return UserDefaults.standard
        }
    }
    var notificationCenter: NotificationCenter {
        get {
            return NotificationCenter.default
        }
    }
    /// Returns the currently selected server url string
    ///
    /// - Returns: url string for server
    func getCurrentServerUrl() -> String? {
        //get the selected server
        let st = getSelectedServerType()
        //if it is custom load it from defaults
        if st == .Custom {
            if let url = serverDefaults.url(forKey: DefaultsKeys.CustomServerUrl.rawValue) {
                return url.absoluteString
            }
        }
        //otherwise load the url from AppSettings
        return AppSettings.serverUrls[getSelectedServerType()]
    }
    
    /// Gets the selected server type
    ///
    /// - Returns: ServerType of selected server
    func getSelectedServerType() -> ServerType {
        if let selectedServer = ServerType.init(rawValue: serverDefaults.integer(forKey: DefaultsKeys.SelectedServer.rawValue)) {
            return selectedServer
        }
        return .Local
    }
    
    /// Called to select a new server
    ///
    /// - Parameters:
    ///   - index: index from segmented control
    ///   - text: UITextField that displays the url
    func selectServer(index: Int, text: UITextField?) {
        //make sure it is a valid type
        if let serverType = ServerType.init(rawValue: (index + ServerType.Local.rawValue)) {
            
            //set it in defaults
            serverDefaults.set(serverType.rawValue, forKey: DefaultsKeys.SelectedServer.rawValue)
            
            //post a server changed notification
            notificationCenter.post(name: .serverChanged, object: nil)
            
            //set the url text field
            setServerControls(control: nil, text: text)
        }
    }
    
    /// Called to set a custom server path
    ///
    /// - Parameter url: url string of custom server
    func setCustomServer(url: String) {
        if let url = URL.init(string: url) {
            serverDefaults.set(url, forKey: DefaultsKeys.CustomServerUrl.rawValue)
        }
    }
    
    /// Sets the server controls
    ///
    /// - Parameters:
    ///   - control: Segmented control for selecting server type
    ///   - text: UITextField displaying the current server
    func setServerControls(control: UISegmentedControl?, text: UITextField?) {

        /// Loads the segments onto the control
        func loadSegments() {
            //remove all of them first
            control?.removeAllSegments()
            //get an ordered array of names of the server types
            //ordered by enum value
            let names = Array(ServerType.Local.rawValue ... ServerType.Custom.rawValue).map {
                String.init(describing: (ServerType.init(rawValue: $0) ?? .Local) )
            }
            //insert all the segments
            for (index, name) in names.enumerated() {
                control?.insertSegment(withTitle: name, at: index, animated: false)
            }
        }
        
        /// Selects the proper segment
        func selectSegment() {
            //load the segment from defaults
            let index = getSelectedServerType().rawValue - ServerType.Local.rawValue
            //set it
            control?.selectedSegmentIndex = index
            
        }
        
        /// Sets the URL text field with the proper value
        func setUrlText() {
            text?.text = getCurrentServerUrl() ?? ""
            //if it is custom, enable editing
            if getSelectedServerType() == .Custom {
                text?.isEnabled = true
            } else {
                //otherwise disable editing
                text?.isEnabled = false
            }
        }
        
        //perform segment actions only if not nil
        if let _ = control {
            loadSegments()
            selectSegment()
        }
        //perform text actions only if not nil
        if let _ = text {
            setUrlText()
        }
    }}
