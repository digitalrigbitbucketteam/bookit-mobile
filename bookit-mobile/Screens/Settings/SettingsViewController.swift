//
//  SettingsViewController.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 4/27/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITextFieldDelegate, SettingsLogin, SettingsServer, SettingsLocation, SettingsGeofence {
    
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var serverSegments: UISegmentedControl!
    @IBOutlet weak var serverUrl: UITextField!
    
    @IBOutlet weak var locationSegments: UISegmentedControl!
    
    @IBOutlet weak var geofenceSwitch: UISwitch!
    
    let authorization = try? Authorization.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setTargets()
        
        configureServerUrlField()
        configureNavController()
    
        UserDefaults.standard.addObserver(self, forKeyPath: DefaultsKeys.CurrentLocation.rawValue, options: .new, context: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setStatus()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //text field delegates
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //just resign when return is pressed
        textField.resignFirstResponder()
        return true
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        //TODO: observe gets called twice for every change due to apple bug
        if keyPath == DefaultsKeys.CurrentLocation.rawValue {
            setLocationControls(control: locationSegments)
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        //when editing ends check to see if it was the serverUrl field
        if textField == serverUrl {
            //if it was, set the curstom server
            if let t = serverUrl.text {
                setCustomServer(url: t)
            }
        }
    }
    
    /// Sets the control targets
    func setTargets() {
        loginButton.addTarget(self, action: #selector(loginout), for: .touchUpInside)
        serverSegments.addTarget(self, action: #selector(serverSelect), for: .valueChanged)
        locationSegments.addTarget(self, action: #selector(locationSelect), for: .valueChanged)
        geofenceSwitch.addTarget(self, action: #selector(geofenceSelect), for: .valueChanged)
    }
    func configureServerUrlField() {
        serverUrl.delegate = self
        serverUrl.returnKeyType = .done
        serverUrl.autocorrectionType = .no
    }
    /// Prepapre navigation controller: set title, brand it etc
    func configureNavController(){
        navigationItem.title = "Settings".localize()
        navigationController?.navigationBar.brandit()
    }
    
    /// Logs the user in or out
    @objc func loginout() {
        guard let authorization = authorization else {
            return
        }
        func setButtonTitle(token: AuthorizationToken?) {
            setStatus()
        }
        do {
           try logInOrOut(authorization: authorization, loginBlock: setButtonTitle, logoutBlock: setButtonTitle)
        } catch let error {
            var description = "Login or out failed"
            if let error = error as? AuthorizationError {
                description = error.localizedDescription
            }
            //TODO: do more than debug print
            debugPrint(description)
        }
    }
    
    /// Selects a new server from segmented control
    @objc func serverSelect() {
        selectServer(index: serverSegments.selectedSegmentIndex, text: serverUrl)
    }
    
    @objc func locationSelect() {
        if let name = locationSegments.titleForSegment(at: locationSegments.selectedSegmentIndex) {
            selectLocation(locationName: name)
        }
    }
    
    @objc func geofenceSelect() {
        setGeofence(enabled: geofenceSwitch.isOn)
    }
    /// sets the control status
    func setStatus() {
        setServerControls(control: serverSegments, text: serverUrl)
        setLoginButtonTitle(authorization: authorization, button: loginButton)
        setLocationControls(control: locationSegments)
        setGeofenceControls(control: geofenceSwitch)
    }
}
