//
//  SettingsGeofenceExtension.swift
//  bookit-mobile
//
//  Created by Cristiana Yambo on 6/14/18.
//  Copyright © 2018 Buildit@Wipro Digital. All rights reserved.
//

import UIKit

// Protocol extension for enabling geolocation of the user to a location
protocol SettingsGeofence {
    func setGeofenceControls(control: UISwitch?)
    func geofenceEnabled() -> Bool
    func setGeofence(enabled: Bool)
    var geofenceDefaults: UserDefaults { get }
    var geofence: Geofence { get }
}

extension SettingsGeofence {
    //User Defaults
    var geofenceDefaults: UserDefaults {
        get {
            return UserDefaults.standard
        }
    }
    //instance of the Geofence class
    var geofence: Geofence {
        get {
            return Geofence.instance
        }
    }
    
    /// Is the geofence enabled?
    ///
    /// - Returns: geofence enabled status
    func geofenceEnabled() -> Bool {
        //if location services are not enabled
        if !geofence.locationServicesEnabled() {
            //then return false
            return false
        }
        //otherwise return the Defaults status
        return geofenceDefaults.bool(forKey: DefaultsKeys.GeofenceEnabled.rawValue)
    }
    
    /// Sets the geofence status directly from UserDefaults
    func setGeofenceFromDefaults() {
        setGeofence(enabled: geofenceEnabled())
    }
    
    /// Directly sets the geofence status
    ///
    /// - Parameter enabled: enable or disable geofence
    func setGeofence(enabled: Bool) {
        //set the defaults
        geofenceDefaults.set(enabled, forKey: DefaultsKeys.GeofenceEnabled.rawValue)
        //enable or disable the Geofence class
        geofence.monitor(enabled)
    }
    
    /// Sets the geofence control in settings
    ///
    /// - Parameter control: switch to set
    func setGeofenceControls(control: UISwitch?) {
        control?.isOn = geofenceEnabled()
    }
}
