//
//  SettingsLoginExtension.swift
//  bookit-mobile
//
//  Created by Cristiana Yambo on 5/25/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit

/// Protocol extension for Logging in the user
protocol SettingsLogin {
    func logInOrOut(authorization: Authorization, loginBlock: @escaping (AuthorizationToken?) -> (), logoutBlock: (AuthorizationToken?) -> ()) throws
    func setLoginButtonTitle(authorization: Authorization?,button: UIButton)
}
extension SettingsLogin {
    
    /// Login or Logout on MSAL User
    ///
    /// - Parameters:
    ///   - authorization: Authorization struct
    ///   - loginBlock: block that is run when logging in is complete
    ///   - logoutBlock: block that is run when logout is complete
    /// - Throws: Error
    func logInOrOut(authorization: Authorization, loginBlock: @escaping (AuthorizationToken?) -> (), logoutBlock: (AuthorizationToken?) -> ()) throws {
        //check to see if user is logged in
        if authorization.isLoggedIn() {
            //try to logout
            do {
                try authorization.logout()
                logoutBlock(nil)
            }
            //the user is logged out, so try to login
        } else {
            do {
                try authorization.acquireUser(completion: loginBlock)
            } 
        }
    }
    
    /// Sets the title of the login button
    ///
    /// - Parameters:
    ///   - authorization: Authorization token
    ///   - button: button to set the title for
    func setLoginButtonTitle(authorization: Authorization?,button: UIButton) {
        //default title
        var title = "Login"
        //check to see if it is a valid token
        if let authorization = authorization  {
            if let token = authorization.getToken() {
                //if it is valid, set it to Logout
                title = "Logout"
                if let name = token.getUserName() {
                    title.append(" \(name)")
                }
                
            }
        }
        //set the button title
        button.setTitle(title, for: .normal)
    }
}
