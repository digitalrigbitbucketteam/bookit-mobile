//
//  SettingsLocationExtension.swift
//  bookit-mobile
//
//  Created by Cristiana Yambo on 6/4/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit

extension Notification.Name {
        static let locationChanged = Notification.Name("BookitLocationChanged")
}
/// Protocol extension for Setting the location
protocol SettingsLocation {
    func getCurrentLocation() -> Location
    func setLocationControls(control: UISegmentedControl?)
    func getAllLocations(_ completion: (([Location]?) -> ())?)
    var locationDefaults: UserDefaults { get }
    var repo: LocationRepository { get }
}
extension SettingsLocation {
    
    /// Location Repo
    var repo: LocationRepository {
        get {
            //TODO: figure a better way to add the repo to the extension so that it isn't initialized each time
            return LocationRepository.init(networkService: LocationNetworkService.init())
        }
    }
    
    /// User Defaults
    var locationDefaults: UserDefaults {
        get {
            return UserDefaults.standard
        }
    }
    
    var notificationCenter: NotificationCenter {
        get {
            return NotificationCenter.default
        }
    }
    
    /// Gets the location that was stored in UserDefaults
    ///
    /// - Returns: Location object previously stored
    func getLocationFromDefaults() -> Location? {
        if let locationData = locationDefaults.object(forKey: DefaultsKeys.CurrentLocation.rawValue) as? Data {
            //try to decode the plist format into a Location
            if let locationObject = try? PropertyListDecoder().decode(Location.self, from: locationData) {
                return locationObject
            }
        }
        return nil
    }
    
    /// Gets a location to use, even if defaults doesn't have one
    ///
    /// - Returns: non optional Location
    func getCurrentLocation() -> Location {
        //first check defaults
        if let location = getLocationFromDefaults() {
            return location
        }
        //then try to take the first location from the cache
        else {
            if let locations = LocationRepository.getCachedLocations() {
                return locations[0]
            }
        }
        //if all else fails, send the one in AppSettings
        return AppSettings.location
    }
    
    /// Initialized the segmented control to display location selection
    ///
    /// - Parameter control: segmented control to use
    func setLocationControls(control: UISegmentedControl?) {

        //first get all locations
        getAllLocations { (locations) in
            //make sure location is valid
            guard let locations = locations else { return }
            
            //get the current location name to determine which is selected
            let currentLocationName = self.getCurrentLocation().name
            
            //checking to see if the new location has already been selected
            if let currIndex = control?.selectedSegmentIndex {
                //making sure the index is within the range
                if currIndex >= 0 && currIndex < (control?.numberOfSegments ?? 0) {
                    if let currSegmentTitle = control?.titleForSegment(at: currIndex) {
                        if currSegmentTitle == currentLocationName {
                            //selected segment matches current location, so return
                            return
                        }
                    }
                }
            }
            
            //not selected, so remove everything, and rebuild
            control?.removeAllSegments()

            //set the selected index
            var selectedIndex = 0
            
            //go through them and insert the location name in the control
            for (index, l) in locations.enumerated() {
                //check to see if the name matches, if so that it the selected one
                if l.name == currentLocationName {
                    selectedIndex = index
                }
                control?.insertSegment(withTitle: l.name, at: index, animated: false)
            }
            
            //then select the correct one
            control?.selectedSegmentIndex = selectedIndex
        }
    }
    
    /// Called when a location has been selected
    ///
    /// - Parameter locationName: name of the location to select
    func selectLocation(locationName: String) {
        //get the location based on the name
        if let location = repo.getLocationForName(name: locationName) {
            //make sure the location is different from currently stored location
            if location != getCurrentLocation() {
                //then try to encode it in a plist
                if let l = try? PropertyListEncoder().encode(location) {
                    //then save it to defaults
                    locationDefaults.set(l, forKey: DefaultsKeys.CurrentLocation.rawValue)
                    //and post a notification
                    notificationCenter.post(name: .locationChanged, object: nil)
                }
            }

        }
    }
    
    /// Gets all of the locations fro the repo
    ///
    /// - Parameter completion: block with found locations
    func getAllLocations(_ completion: (([Location]?) -> ())?) {
        repo.getAll { (data, err) in
            completion?(data)
        }
        
    }
}
