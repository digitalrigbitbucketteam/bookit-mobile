//
//  MyBookingsTableViewCell.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 5/3/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit


/// Represents MyBooking Table Cell
class MyBookingsTableViewCell: UITableViewCell {
    
    /// Associated view
    let myBookingView = MyBookingView(frame: .zero)
    
    /// Init from code
    ///
    /// - Parameters:
    ///   - style: Cell style
    ///   - reuseIdentifier: Reusbale identifier
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    /// Init from nib
    ///
    /// - Parameter aDecoder: actual decoder
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    /// Actual preparation of this entity
    func commonInit(){
        selectionStyle = .none
        contentView.addSubviewWithConstraints(view: myBookingView, top: 10, left: 20, bottom: -20, right: -20)
    }
    
    /// Setup with data.
    /// Pay attention that this method might be called several times during lifetime of the app
    ///
    /// - Parameter booking: Data model
    func setup(for booking: Booking){
        myBookingView.setup(for: booking)
    }
    
}
