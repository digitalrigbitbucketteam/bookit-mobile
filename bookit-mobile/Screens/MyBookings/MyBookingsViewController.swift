//
//  MyBookingsViewController.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 4/27/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit


/// Represents screen of My Bookings
class MyBookingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    //semaphore so that loadData is only called one at a time
    let loadDataSemaphore = DispatchSemaphore.init(value: 1)

    // Table view instance
    let tableView = UITableView(frame: CGRect.zero)
    
    // Data
    var bookings = [Booking]() {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    // Identifier for cells
    let reuseIdentifier = "myBookingsCell"
    
    let center = NotificationCenter.default
    
    // Source of data
    let bookingRepository: BookingRepository
    
    /// Convenience init that adds the notifications to observe
    init(bookingRepository: BookingRepository) {
        self.bookingRepository = bookingRepository
        
        super.init(nibName: nil, bundle: nil)
        
        //set the notifications to observe
        center.addObserver(forName: .loggedIn, object: nil, queue: nil, using: observeNotification)
        center.addObserver(forName: .loggedOut, object: nil, queue: nil, using: observeNotification)
        center.addObserver(forName: .serverChanged, object: nil, queue: nil, using: observeNotification)
        center.addObserver(forName: .bookingAdded, object: nil, queue: nil, using: observeNotification)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        configureNavController()
        
        loadData()
    }
    
    /// Observer function for notifications
    ///
    /// - Parameter notification: notification sent
    func observeNotification(notification: Notification) {
        //if it is a logout notification erase the stored data
        if notification.name == .loggedOut {
            bookings = [Booking]()
        //otherwise load new data
        } else {
            loadData()
        }
    }
    /// Prepare navigation controller: set title, brand it etc
    func configureNavController(){
        navigationItem.title = "My Bookings".localize()
        
        navigationController?.navigationBar.brandit()
    }
    
    /// Prepare table view
    func configureTableView(){
        tableView.register(MyBookingsTableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80
        
        view.addSubviewWithConstraints(
            view: tableView,
            top: 0,
            left: 0,
            bottom: 0,
            right: 0
        )
    }
    
    /// Number of sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    /// Number of rows in each section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookings.count
    }
    
    /// Configures reusable cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? MyBookingsTableViewCell else {
            fatalError("Cannot load cel for \(reuseIdentifier)")
        }
        
        cell.setup(for: bookings[indexPath.row])
        
        return cell
    }
    
    /// Commit method for tableview - deletes row when cell swiped left
    ///
    /// - Parameters:
    ///   - tableView: default tableView
    ///   - editingStyle: default editingStyle
    ///   - indexPath: indexPath of cell that is swiped
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.bookings.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    /// Load data from external source and add it into the table
    func loadData() {        
        bookingRepository.getMyBookings { (data, err) in
            //check the semaphore
            self.loadDataSemaphore.wait()
            
            if let err = err {
                debugPrint(err)
            } else if let data = data {
                self.bookings = data
            }
            
            //signal that we are done with the semaphore
            self.loadDataSemaphore.signal()
        }
    }
    
    deinit {
        //remove all observers on deinit
        center.removeObserver(self)
    }
}
