//
//  WaitViewController.swift
//  bookit-mobile
//
//  Created by Cristiana Yambo on 5/25/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit

class WaitViewController: UINavigationController {
    var viewController : UIViewController?
    let loader = UIActivityIndicatorView.init(activityIndicatorStyle: .gray)
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavController()
        configureView()
    }
    func configureNavController() {
        navigationBar.brandit()
    }
    func configureView() {
        let vc = UIViewController.init()
        
        vc.view.backgroundColor = .white
        pushViewController(vc, animated: false)
        loader.layer.opacity = 1.0

        vc.view.addSubviewWithConstraints(view: loader, centerX: 0, centerY: 0)
        loader.startAnimating()
        viewController = vc
//        UIView.animate(withDuration: 50.0, animations: {
//            self.loader.layer.opacity = 1.0
//        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
