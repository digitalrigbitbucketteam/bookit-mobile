//
//  TestViewController.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 5/2/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit

/// Represents controller for succesful booking
class BookingSuccessViewController: UIViewController, CAAnimationDelegate {
    
    /// Animated view to be shown
    var animationObj: SuccessAnimation?
    
    /// Constraint to be adjusted during animation
    var animationObjTopConstraint : NSLayoutConstraint?
    
    //Booking data model
    var booking: Booking?
    
    //completion handler
    var completion: (() -> Void)?
    /// Main title
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Success!".localize()
        label.textAlignment = .center
        label.brandAsTitle()
        label.alpha = 0
        
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    /// Main description
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.brandAsHeading()
        label.alpha = 0
        label.numberOfLines = 0
        
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    /// Final button
    let gotitButton: UIButton = {
        let btn = UIButton()
        btn.brandAsPrimary()
        btn.setTitle("Thanks!".localize(), for: .normal)
        btn.alpha = 0
        
        btn.translatesAutoresizingMaskIntoConstraints = false
        
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        self.animationObj = createAnimationObject()
        
        // prepare title
        view.addSubviewWithConstraints(view: titleLabel, top: nil, left: 50, bottom: nil, right: -50)
        titleLabel.topAnchor.constraint(equalTo: (animationObj?.bottomAnchor)!, constant: 20).isActive = true

        // prepare description
        view.addSubviewWithConstraints(view: descriptionLabel, top: nil, left: 50, bottom: nil, right: -50)
        descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20).isActive = true

        // prepare button
        view.addSubviewWithConstraints(view: gotitButton, top: nil, left: 70, bottom: nil, right: -70)
        gotitButton.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 70).isActive = true
        gotitButton.addTarget(self, action: #selector(gotit(sender:)), for: .touchUpInside)
    }
    
    /// Creating animation view and preparing it with branding, contraints etc
    ///
    /// - Returns: Animated view
    func createAnimationObject() -> SuccessAnimation {
        let anim = SuccessAnimation(
            frame: CGRect(x: 0, y: 0, width: 100, height: 100),
            lineColor: ThemeService.primaryBgColor.cgColor,
            lineWidth: 3,
            circleColor: ThemeService.primaryBrandColor.cgColor,
            delegate: self as CAAnimationDelegate
        )
        
        // we cannot use helper's addSubviewWithConstraints(...) because width and height are fixed, not relative to superview
        view.addSubview(anim)
        anim.translatesAutoresizingMaskIntoConstraints = false
        anim.widthAnchor.constraint(equalToConstant: 100).isActive = true
        anim.heightAnchor.constraint(equalToConstant: 100).isActive = true
        anim.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0).isActive = true
        
        // save this constraint to adjust it later on
        animationObjTopConstraint = anim.topAnchor.constraint(equalTo: self.view.topAnchor, constant: view.bounds.height / 2 - anim.frame.height)
        animationObjTopConstraint?.isActive = true
        
        return anim
    }
    
    /// Fill in date and start the animation
    ///
    /// - Parameter room: data model
    func start(booked: Booking, completion: (() -> Void)? = nil){
        self.booking = booked
        self.completion = completion
        
        guard let animationObj = animationObj else {
            return
        }
        guard let room = booking?.bookable else {
            return
        }

        let (sd,st) = booked.start.getLocalizedDateTime()
        let (_,et) = booked.end.getLocalizedDateTime()
        

        // prepare description text
        // TODO: localize
        descriptionLabel.text = "You booked \(room.name.capitalizingFirstLetter()) on \(sd) from \(st) to \(et)"
        
        animationObj.start()
    }
    
    /// When animated object completed animation
    ///
    /// - Parameters:
    ///   - anim: animation object
    ///   - flag: finishing flag
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        guard let animationObj = animationObj else {
            return
        }
        
        // animate animated view
        self.animationObjTopConstraint?.constant = view.frame.height * 0.40 - animationObj.frame.height
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
        
        // animate title
        UIView.animate(withDuration: 0.3, delay: 0.5, options: .curveEaseInOut, animations: {
            self.titleLabel.alpha = 1
        }, completion: nil)
        
        // animate description
        UIView.animate(withDuration: 0.3, delay: 0.8, options: .curveEaseInOut, animations: {
            self.descriptionLabel.alpha = 1
        }, completion: nil)
        
        // animate button
        UIView.animate(withDuration: 0.3, delay: 1.2, options: .curveEaseInOut, animations: {
            self.gotitButton.alpha = 1
        }, completion: nil)
    }
    
    /// When final is pressed cleanup and dismiss
    @objc func gotit(sender: UIButton){
        dismiss(animated: true, completion: {
            self.animationObj?.removeFromSuperview()
            self.animationObj = nil
            if let completion = self.completion {
                completion()
            }
        })
    }

}
