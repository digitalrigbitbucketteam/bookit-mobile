//
//  BookTableViewCell.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 4/30/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit

/// Represents Bookable Table Cell
class BookTableViewCell: UITableViewCell {
    
    /// Associated view
    let roomView = BookableView(frame: .zero)
    
    /// Init from code
    ///
    /// - Parameters:
    ///   - style: Cell style
    ///   - reuseIdentifier: Reusbale identifier
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }
    
    /// Init from nib
    ///
    /// - Parameter aDecoder: actual decoder
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    /// Actual preparation of this entity
    func commonInit(){
        contentView.addSubviewWithConstraints(
            view: roomView,
            top: 10,
            left: 20,
            bottom: -20,
            right: -20
        )
    }
    
    /// Setup with data.
    /// Pay attention that this method might be called several times during lifetime of the app
    ///
    /// - Parameter room: Data model
    func setup(room: Bookable){
        if room.disposition.closed {
            selectionStyle = .none
            contentView.alpha = 0.3
        } else {
            selectionStyle = .default
            contentView.alpha = 1.0
        }
        
        roomView.setup(for: room)
    }

}
