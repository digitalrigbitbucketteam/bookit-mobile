//
//  BookViewController.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 4/27/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//
import UIKit

/// Represents screen of bookables
class BookViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SettingsLocation {
    //semaphore so that loadData is only called one at a time
    let loadDataSemaphore = DispatchSemaphore.init(value: 1)

    /// Table view instance
    let tableView = UITableView(frame: .zero)
    
    /// Loader (activity indicator)
    let loader = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    // Controller to be shown on search
    let searchController: UIViewController
    
    // Controller to be shown on booking confirmation
    let confirmController: UIViewController
    
    // Notification center
    let center = NotificationCenter.default
    
    // Data
    var rooms = [[Bookable]]()
    
    // Identifier for cells
    let reuseIdentifier = "roomCell"
    
    // Source of data
    let bookableRepository: BookableRepository
    
    /// Was the Search params changed?
    var searchChanged = true
    
    /// Search start date param
    var searchStartDate: Date?
    
    /// Search end date param
    var searchEndDate: Date?
    
    /// Search duration param
    var searchDuration: Double?
    
    /// Search capacity param
    var searchCapacity: Int?
    
    var locationButtonItem: UIBarButtonItem?
    var backButtonItem: UIBarButtonItem?
    
    init(bookableRepository: BookableRepository, confirmController: UIViewController, searchController: UIViewController){
        self.confirmController = confirmController
        self.bookableRepository = bookableRepository
        self.searchController = searchController
        
        super.init(nibName: nil, bundle: nil)
        
        center.addObserver(forName: .locationChanged, object: nil, queue: nil, using: observeNotification)
        center.addObserver(forName: .loggedIn, object: nil, queue: nil, using: observeNotification)
        center.addObserver(forName: .loggedOut, object: nil, queue: nil, using: observeNotification)
        center.addObserver(forName: .serverChanged, object: nil, queue: nil, using: observeNotification)
        center.addObserver(forName: .bookingAdded, object: nil, queue: nil, using: observeNotification)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView()
        configureNavController()

        view.addSubviewWithConstraints(view: loader, centerX: 0, centerY: 0)
        loadData()
        
        // Add observers for search params changes
        center.addObserver(self, selector: #selector(searchStartDateChanged(notification:)), name: .searchStartDateChanged, object: nil)
        center.addObserver(self, selector: #selector(searchEndDateChanged(notification:)), name: .searchEndDateChanged, object: nil)
        center.addObserver(self, selector: #selector(searchDurationChanged(notification:)), name: .searchDurationChanged, object: nil)
        center.addObserver(self, selector: #selector(searchCapacityChanged(notification:)), name: .searchCapacityChanged, object: nil)
        

    }
    
    /// Observer function for notifications
    ///
    /// - Parameter notification: notification sent
    func observeNotification(notification: Notification) {
        //remove all data if it is a logout notification
        if notification.name == .loggedOut {
            rooms = [[Bookable]]()
            tableView.reloadData()
        } else {
        //otherwise just load the data again
           loadData()
        }
        setNavLocation()
    }
    
    /// Sets the location name in the navigation bar
    func setNavLocation() {
        locationButtonItem?.title = getCurrentLocation().name
        backButtonItem?.title = getCurrentLocation().name
    }
    
    /// Prepare navigation controller: set title, brand it etc
    func configureNavController(){
        let searchButton = UIBarButtonItem(image: UIImage(named: "search"), style: .plain, target: self, action: #selector(search(sender:)))
        let backButton = UIBarButtonItem(title: getCurrentLocation().name, style: .plain, target: nil, action: nil)
        backButtonItem = backButton
        
        let locationButton = UIBarButtonItem.init(title: getCurrentLocation().name, style: .plain, target: nil, action: nil)
        locationButton.isEnabled = false
        locationButton.setTitleTextAttributes([.foregroundColor : UIColor.white], for: .disabled)
        locationButtonItem = locationButton
        
        navigationItem.leftBarButtonItem = locationButton
        navigationItem.rightBarButtonItem = searchButton
        navigationItem.backBarButtonItem = backButton
        
        navigationItem.title = "Book a Room".localize()
        
        locationButtonItem?.title = getCurrentLocation().name
        
        navigationController?.navigationBar.brandit()
    }
    
    /// Prepare table view
    func configureTableView(){
        tableView.register(BookTableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 48
        
        view.addSubviewWithConstraints(
            view: tableView,
            top: 0,
            left: 0,
            bottom: 0,
            right: 0
        )
    }

    /// Just before appearing on the screen
    override func viewWillAppear(_ animated: Bool) {
        if searchChanged {
            searchChanged = false
            rooms.removeAll()
            tableView.reloadData()
            loadData()
        }
        
        // Deselect row on vc appearance
        guard let selectedRow = tableView.indexPathForSelectedRow else {
            return
        }
        
        tableView.deselectRow(at: selectedRow, animated: true)
    }

    /// Number of sections
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    /// Number of rows in each section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(!rooms.indices.contains(section)){
            return 0
        }
        
        return rooms[section].count
    }
    
    /// Configures header title
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            // If user searched for something display custom header title
            if searchStartDate != nil && searchEndDate != nil {
                let (dt, st) = (searchStartDate?.getLocalizedDateTimeRelative())!
                let (_, et) = (searchEndDate?.getLocalizedDateTime())!
                return "Available \(dt) \(st) - \(et)"
            }
            // Default title in case there is no search filter
            return "Available now for 1 hour".localize()
        default:
            return "Not available"
        }
    }
    
    /// Configures reusable cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? BookTableViewCell else {
            fatalError("Cannot load cel for \(reuseIdentifier)")
        }

        if(!rooms.indices.contains(indexPath.section)){
            return cell
        }
        
        if(!rooms[indexPath.section].indices.contains(indexPath.row)){
            return cell
        }
        
        cell.setup(room: rooms[indexPath.section][indexPath.row])
        
        return cell
    }
    
    /// Navigate to confirm controller on row selection
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section > 0 {
            return
        }
        
        guard let confirmController = confirmController as? BookConfirmViewController else {
            return
        }
        
        confirmController.bookable = self.rooms[indexPath.section][indexPath.row]
        navigationController?.pushViewController(confirmController, animated: true)
    }
    
    /// Navigate to search controller on search button touch
    @objc func search(sender:UIButton){
        navigationController?.present(searchController, animated: true, completion: nil)
    }
    
    /// Load data from external source and add it into the table
    func loadData(){

        loader.startAnimating()
        
        // If user've searched for something then use special Repo's method to get data
        if searchStartDate != nil && searchEndDate != nil {
            bookableRepository.search(start: searchStartDate!, end: searchEndDate!, completion: dataLoaded)
            
            return
        }
 
        // Otherwise just load current stuff
        bookableRepository.getCurrent(completion: dataLoaded)
    }
    
    /// When data was loaded we must analize results and update tableView in case of success
    ///
    /// - Parameters:
    ///   - data: Data that was loaded
    ///   - error: Error explanation
    func dataLoaded(data: [Bookable]?, error: String?){
        
        DispatchQueue.main.async {
            //check the semaphore
            self.loadDataSemaphore.wait()
            
            self.loader.stopAnimating()
            
            if error != nil {
                debugPrint(error!)
                return
            } else if let data = data {
                self.rooms = [
                    data.filter({ !$0.disposition.closed }),
                    data.filter({ $0.disposition.closed })
                ]
                
                self.tableView.reloadData()
            } else {
                debugPrint("Rooms are nil")
            }
            
            //signal that we are done with the semaphore
            self.loadDataSemaphore.signal()

        }

    }
    
    /// When search start date was changed
    ///
    /// - Parameter notification: Notification details
    @objc func searchStartDateChanged(notification: Notification){
        guard
            let data = notification.userInfo?.first,
            let value = data.value as? Date else {
                return
        }
        
        searchChanged = true
        searchStartDate = value
    }
    
    /// When search end date was changed
    ///
    /// - Parameter notification: Notification details
    @objc func searchEndDateChanged(notification: Notification){
        guard
            let data = notification.userInfo?.first,
            let value = data.value as? Date else {
                return
        }
        
        searchChanged = true
        searchEndDate = value
    }
    
    /// When search duration was changed
    ///
    /// - Parameter notification: Notification details
    @objc func searchDurationChanged(notification: Notification){
        guard
            let data = notification.userInfo?.first,
            let value = data.value as? Double else {
                return
        }
        
        searchChanged = true
        searchDuration = value
    }
    
    /// When search capacity was changed
    ///
    /// - Parameter notification: Notification details
    @objc func searchCapacityChanged(notification: Notification){
        guard
            let data = notification.userInfo?.first,
            let value = data.value as? Int else {
                return
        }
        
        searchChanged = true
        searchCapacity = value
    }
    
    deinit {
        //remove all observers on deinit
        center.removeObserver(self)
    }
}
