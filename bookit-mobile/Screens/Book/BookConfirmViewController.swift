//
//  BookConfirmViewController.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 5/1/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit
extension Notification.Name {
    static let bookingAdded = Notification.Name("BookitBookingAdded")
}
class BookConfirmViewController: UIViewController, UITextFieldDelegate, SettingsLocation {
    
    /// Selected bookable
    var bookable: Bookable?
    
    //ScrollView containing the Bookable info
    @IBOutlet weak var contentScrollView: UIScrollView!
    
    /// Room view (presents necessary room data)
    @IBOutlet weak var roomView: BookableView!
    
    /// Date label
    @IBOutlet weak var dateLabel: UILabel!
    
    /// Date UITextField + UIPickerView + UIToolbar or simply: Picker
    @IBOutlet weak var datePicker: Picker!
    
    /// Start time label
    @IBOutlet weak var startTimeLabel: UILabel!
    
    /// Start time UITextField + UIPickerView + UIToolbar or simply: Picker
    @IBOutlet weak var startTimePicker: Picker!
    
    /// Duration label
    @IBOutlet weak var durationLabel: UILabel!
    
    /// Duration UITextField + UIPickerView + UIToolbar or simply: Picker
    @IBOutlet weak var durationPicker: Picker!
    
    /// Event name label
    @IBOutlet weak var nameLabel: UILabel!
    
    /// Event name value textfiled
    @IBOutlet weak var nameValue: UITextField!
    
    /// Confirm button
    @IBOutlet weak var button: UIButton!
    
    // Source of data
    var bookingRepository: BookingRepository
    
    /// Opitions for start time
    let timeValues = PickerValues.timeValues
    
    /// Options for duration
    let durationValues = PickerValues.durationValues
    
    /// Options for date
    var dateValues = PickerValues.dateValues
    
    /// Options for capacity
    let capacityValues = PickerValues.capacityValues
    
    /// Width setting for Time picker
    let widthForTimePicker = PickerValues.widthForTimePicker
    
    let center = NotificationCenter.default
    
    /// When user pressed Bookit do actual booking
    @IBAction func bookit(_ sender: UIButton) {
        guard let bookable = bookable else {
            fatalError("bookable gone!")
        }

        let (start,duration) = getPickerValues()
        
        if start == nil || duration == nil {
            fatalError("Start or end time is unreachable")
        }
        
        let booking = Booking(
            id: nil,
            subject: nameValue.text,
            start: start!,
            startTimezoneAbbreviation: getCurrentLocation().timeZone,
            end:  (start?.addingTimeInterval(duration! * 60))!,
            endTimezoneAbbreviation: getCurrentLocation().timeZone,
            bookable: bookable,
            user: nil)
        
        bookingRepository.save(booking: booking, completion: { (success, error) in
            if let error = error {
                debugPrint(error)
                //TODO: does anything need to happen here?
                
                return
            }
            
            let controller = BookingSuccessViewController()
            self.present(controller, animated: true, completion: {
                controller.start(booked: booking, completion: {
                    self.navigationController?.popViewController(animated: true)
                    //post a notification, because there is no way to get to the booking tab from here
                    //TODO: think of a new data model to pass information to the different views that does not involve notifications
                    self.notificationCenter.post(name: .bookingAdded, object: nil)
                })
            })
            
        })
    }
    
    init(bookingRepository: BookingRepository, nibName: String?, bundle: Bundle?) {
        self.bookingRepository = bookingRepository
        super.init(nibName: nibName, bundle: bundle)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func getPickerValues() -> (Date?,Double?) {

        let duration: Double? = durationPicker.getValues().first

        let time: [String] = startTimePicker.getValues()

        if let date: String = datePicker.getValues().first {
            let dateString = "\(date)T\(time.joined(separator: ":"))"
            if let start = Formatter.bookit.date(from: dateString)  {
                return(start,duration)
            }
        }

        return (nil,duration)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        brandit()
        
        configureNavController()
        
        nameValue.text = "My Event".localize()
        nameValue.delegate = self
        
        datePicker.setup(values: dateValues, selectedIndexes: PickerValues.selectedDate)
        startTimePicker.setup(values: timeValues, selectedIndexes: Picker.getSelectedIndexForDate(date: Date().roundUpToThirtyMinutes()))
        durationPicker.setup(values: durationValues, selectedIndexes: PickerValues.selectedDuration)
        
        // Add observers for search params changes
        center.addObserver(self, selector: #selector(searchStartDateChanged(notification:)), name: .searchStartDateChanged, object: nil)
        center.addObserver(self, selector: #selector(searchDurationChanged(notification:)), name: .searchDurationChanged, object: nil)
        
        // add keyboard observers
        center.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillHide, object: nil)
        center.addObserver(self, selector: #selector(adjustForKeyboard), name: Notification.Name.UIKeyboardWillChangeFrame, object: nil)
        
    }

    /// Apply all branding styles to all views
    func brandit(){
        dateLabel.brandAsSubheading()
        datePicker.brandAsSubtitle()
        
        startTimeLabel.brandAsSubheading()
        startTimePicker.brandAsSubtitle()
        
        durationLabel.brandAsSubheading()
        durationPicker.brandAsSubtitle()
        
        nameLabel.brandAsSubheading()
        nameValue.brandAsSubtitle()
        
        //set the widths for the time picker
        startTimePicker.componentWidths = widthForTimePicker
        
        //add borders to editable fields
        durationPicker.addBorder(position: .bottom, width: 1, color: ThemeService.primaryTextColor)
        datePicker.addBorder(position: .bottom, width: 1, color: ThemeService.primaryTextColor)
        startTimePicker.addBorder(position: .bottom, width: 1, color: ThemeService.primaryTextColor)
        nameValue.addBorder(position: .bottom, width: 1, color: ThemeService.primaryTextColor)
        
        button.brandAsPrimary()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        guard let bookable = bookable else {
            fatalError("Room is required for this screen")
        }
        
        // setup room view with actual data
        roomView.setup(for: bookable)
        roomView.occupancy.isHidden = true
    }
    
    /// Prepare navigation controller: set title, brand it etc
    func configureNavController(){
        navigationItem.title = "Confirm booking"
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        nameValue.resignFirstResponder()
        
        return true
    }
    
    /// When search start date was changed
    ///
    /// - Parameter notification: Notification details
    @objc func searchStartDateChanged(notification: Notification){
        guard
            let data = notification.userInfo?.first,
            let value = data.value as? Date else {
                return
        }
        
        datePicker.setup(values: dateValues, selectedIndexes: [PickerIndex.init(component: 0, row: Date().getDaysThrough(end: value))])
        startTimePicker.setup(values: timeValues, selectedIndexes: Picker.getSelectedIndexForDate(date: value))
    }
    
    /// When search duration was changed
    ///
    /// - Parameter notification: Notification details
    @objc func searchDurationChanged(notification: Notification){
        guard
            let data = notification.userInfo?.first,
            let value = data.value as? Double else {
                return
        }
        
        guard let values: [PickerValue] = PickerValues.durationValues.first else {
            return
        }
        
        let index = values.index { (item) -> Bool in
            guard let itemValue = item.value as? Double else {
                return false
            }
            
            return itemValue == value
        }
        
        if index != nil {
            durationPicker.setup(values: durationValues, selectedIndexes: [PickerIndex(component: 0, row: index!)])
        }
        
    }
    
    /// Moves the scrollview to make sure edited item is visible
    ///
    /// - Parameter notification: default notification
    @objc func adjustForKeyboard(notification: Notification) {
        
        let userInfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            contentScrollView.contentInset = UIEdgeInsets.zero
        } else {
            contentScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height, right: 0)
        }
        
        contentScrollView.scrollIndicatorInsets = contentScrollView.contentInset
    }
    deinit {
        //remove all observers on deinit
        center.removeObserver(self)
    }
}
