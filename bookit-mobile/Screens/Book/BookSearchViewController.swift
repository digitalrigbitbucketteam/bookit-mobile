//
//  BookSearchViewController.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 4/30/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit

/// Representd search screen
class BookSearchViewController: UIViewController{
    
    /// Main title
    @IBOutlet weak var titleLabel: UILabel!
    
    /// Date label
    @IBOutlet weak var dateLabel: UILabel!
    
    /// Date UITextField + UIPickerView + UIToolbar or simply: Picker
    @IBOutlet weak var datePicker: Picker!
    
    /// Start time label
    @IBOutlet weak var timeLabel: UILabel!
    
    /// Start time UITextField + UIPickerView + UIToolbar or simply: Picker
    @IBOutlet weak var timePicker: Picker!
    
    /// Duration label
    @IBOutlet weak var durationLabel: UILabel!
    
    /// Duration UITextField + UIPickerView + UIToolbar or simply: Picker
    @IBOutlet weak var durationPicker: Picker!
    
    /// Capacity label
    @IBOutlet weak var capacityLabel: UILabel!
    
    /// Capacity UITextField + UIPickerView + UIToolbar or simply: Picker
    @IBOutlet weak var capacityPicker: Picker!
    
    /// Date label
    @IBOutlet weak var searchButton: UIButton!
    
    let center = NotificationCenter.default
    
    /// Start date of the event
    var start: Date? {
        willSet {
            if newValue != start {
                center.post(name: .searchStartDateChanged, object: nil, userInfo: ["value": newValue as Any])
            }
            
        }
    }
    
    /// End date of the event
    var end: Date? {
        willSet {
            if newValue != end{
                center.post(name: .searchEndDateChanged, object: nil, userInfo: ["value": newValue as Any])
            }
            
        }
    }
    
    /// Duration of the event
    var duration: Double? {
        willSet {
            if newValue != duration{
                center.post(name: .searchDurationChanged, object: nil, userInfo: ["value": newValue as Any])
            }
            
        }
    }
    
    /// Room capacity necessary for the event
    var capacity: Int? {
        willSet {
            if newValue != capacity{
                center.post(name: .searchCapacityChanged, object: nil, userInfo: ["value": newValue as Any])
            }
            
        }
    }
    
    /// Opitions for start time
    let timeValues = PickerValues.timeValues
    
    /// Options for duration
    let durationValues = PickerValues.durationValues
    
    /// Options for date
    var dateValues = PickerValues.dateValues
    
    /// Options for capacity
    let capacityValues = PickerValues.capacityValues
    
    /// Selected event date
    var selectedDate = PickerValues.selectedDate
    
    /// Selected event start time
    var selectedTime = PickerValues.selectedTime
    
    /// Selected event duration
    var selectedDuration = PickerValues.selectedDuration
    
    /// Selected room capacity for the event
    var selectedCapacity = PickerValues.selectedCapacity
    
    /// Width setting for Time picker
    let widthForTimePicker = PickerValues.widthForTimePicker
    
    /// Search button pressed
    // TODO: Add capacity filter after API will be ready for it
    @IBAction func search(_ sender: UIButton) {
        let time: [String] = timePicker.getValues()
        
        guard let date: String = datePicker.getValues().first else {
            fatalError("Date is empty")
        }
        
        guard let duration: Double = durationPicker.getValues().first else {
            fatalError("Duration is empty")
        }
        
        let dateString = "\(date)T\(time.joined(separator: ":"))"
        guard let start = Formatter.bookit.date(from: dateString) else {
            fatalError("Cannot create start date from: \(dateString)")
        }
        
        self.start = start
        self.end = start.addingTimeInterval(duration * 60)
        self.duration = duration
        
        dismiss(animated: true, completion: nil)
    }
    
    /// Close button pressed
    @IBAction func close(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker.setup(values: dateValues, selectedIndexes: selectedDate)
        timePicker.setup(values: timeValues, selectedIndexes: Picker.getSelectedIndexForDate(date: Date().roundUpToThirtyMinutes()))
        durationPicker.setup(values: durationValues, selectedIndexes: selectedDuration)
        capacityPicker.setup(values: capacityValues, selectedIndexes: selectedCapacity)
        
        brandit()
    }
    
    /// Apply all branding styles to all views
    func brandit(){
        titleLabel.brandAsTitle()
        
        dateLabel.brandAsSubheading()
        datePicker.brandAsSubtitle()
        
        timeLabel.brandAsSubheading()
        timePicker.brandAsSubtitle()
        
        durationLabel.brandAsSubheading()
        durationPicker.brandAsSubtitle()
        
        capacityLabel.brandAsSubheading()
        capacityPicker.brandAsSubtitle()
        
        searchButton.brandAsPrimary()
        
        timePicker.componentWidths = widthForTimePicker
        datePicker.addBorder(position: .bottom, width: 1, color: ThemeService.primaryTextColor)
        timePicker.addBorder(position: .bottom, width: 1, color: ThemeService.primaryTextColor)
        durationPicker.addBorder(position: .bottom, width: 1, color: ThemeService.primaryTextColor)
        capacityPicker.addBorder(position: .bottom, width: 1, color: ThemeService.primaryTextColor)
    }
}
