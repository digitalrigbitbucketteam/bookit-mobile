//
//  LoginMessagingViewController.swift
//  bookit-mobile
//
//  Created by Cristiana Yambo on 5/15/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit
protocol LoginMessagingAction: class {
    func loginCancelled()
}
class LoginMessagingViewController: UINavigationController {

    
    /// Empty tabbar to hide bottom buttons on MS controls
    let tabController = UITabBarController.init()
    weak var actionDelegate: LoginMessagingAction?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureNavController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //start at transparent
        view.layer.opacity = 0.0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //then animate the opacity when appears
        //this is to hids some issues with how the MS controls show an dhide
        UIView.animate(withDuration: 0.5) {
            self.view.layer.opacity = 1.0
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIView.animate(withDuration: 0.5) {
            self.view.layer.opacity = 0.0
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /// Configures the nav controller
    func configureNavController() {
        //show the tabbar
        pushViewController(tabController, animated: true)
        //set the title
        tabController.navigationItem.title = "Login with your Microsoft Account".localize()
        navigationBar.brandit()
        
        //put a cancel button on the right
        let cancel = UIBarButtonItem.init(image: UIImage.init(named: "close"), style: .plain, target: self, action: #selector(cancelLogin(sender:)))
        tabController.navigationItem.rightBarButtonItem = cancel
        
        //put an activity view on the left
        let activity = UIActivityIndicatorView.init(activityIndicatorStyle: .white)
        tabController.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: activity)
        activity.startAnimating()
        //and since there are no notifications from the MS controls, hide it in 5 seconds
        UIView.animate(withDuration: 5.0, animations: {
            activity.layer.opacity = 0.0
        }) { (_) in
            activity.removeFromSuperview()
        }
    }
    
    /// Login was cancelled via button
    ///
    /// - Parameter sender: default sender
    @objc func cancelLogin(sender: UIButton) {
        //simply call the delegate when cancelledk
        actionDelegate?.loginCancelled()
    }

}
