//
//  LoginViewController.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 4/27/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit
import WebKit
class LoginViewController: UIViewController {
    
    @IBOutlet weak var loginButton: UIButton!
    let authorization = try? Authorization.init()
    override func viewDidLoad() {
        super.viewDidLoad()
        loginButton.addTarget(self, action: #selector(login), for: .touchUpInside)
        configureNavController()
    }

    @objc func login() {
        authorization?.showLoginScreen()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /// Prepapre navigation controller: set title, brand it etc
    func configureNavController(){
        navigationItem.title = "Login".localize()
        navigationController?.navigationBar.brandit()
    }

}
