//
//  Errors.swift
//  bookit-mobile
//
//  Created by Cristiana Yambo on 5/18/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit

/// Authorization Error Struct
struct AuthorizationError : Error {
    enum ErrorKind : String {
        case acquireTokenFailed = "Acquire Token Failed"
        case loadContextFailed = "Cannot load Application Context"
        case externalError = "Authorization Failed"
        case resultFailed = "Authorization Result Failed"
        case invalidContext = "Invalid Context"
        case logoutFailed = "Logout failed"
        case loginFailed = "Login failed"
    }
    let kind : ErrorKind
    var localizedDescription: String {
        return kind.rawValue.localize()
    }
}

enum BookingRepositoryErrorKinds : String {
    case nilBooking = "Bookings are nil"
    case nilUser = "UserId is nil"
    case noBookings = "User has no bookings"
    case acquireUserFailed = "Failed to retrieve user"
    case noAuthorization = "Failed to load authorization"
}
/// NetworkError Protocol
protocol NetworkError {
    var code: Int? { get set }
    var message: String? { get set }
}

// MARK: - Extension to error to display an alert with an error message
extension Error {
    func displayError(with viewController: UIViewController,  completion: (() -> Void)?) {
        var message = localizedDescription
        if let networkMessage = (self as? NetworkError)?.message {
            message = networkMessage
        }
        let alert = UIAlertController.init(title: "Error".localize(), message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction.init(title: "Continue".localize(), style: .cancel, handler: { (action) in
            completion?()
        }))
        viewController.present(alert, animated: true)
    }
}

/// Codable for the JSON error responses from service
struct NetworkErrorResponse: Codable {
    let timestamp: String
    let status: Int
    let error: String
    let exception: String
    let message: String
    let path: String
}
