//
//  ThemeService.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 5/1/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit

// Contains structure for font sizes
struct ThemeFontSize {
    let title: ThemeFont
    let subtitle: ThemeFont
    let heading: ThemeFont
    let subheading: ThemeFont
}

// Determines params for themable font
struct ThemeFont {
    let isSystem: Bool
    let name: String?
    let weight: UIFont.Weight?
    let size: CGFloat
}

/// Contains branding styles for particular app: Colors, sizes, fonts names etc
struct ThemeService {
    // General
    static let primaryBrandColor = UIColor(red:0.48, green:0.78, blue:1.00, alpha:1.0)
    static let secondaryBrandColor = UIColor(red:0.00, green:0.25, blue:0.58, alpha:1.0)
    static let primaryBgColor = UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0)
    
    // Contains actual values for different title/heading types
    static let font = ThemeFontSize(
        title: ThemeFont(isSystem: true, name: nil, weight: .regular, size: 25),
        subtitle: ThemeFont(isSystem: true, name: nil, weight: .regular, size: 17),
        heading: ThemeFont(isSystem: true, name: nil, weight: .regular, size: 17),
        subheading: ThemeFont(isSystem: true, name: nil, weight: .regular, size: 15)
    )
    
    // Text
    static let primaryTextColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:1.0)
    static let secondaryTextColor = UIColor(red:0.80, green:0.80, blue:0.80, alpha:1.0)
    
    // Button
    static let primaryButtonBgColor = UIColor(red:0.48, green:0.78, blue:1.00, alpha:1.0)
    static let primaryButtonTextColor = UIColor(red:1.00, green:1.00, blue:1.00, alpha:1.0)
    
    /// Returns UIFont branded with provided theme font options
    ///
    /// - Parameter themeFont: theme font params
    /// - Returns: branded font
    static func brandFont(with themeFont: ThemeFont) -> UIFont {
        if !themeFont.isSystem && themeFont.name != nil {
            return UIFont(name: themeFont.name!, size: themeFont.size)!
        }
        
        if themeFont.weight != nil{
            return UIFont.systemFont(ofSize: themeFont.size, weight: themeFont.weight!)
        }
        
        
        return UIFont.systemFont(ofSize: themeFont.size)
    }
}
