//
//  AppSettings.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 5/7/18
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import Foundation
import CoreLocation

/// Setting of current application
struct AppSettings {
    static let serverUrls : [ServerType : String] = [
        .Local: "http://localhost:8080/",
        .Dev: "",
        .Stage: "https://buildit-blue-staging-bookit-api.buildit.tools",
        .Prod: "",
        .Custom: ""
    ]
    
    static let apiV = "/v1/"
    
    static var location = Location(id: "b1177996-75e2-41da-a3e9-fcdd75d1ab31", name: "NYC", timeZone: "EDT", timezoneDisplayName: "Eastern")

    static let geoFenceRadius = 20.0 //radius in kilometers for the geofence
    static let distanceFilter = 250.0 //distance in meters for a location update to trigger
    static let coordinates: [String : CLLocation] = [
        //NYC
        "b1177996-75e2-41da-a3e9-fcdd75d1ab31" : CLLocation.init(latitude: 40.7004284, longitude: -73.9896764),
        //LON
        "43ec3f7d-348d-427f-8c13-102ca0362a62" : CLLocation.init(latitude: 51.5201052, longitude: -0.0871921),
        //DEN
        "439c3fe8-124f-4a44-8f97-662a5d8334d3" : CLLocation.init(latitude: 39.7525791, longitude: -105.0046724)
        
    ]
    
    static let loginClientId = "042e99b8-e551-4645-8baa-e9fa0a659ed3"
    //info from https://docs.microsoft.com/en-us/azure/active-directory/develop/active-directory-v2-protocols-oauth-code
    static let appUrlScheme = "bookit-mobile"
    static let redirectPath = "/oauth2/callback"
    static let loginHost = "login.microsoftonline.com"
    static let loginPath = "/common/oauth2/v2.0/authorize"
    static let loginUrl = "https://login.microsoftonline.com/common/v2.0"
    static let loginScopes = [
        "https://graph.microsoft.com/calendars.read",
        "https://graph.microsoft.com/calendars.readwrite",
        "https://graph.microsoft.com/user.read",
        ]
    static let loginResponseTypes = [
        "id_token",
        "token"
    ]
    
}

enum DefaultsKeys: String {
    case SelectedServer = "SelectedServer"
    case CustomServerUrl = "CustomServerUrl"
    case CurrentLocation = "CurrentLocation"
    case GeofenceEnabled = "GeofenceEnabled"
}
