//
//  AppDelegate.swift
//  bookit-mobile
//
//  Created by Gregory Soloshchenko on 4/27/18.
//  Copyright © 2018 Gregory Soloshchenko. All rights reserved.
//

import UIKit
import MSAL
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, SettingsGeofence {

    var window: UIWindow?
    let authorization = try? Authorization.init()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

    
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = UIColor.black
        window?.makeKeyAndVisible()
        window?.rootViewController = WaitViewController.init()
        
        //try to get the login token first
        try? authorization?.acquireUser(completion: { (_) in
            //display the ui on main thread
            DispatchQueue.main.async {
                
                //making sure all the locations are available before anything else starts
                //?? is this a good idea ??
                let locations = LocationRepository.init(networkService: LocationNetworkService.init())
                locations.getAll() { (location, err) in
                    
                    self.setGeofenceFromDefaults()
                    
                    // Auth
                    let authorization = try? Authorization.init()

                    DispatchQueue.main.async {
                        // Repos
                        let bookingRepository = BookingRepository(networkService: BookingNetworkService(), authorization: authorization)
                        let bookableRepository = BookableRepository(networkService: BookableNetworkService())
                        
                        // Controllers
                        let searchController = BookSearchViewController(nibName: "BookSearchViewController", bundle: .main)
                        let confirmController = BookConfirmViewController(bookingRepository: bookingRepository, nibName: "BookConfirmViewController", bundle: .main)
                        
                        // Tabbat controllers
                        let myBookings = MyBookingsViewController(bookingRepository: bookingRepository)
                        myBookings.tabBarItem = UITabBarItem(title: "MyBookings".localize(), image: UIImage(named: "bag"), tag: 0)
                        
                        let book = BookViewController(
                            bookableRepository: bookableRepository,
                            confirmController: confirmController,
                            searchController: searchController
                        )
                        
                        book.tabBarItem = UITabBarItem(title: "Book a room".localize(), image: UIImage(named: "plus"), tag: 1)
                        
                        let settings = SettingsViewController()
                        settings.tabBarItem = UITabBarItem(title: "Settings".localize(), image: UIImage(named: "settings"), tag: 2)
                        
                        let tabBarController = UITabBarController()
                        let controllers = [myBookings, book, settings]
                        tabBarController.viewControllers = controllers.map { UINavigationController(rootViewController: $0)}
                        tabBarController.selectedIndex = 1
                        tabBarController.tabBar.tintColor = ThemeService.primaryBrandColor
                        
                        // Display the tabbar
                        self.window?.rootViewController = tabBarController
                    }
                }
            }
        })

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if MSALPublicClientApplication.handleMSALResponse(url) == true {
            //callback received
        }
        return true
    }

}

